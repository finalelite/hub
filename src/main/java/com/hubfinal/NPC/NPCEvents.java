package com.hubfinal.NPC;

import com.hubfinal.Main;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;
import net.minecraft.server.v1_13_R2.Packet;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_13_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import java.lang.reflect.Field;
import java.util.List;
import java.util.NoSuchElementException;

public class NPCEvents {

    private static Player player;
    private static Channel channel;
    private static boolean check;

    public NPCEvents(Player player) {
        NPCEvents.player = player;
    }

    public void inject() {
        try {
            CraftPlayer cPlayer = (CraftPlayer) player;
            channel = cPlayer.getHandle().playerConnection.networkManager.channel;
            channel.pipeline().addAfter("decoder", "PacketInjector", new MessageToMessageDecoder<Packet<?>>() {
                @Override
                protected void decode(ChannelHandlerContext arg0, Packet<?> packet, List<Object> arg2) throws Exception {
                    arg2.add(packet);
                    interact(packet);
                }
            });
        } catch (NoSuchElementException e) {
            //-
        }
    }

    public void uninject() {
        if (channel.pipeline().get("PacketInjector") != null) {
            channel.pipeline().remove("PacketInjector");
        }
    }

    public void setValue(Object obj, String name, Object value) {
        try {
            Field field = obj.getClass().getDeclaredField(name);
            field.setAccessible(true);
            field.set(obj, value);
        } catch (Exception e) {
        }
    }

    public Object getValue(Object obj, String name) {
        try {
            Field field = obj.getClass().getDeclaredField(name);
            field.setAccessible(true);
            return field.get(obj);
        } catch (Exception e) {
        }
        return null;
    }

    public void interact(Packet<?> packet) {
        if (packet.getClass().getSimpleName().equalsIgnoreCase("PacketPlayInUseEntity")) {
            try {
                int ID = (Integer) getValue(packet, "a");

                Player target = Bukkit.getPlayerExact(RegisterPlayerName.get(ID).getUUID());
                int EntityID = NPCApi.getNpcIDProv(target.getName() + ID);

                if (NPCFunctions.getFunctionsWithEntityID(EntityID) != null) {
                    if (NPCFunctions.getFunctionsWithEntityID(EntityID).contains("inv:")) {
                        if (check == false) {
                            Inventory inv = (Inventory) NPCFunctions.getExistMethod(NPCFunctions.getFunctionsWithEntityID(EntityID), target);
                            target.openInventory(inv);
                            check = true;

                            Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                @Override
                                public void run() {
                                    check = false;
                                }
                            }, 20L);
                        }
                    }

                    if (NPCFunctions.getFunctionsWithEntityID(EntityID).contains("say:")) {
                        if (check == false) {
                            NPCFunctions.getExistMethod(NPCFunctions.getFunctionsWithEntityID(EntityID), target);
                            check = true;

                            Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                @Override
                                public void run() {
                                    check = false;
                                }
                            }, 20L);
                        }
                    }

                    if (NPCFunctions.getFunctionsWithEntityID(EntityID).contains("tp:")) {
                        if (check == false) {
                            NPCFunctions.getExistMethod(NPCFunctions.getFunctionsWithEntityID(EntityID), target);
                            check = true;

                            Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                @Override
                                public void run() {
                                    check = false;
                                }
                            }, 20L);
                        }
                    }
                }
            } catch (Exception e) {

            }
        }
    }
}
