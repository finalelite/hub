package com.hubfinal.NPC;

import java.util.HashMap;

public class NpcCacheLastV {

    private String Location;
    private String lastViewer;
    private static final HashMap<String, NpcCacheLastV> CACHE = new HashMap<String, NpcCacheLastV>();

    public NpcCacheLastV(String Location, String lastViewer) {
        this.Location = Location;
        this.lastViewer = lastViewer;
    }

    public NpcCacheLastV insert() {
        CACHE.put(String.valueOf(this.Location), this);

        return this;
    }

    public String getLastViewer() {
        return this.lastViewer;
    }

    public void setLastViewer(String lastViewer) {
        this.lastViewer = lastViewer;
    }

    public void setLocation(String Location) {
        this.Location = Location;
    }

    public String getLocation() {
        return this.Location;
    }

    public static NpcCacheLastV get(String Location) {
        return CACHE.get(String.valueOf(Location));
    }
}
