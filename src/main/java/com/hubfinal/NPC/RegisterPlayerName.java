package com.hubfinal.NPC;

import java.util.HashMap;

public class RegisterPlayerName {

    private int ID;
    private String UUID;
    private static final HashMap<Integer, RegisterPlayerName> CACHE = new HashMap<Integer, RegisterPlayerName>();

    public RegisterPlayerName(int ID, String UUID) {
        this.ID = ID;
        this.UUID = UUID;
    }

    public RegisterPlayerName insert() {
        CACHE.put(Integer.valueOf(this.ID), this);

        return this;
    }

    public int getId() {
        return this.ID;
    }

    public void setId(int ID) {
        this.ID = ID;
    }

    public String getUUID() {
        return this.UUID;
    }

    public static RegisterPlayerName get(int ID) {
        return CACHE.get(Integer.valueOf(ID));
    }
}
