package com.hubfinal.NPC;

import com.hubfinal.API.PlayerUUID;
import com.hubfinal.Main;
import com.hubfinal.MySql;
import com.hubfinal.provisional.IDprov;
import com.hubfinal.utils.AlternateColor;
import com.hubfinal.utils.Skin;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import net.minecraft.server.v1_13_R2.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_13_R2.CraftServer;
import org.bukkit.craftbukkit.v1_13_R2.CraftWorld;
import org.bukkit.craftbukkit.v1_13_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.lang.reflect.Field;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class NPC {

    public static void enableNPCS(Player player) {
        if (getLastID() != 0) {
            for (int i = 1; i <= getLastID(); i++) {
                int entityID = NPCApi.getNpcIDD(i);
                setCacheInfos(entityID);
                onEnableNpcs(entityID, player);
            }
        }
    }

    public static void updateIDS(int idremoved) {
        if (getLastID() > idremoved) {
            for (int i = idremoved + 1; i <= getLastID(); i++) {
                NPCApi.updateID(i);
            }
        }
    }

    public static void setCacheInfos(int entityID) {
        if (NpcCacheLoc.get(String.valueOf(entityID)) == null) {
            new NpcCacheLoc(String.valueOf(entityID), NPCApi.getLocationEntityID(entityID)).insert();
        }
        if (NpcCacheHR.get(NpcCacheLoc.get(String.valueOf(entityID)).getLocation()) == null) {
            new NpcCacheHR(NpcCacheLoc.get(String.valueOf(entityID)).getLocation(), NPCApi.getHeadRotateNPCWithEntityID(entityID)).insert();
        }
        if (NpcCacheLastV.get(NpcCacheLoc.get(String.valueOf(entityID)).getLocation()) == null) {
            new NpcCacheLastV(NpcCacheLoc.get(String.valueOf(entityID)).getLocation(), NPCApi.getLastViewerNPC(NpcCacheLoc.get(String.valueOf(entityID)).getLocation())).insert();
        }
    }

    public static int getLastID() {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT max(ID) FROM Npcs_data");
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static void updateInfoNpc(int npcID) {

        String[] loc = NPCApi.getLocationEntityID(npcID).split(":");
        String[] visao = NPCApi.getVisaoEntityID(npcID).split(":");
        Location location = new Location(Bukkit.getWorld(NPCApi.getWorldEntityID(npcID)), Double.parseDouble(loc[0]), Double.parseDouble(loc[1]), Double.parseDouble(loc[2]), Float.parseFloat(visao[0]), Float.parseFloat(visao[1]));

        MinecraftServer nmsServer = ((CraftServer) Bukkit.getServer()).getServer();
        WorldServer nmsWorld = ((CraftWorld) location.getWorld()).getHandle();
        GameProfile gameProfile = new GameProfile(UUID.randomUUID(), AlternateColor.alternate(NPCApi.getTextoNpcWithNPCId(npcID)));
        changeSkin(gameProfile, NPCApi.getUUIDNpcWithNPCId(npcID));
        EntityPlayer npc = new EntityPlayer(nmsServer, nmsWorld, gameProfile, new PlayerInteractManager(nmsWorld));
        npc.setLocation(location.getX(), location.getY(), location.getZ(), (location.getYaw() * 256.0F) / 360.0F, location.getPitch());

        for (Player target : Bukkit.getOnlinePlayers()) {
            PlayerConnection connection = ((CraftPlayer) target).getHandle().playerConnection;
            connection.sendPacket(new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER, npc));
            connection.sendPacket(new PacketPlayOutNamedEntitySpawn(npc));
            headRotation(npc.getId(), location.getYaw(), location.getPitch());

            IDprov.get(target.getName() + npcID).setUUID("NULL");
            new IDprov(target.getName() + npcID, npc.getId()).insert();
        }

        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
            @Override
            public void run() {
                for (Player target : Bukkit.getOnlinePlayers()) {
                    PlayerConnection connection = ((CraftPlayer) target).getHandle().playerConnection;
                    connection.sendPacket(new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.REMOVE_PLAYER, npc));
                }
            }
        }, 5L);
    }

    public static void createNPC(Location location, String npcUUID, String msg) {
        MinecraftServer nmsServer = ((CraftServer) Bukkit.getServer()).getServer();
        WorldServer nmsWorld = ((CraftWorld) location.getWorld()).getHandle();
        GameProfile gameProfile = new GameProfile(UUID.randomUUID(), AlternateColor.alternate(msg));
        changeSkin(gameProfile, npcUUID);
        EntityPlayer npc = new EntityPlayer(nmsServer, nmsWorld, gameProfile, new PlayerInteractManager(nmsWorld));
        npc.setLocation(location.getX(), location.getY(), location.getZ(), (location.getYaw() * 256.0F) / 360.0F, location.getPitch());

        String loc = location.getX() + ":" + location.getY() + ":" + location.getZ();
        String visao = location.getYaw() + ":" + location.getPitch();
        Skin skin = new Skin(npcUUID);
        NPCApi.setNpc(getLastID() + 1, loc, npc.getId(), msg, npcUUID, location.getWorld().getName(), visao, false, "NULL", skin.getSkinValue(), skin.getSkinSignatur());

        for (Player target : Bukkit.getOnlinePlayers()) {
            PlayerConnection connection = ((CraftPlayer) target).getHandle().playerConnection;
            connection.sendPacket(new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER, npc));
            connection.sendPacket(new PacketPlayOutNamedEntitySpawn(npc));
            headRotation(npc.getId(), location.getYaw(), location.getPitch());

            new IDprov(target.getName() + npc.getId(), npc.getId()).insert();
            NPCApi.setNpcIDProv(target.getName() + npc.getId(), npc.getId(), PlayerUUID.getUUID(target.getName()));
            new RegisterPlayerName(npc.getId(), target.getName()).insert();
        }

        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
            @Override
            public void run() {
                for (Player target : Bukkit.getOnlinePlayers()) {
                    PlayerConnection connection = ((CraftPlayer) target).getHandle().playerConnection;
                    connection.sendPacket(new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.REMOVE_PLAYER, npc));
                }
            }
        }, 5L);
    }

    public static void onEnableNpcs(int npcID, Player player) {
        String[] loc = NPCApi.getLocationEntityID(npcID).split(":");
        String[] visao = NPCApi.getVisaoEntityID(npcID).split(":");
        Location location = new Location(Bukkit.getWorld(NPCApi.getWorldEntityID(npcID)), Double.parseDouble(loc[0]), Double.parseDouble(loc[1]), Double.parseDouble(loc[2]), Float.parseFloat(visao[0]), Float.parseFloat(visao[1]));

        MinecraftServer nmsServer = ((CraftServer) Bukkit.getServer()).getServer();
        WorldServer nmsWorld = ((CraftWorld) location.getWorld()).getHandle();
        GameProfile gameProfile = new GameProfile(UUID.randomUUID(), AlternateColor.alternate(NPCApi.getTextoNpcWithNPCId(npcID)));
        skinLoad(gameProfile, NPCApi.getValueSkin(NPCApi.getLocationEntityID(npcID)), NPCApi.getSignatureSkin(NPCApi.getLocationEntityID(npcID)));
        EntityPlayer npc = new EntityPlayer(nmsServer, nmsWorld, gameProfile, new PlayerInteractManager(nmsWorld));
        npc.setLocation(location.getX(), location.getY(), location.getZ(), (location.getYaw() * 256.0F) / 360.0F, location.getPitch());

        PlayerConnection connection = ((CraftPlayer) player).getHandle().playerConnection;
        connection.sendPacket(new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER, npc));
        connection.sendPacket(new PacketPlayOutNamedEntitySpawn(npc));
        headRotation(npc.getId(), location.getYaw(), location.getPitch());

        new IDprov(player.getName() + npcID, npc.getId()).insert();
        NPCApi.setNpcIDProv(player.getName() + npc.getId(), npcID, PlayerUUID.getUUID(player.getName()));
        new RegisterPlayerName(npc.getId(), player.getName()).insert();

        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
            @Override
            public void run() {
                PlayerConnection connection = ((CraftPlayer) player).getHandle().playerConnection;
                connection.sendPacket(new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.REMOVE_PLAYER, npc));
            }
        }, 5L);

        if (RegionNpc.get(String.valueOf(npcID)) == null) {
            RegionNpc newNpc = new RegionNpc(String.valueOf(npcID), Double.parseDouble(loc[0]) - 4, 0, Double.parseDouble(loc[2]) - 4, Double.parseDouble(loc[1]) + 4, 256, Double.parseDouble(loc[2]) + 4, Double.parseDouble(loc[1]), location.getWorld().getName());
            RegionsNpc.getTerrains().add(newNpc);
        }
    }

    private static void changeSkin(GameProfile profile, String npcName) {
        Skin skin = new Skin(npcName);
        profile.getProperties().put("textures", new Property("textures", skin.getSkinValue(), skin.getSkinSignatur()));
    }

    private static void skinLoad(GameProfile profile, String value, String signature) {
        profile.getProperties().put("textures", new Property("textures", value, signature));
    }

    public static void teleport(int entityID, Location location) {
        PacketPlayOutEntityTeleport packet = new PacketPlayOutEntityTeleport();
        setValue(packet, "a", entityID);
        setValue(packet, "b", getFixLocation(location.getX()));
        setValue(packet, "c", getFixLocation(location.getY()));
        setValue(packet, "d", getFixLocation(location.getZ()));
        setValue(packet, "e", getFixRotation(location.getYaw()));
        setValue(packet, "f", getFixRotation(location.getPitch()));

        sendPacket(packet);
        headRotation(entityID, location.getYaw(), location.getPitch());
    }


    public static void headRotation(int entityID, float yaw, float pitch) {
        for (Player target : Bukkit.getOnlinePlayers()) {
            if (IDprov.get(target.getName() + entityID) != null) {
                int npcId = IDprov.get(target.getName() + entityID).getIDNpcPlayer();

                PacketPlayOutEntity.PacketPlayOutEntityLook packet = new PacketPlayOutEntity.PacketPlayOutEntityLook(npcId, getFixRotation(yaw), getFixRotation(pitch), true);
                PacketPlayOutEntityHeadRotation packetHead = new PacketPlayOutEntityHeadRotation();
                setValue(packetHead, "a", Integer.valueOf(npcId));
                setValue(packetHead, "b", Byte.valueOf(getFixRotation(yaw)));

                sendPacket(packet);
                sendPacket(packetHead);
            } else {
                PacketPlayOutEntity.PacketPlayOutEntityLook packet = new PacketPlayOutEntity.PacketPlayOutEntityLook(entityID, getFixRotation(yaw), getFixRotation(pitch), true);
                PacketPlayOutEntityHeadRotation packetHead = new PacketPlayOutEntityHeadRotation();
                setValue(packetHead, "a", Integer.valueOf(entityID));
                setValue(packetHead, "b", Byte.valueOf(getFixRotation(yaw)));

                sendPacket(packet);
                sendPacket(packetHead);
            }
        }
    }

    public static void destroy(int entityID) {
        for (Player target : Bukkit.getOnlinePlayers()) {
            if (IDprov.get(target.getName() + entityID) != null) {
                int npcId = IDprov.get(target.getName() + entityID).getIDNpcPlayer();
                IDprov.get(target.getName() + entityID).setUUID("NULL");
                PacketPlayOutEntityDestroy packet = new PacketPlayOutEntityDestroy(npcId);
                sendPacket(packet);
            }
        }
    }

    private static int getFixLocation(double pos) {
        return MathHelper.floor(pos * 32.0D);
    }

    private static byte getFixRotation(float yawpitch) {
        return (byte) ((int) (yawpitch * 256.0F / 360.0F));
    }

    private static void setValue(Object obj, String name, Object value) {
        try {
            Field field = obj.getClass().getDeclaredField(name);
            field.setAccessible(true);
            field.set(obj, value);
        } catch (Exception e) {
        }
    }

    public static Object getValue(Object obj, String name) {
        try {
            Field field = obj.getClass().getDeclaredField(name);
            field.setAccessible(true);
            return field.get(obj);
        } catch (Exception e) {
        }
        return null;
    }

    private static void sendPacket(Packet<?> packet, Player player) {
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
    }

    private static void sendPacket(Packet<?> packet) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            sendPacket(packet, player);
        }
    }
}
