package com.hubfinal.NPC;

import org.bukkit.Location;

import java.util.List;

public class RegionsNpcUtil {

    public static RegionNpc playerInArea(Location loc) {

        List<RegionNpc> regionList = RegionsNpc.getTerrains();
        for (RegionNpc rg : regionList) {
            if (!rg.getUUID().equals("NULL")) {

                double p1x = rg.getP1x();
                double p1y = rg.getP1y();
                double p1z = rg.getP1z();
                double p2x = rg.getP2x();
                double p2y = rg.getP2y();
                double p2z = rg.getP2z();

                double minX = p1x < p2x ? p1x : p2x;
                double minY = p1y < p2y ? p1y : p2y;
                double minZ = p1z < p2z ? p1z : p2z;

                double maxX = p1x > p2x ? p1x : p2x;
                double maxY = p1y > p2y ? p1y : p2y;
                double maxZ = p1z > p2z ? p1z : p2z;

                if ((loc.getBlockX() >= minX) && (loc.getBlockX() <= maxX) && (loc.getBlockY() >= minY)
                        && (loc.getBlockY() <= maxY) && (loc.getBlockZ() >= minZ) && (loc.getBlockZ() <= maxZ)) {

                    return rg;

                }
            }
        }
        return null;
    }
}
