package com.hubfinal.NPC;

import com.hubfinal.API.LoginAPI;
import com.hubfinal.API.PlayerUUID;
import com.hubfinal.API.ServerAPI;
import com.hubfinal.API.TagAPI;
import com.hubfinal.MySql;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class NPCFunctions {

    public static Object getExistMethod(String method, Player player) {

        for (int i = 1; i <= getLastFunctionName(); i++) {
            if (getFunctionsWithID(i).equals(method)) {
                if (method.equals("tp:mixedup")) {
                    if (LoginAPI.getLoggedStatus(PlayerUUID.getUUID(player.getName())) == true) {
                        if (ServerAPI.getManutencao("Factions") == true) {
                            if (TagAPI.getTag(PlayerUUID.getUUID(player.getName())).equalsIgnoreCase("Membro") || TagAPI.getTag(PlayerUUID.getUUID(player.getName())).equalsIgnoreCase("Vip") || TagAPI.getTag(PlayerUUID.getUUID(player.getName())).equalsIgnoreCase("Vip+")
                                    || TagAPI.getTag(PlayerUUID.getUUID(player.getName())).equalsIgnoreCase("Vip++") || TagAPI.getTag(PlayerUUID.getUUID(player.getName())).equalsIgnoreCase("Vip+++")) {
                                player.sendMessage(ChatColor.RED + "Ops, este servidor está em manutenção no momento!");
                                player.getOpenInventory().close();
                                return null;
                            }
                        }
                        if (ServerAPI.getOnlineStatus("Factions") == true) {
                            //addPlayerToQueue(player);
                            //sendCurrentPosition();
                        } else {
                            player.sendMessage(ChatColor.RED + "Ops, este servidor aparentemente está offline no momento!");
                            player.getOpenInventory().close();
                        }
                    }
                }

                i = getLastFunctionName() + 10;
                break;
            }
        }
        return null;
    }

    public static int getLastFunctionName() {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT max(ID) FROM Npc_functions");
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static String getFunctionsWithID(int id) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npc_functions WHERE ID = ?");
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("FunctionName");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getFunctionExist(String function) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npc_functions WHERE FunctionName = ?");
            st.setString(1, function);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("FunctionName");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getFunctionsWithEntityID(int EntityID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data WHERE EntityID = ?");
            st.setInt(1, EntityID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("FunctionName");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void updateFunctionNPC(int EntityID, String function) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("UPDATE Npcs_data SET FunctionName = ? WHERE EntityID = ?");
            st.setInt(2, EntityID);
            st.setString(1, function);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
