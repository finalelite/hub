package com.hubfinal.NPC;

import com.hubfinal.API.PlayerUUID;
import com.hubfinal.Main;
import com.hubfinal.provisional.IDprov;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.util.Vector;

public class PlayerMoveNPCUpdate implements Listener {

    @EventHandler
    public void onMove(PlayerMoveEvent event) {
        Player player = event.getPlayer();
        String UUID = PlayerUUID.getUUID(player.getName());

        if (NpcVerify.get(UUID) == null) {
            new NpcVerify(UUID, true).insert();
            NPC.enableNPCS(player);
        } else if (NpcVerify.get(UUID).getStatus() == false) {
            NpcVerify.get(UUID).setStatus(true);
            NPC.enableNPCS(player);
        }

        if (RegionsNpcUtil.playerInArea(player.getLocation()) != null) {
            if (!RegionsNpcUtil.playerInArea(player.getLocation()).getUUID().equals("NULL")) {
                String entityID = RegionsNpcUtil.playerInArea(player.getLocation()).getUUID();

                if (NpcCacheHR.get(NpcCacheLoc.get(entityID).getLocation()).getStatus() == true) {
                    String location = (RegionsNpcUtil.playerInArea(player.getLocation()).getP1x() + 4) + ":" + RegionsNpcUtil.playerInArea(player.getLocation()).getPy() + ":" + (RegionsNpcUtil.playerInArea(player.getLocation()).getP1z() + 4);

                    if (LastNpcView.get(player.getName()) == null) {
                        new LastNpcView(player.getName(), location).insert();
                    } else {
                        LastNpcView.get(player.getName()).setLocation(location);
                    }

                    if (NpcCacheLastV.get(location).getLastViewer().equals("NULL")) {
                        NpcCacheLastV.get(location).setLastViewer(player.getName());

                        Vector to = new Vector(player.getLocation().getX(), player.getLocation().getY(), player.getLocation().getZ());
                        Vector from = new Vector(RegionsNpcUtil.playerInArea(player.getLocation()).getP1x() + 4, RegionsNpcUtil.playerInArea(player.getLocation()).getPy(), RegionsNpcUtil.playerInArea(player.getLocation()).getP1z() + 4);
                        Vector v = to.subtract(from);
                        int x = -26;
                        int y = 64;
                        int z = -24;

                        for (Player target : Bukkit.getOnlinePlayers()) {
                            int id = IDprov.get(target.getName() + entityID).getIDNpcPlayer();
                            NPC.headRotation(Integer.valueOf(id), getLookAtYaw(v) + 92, getLookAtPitch(v));
                        }

                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                            @Override
                            public void run() {
                                NpcCacheLastV.get(location).setLastViewer("NULL");
                            }
                        }, 1200L);
                    } else {
                        if (NpcCacheLastV.get(location).getLastViewer().equals(player.getName())) {
                            Vector to = new Vector(player.getLocation().getX(), player.getLocation().getY(), player.getLocation().getZ());
                            Vector from = new Vector(RegionsNpcUtil.playerInArea(player.getLocation()).getP1x() + 4, RegionsNpcUtil.playerInArea(player.getLocation()).getPy(), RegionsNpcUtil.playerInArea(player.getLocation()).getP1z() + 4);
                            Vector v = to.subtract(from);
                            int x = -26;
                            int y = 64;
                            int z = -24;

                            for (Player target : Bukkit.getOnlinePlayers()) {
                                int id = IDprov.get(target.getName() + entityID).getIDNpcPlayer();
                                NPC.headRotation(Integer.valueOf(id), getLookAtYaw(v) + 92, getLookAtPitch(v));
                            }
                        }
                    }
                }
            }
        } else {
            if (LastNpcView.get(player.getName()) != null) {
                if (!LastNpcView.get(player.getName()).getLocation().equals("NULL")) {
                    if (NpcCacheLastV.get(LastNpcView.get(player.getName()).getLocation()) != null) {
                        NpcCacheLastV.get(LastNpcView.get(player.getName()).getLocation()).setLastViewer("NULL");
                        LastNpcView.get(player.getName()).setLocation("NULL");
                    }
                }
            }
        }
    }

    public static float getLookAtYaw(Vector motion) {
        double dx = motion.getX();
        double dz = motion.getZ();
        double yaw = 0;

        if (dx != 0) {
            if (dx < 0) {
                yaw = 1.5 * Math.PI;
            } else {
                yaw = 0.5 * Math.PI;
            }
            yaw -= Math.atan(dz / dx);
        } else if (dz < 0) {
            yaw = Math.PI;
        }
        return (float) (-yaw * 180 / Math.PI - 90);
    }

    public static float getLookAtPitch(Vector motion) {
        double dx = motion.getX();
        double dz = motion.getZ();
        double dy = motion.getY();

        double cateto = Math.sqrt(dx * dx + dz * dz);
        double hipotenuza = Math.sqrt(cateto * cateto + dy * dy);
        double resultado = Math.toDegrees(dy / hipotenuza);

        if (resultado < 0) {
            return (float) -(resultado);
        } else if (resultado > 0) {
            double rs = resultado;
            return (float) (rs - resultado - resultado);
        } else if (resultado == 0) {
            return (float) resultado;
        }

        return 0;
    }
}
