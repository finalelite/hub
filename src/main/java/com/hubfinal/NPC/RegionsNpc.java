package com.hubfinal.NPC;

import java.util.ArrayList;
import java.util.List;

public class RegionsNpc {

    private static List<RegionNpc> terrain = new ArrayList<>();

    public static RegionNpc getTerrain(String UUID) {
        for (RegionNpc terrain : getTerrains()) {
            if (terrain.getUUID().equalsIgnoreCase(UUID)) {
                return terrain;
            }
        }
        return null;
    }

    public static List<RegionNpc> getTerrains() {
        return terrain;
    }

    public static void setTerrains(List<RegionNpc> terrain) {
        RegionsNpc.terrain = terrain;
    }
}
