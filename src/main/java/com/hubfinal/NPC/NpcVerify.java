package com.hubfinal.NPC;

import java.util.HashMap;

public class NpcVerify {

    private String UUID;
    private boolean status;
    private static final HashMap<String, NpcVerify> CACHE = new HashMap<String, NpcVerify>();

    public NpcVerify(String UUID, boolean status) {
        this.UUID = UUID;
        this.status = status;
    }

    public NpcVerify insert() {
        CACHE.put(String.valueOf(this.UUID), this);

        return this;
    }

    public boolean getStatus() {
        return this.status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getUUID() {
        return this.UUID;
    }

    public static NpcVerify get(String UUID) {
        return CACHE.get(String.valueOf(UUID));
    }
}
