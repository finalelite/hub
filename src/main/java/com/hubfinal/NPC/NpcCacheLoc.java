package com.hubfinal.NPC;

import java.util.HashMap;

public class NpcCacheLoc {

    private String ID;
    private String Location;
    private static final HashMap<String, NpcCacheLoc> CACHE = new HashMap<String, NpcCacheLoc>();

    public NpcCacheLoc(String ID, String Location) {
        this.ID = ID;
        this.Location = Location;
    }

    public NpcCacheLoc insert() {
        CACHE.put(String.valueOf(this.ID), this);

        return this;
    }

    public String getLocation() {
        return this.Location;
    }

    public void setLocation(String Location) {
        this.Location = Location;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getID() {
        return this.ID;
    }

    public static NpcCacheLoc get(String ID) {
        return CACHE.get(String.valueOf(ID));
    }
}
