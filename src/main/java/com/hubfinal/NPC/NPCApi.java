package com.hubfinal.NPC;

import com.hubfinal.MySql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class NPCApi {

    public static void setNpc(int ID, String Location, int npcID, String text, String nick, String world, String visao, boolean movehead, String lastviewer, String value, String signature) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Npcs_data(ID, Location, EntityID, Texto, UUID, World, Visao, MoveHead, LastViewer, FunctionName, Value, Signature) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            st.setInt(1, ID);
            st.setString(2, Location);
            st.setInt(3, npcID);
            st.setString(4, text);
            st.setString(5, nick);
            st.setString(6, world);
            st.setString(7, visao);
            st.setBoolean(8, movehead);
            st.setString(9, lastviewer);
            st.setString(10, "NULL");
            st.setString(11, value);
            st.setString(12, signature);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void removeNpc(int npcID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("DELETE FROM Npcs_data WHERE EntityID = ?");
            st.setInt(1, npcID);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static String getValueSkin(String location) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data WHERE Location = ?");
            st.setString(1, location);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Value");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getSignatureSkin(String location) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data WHERE Location = ?");
            st.setString(1, location);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Signature");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getLastViewerNPC(String location) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data WHERE Location = ?");
            st.setString(1, location);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("LastViewer");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getIamLastViewerNPC(String UUID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data WHERE LastViewer = ?");
            st.setString(1, UUID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("LastViewer");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getTextNpc(String location) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data WHERE Location = ?");
            st.setString(1, location);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Texto");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getNickNpc(String location) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data WHERE Location = ?");
            st.setString(1, location);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Nick");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static int getNpcID(String location) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data WHERE Location = ?");
            st.setString(1, location);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt("EntityID");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static int getNpcIDD(String location) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data WHERE Location = ?");
            st.setString(1, location);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt("ID");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static int getNpcIDWithEntityID(int EntityID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data WHERE EntityID = ?");
            st.setInt(1, EntityID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt("ID");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static String getNpcVisao(String location) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data WHERE Location = ?");
            st.setString(1, location);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Visao");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getNpcVisaoID(int ID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data WHERE ID = ?");
            st.setInt(1, ID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Visao");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getExistNpcHere(String location) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data WHERE Location = ?");
            st.setString(1, location);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Location");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getNpcsLocations() {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Location");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getWorldNpcHere(String location) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data WHERE Location = ?");
            st.setString(1, location);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("World");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getWorldNpcID(int ID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data WHERE ID = ?");
            st.setInt(1, ID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("World");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getTextNpcID(int ID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data WHERE ID = ?");
            st.setInt(1, ID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Texto");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getUUIDNpcID(int ID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data WHERE ID = ?");
            st.setInt(1, ID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("UUID");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getUUIDNpcWithNPCId(int NpcID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data WHERE EntityID = ?");
            st.setInt(1, NpcID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("UUID");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getUUIDNpc(String location) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data WHERE Location = ?");
            st.setString(1, location);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("UUID");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static int getNpcIDD(int ID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data WHERE ID = ?");
            st.setInt(1, ID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt("EntityID");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static String getLocationID(int ID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data WHERE ID = ?");
            st.setInt(1, ID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Location");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getLocationEntityID(int EntityID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data WHERE EntityID = ?");
            st.setInt(1, EntityID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Location");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getVisaoEntityID(int EntityID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data WHERE EntityID = ?");
            st.setInt(1, EntityID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Visao");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getWorldEntityID(int EntityID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data WHERE EntityID = ?");
            st.setInt(1, EntityID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("World");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getTextoNpcWithNPCId(int NpcID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data WHERE EntityID = ?");
            st.setInt(1, NpcID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Texto");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void updateLastViewerNPC(String location, String lastviewer) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("UPDATE Npcs_data SET LastViewer = ? WHERE Location = ?");
            st.setString(2, location);
            st.setString(1, lastviewer);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateID(int ID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("UPDATE Npcs_data SET ID = ? WHERE ID = ?");
            st.setInt(2, ID);
            st.setInt(1, ID - 1);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void removeLastViewerNPC(String UUID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("UPDATE Npcs_data SET LastViewer = ? WHERE LastViewer = ?");
            st.setString(2, UUID);
            st.setString(1, "NULL");
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateTextoNPC(String location, String text) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("UPDATE Npcs_data SET Texto = ? WHERE Location = ?");
            st.setString(2, location);
            st.setString(1, text);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateSkinNPC(String location, String UUID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("UPDATE Npcs_data SET UUID = ? WHERE Location = ?");
            st.setString(2, location);
            st.setString(1, UUID);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateNPCId(String location, int newid) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("UPDATE Npcs_data SET EntityID = ? WHERE Location = ?");
            st.setString(2, location);
            st.setInt(1, newid);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static boolean getHeadRotateNPC(String location) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data WHERE Location = ?");
            st.setString(1, location);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getBoolean("MoveHead");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean getHeadRotateNPCWithEntityID(int EntityID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Npcs_data WHERE EntityID = ?");
            st.setInt(1, EntityID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getBoolean("MoveHead");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void updateHeadRotateNPC(String location, boolean movehead) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("UPDATE Npcs_data SET MoveHead = ? WHERE Location = ?");
            st.setString(2, location);
            st.setBoolean(1, movehead);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateNPCLocation(String location, String newloc) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("UPDATE Npcs_data SET Location = ? WHERE Location = ?");
            st.setString(2, location);
            st.setString(1, newloc);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateNPCVisao(String location, String visao) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("UPDATE Npcs_data SET Visao = ? WHERE Location = ?");
            st.setString(2, location);
            st.setString(1, visao);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static int getNpcIDProv(String player) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Player_npcs WHERE Player = ?");
            st.setString(1, player);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt("NpcID");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static void setNpcIDProv(String player, int npcID, String UUID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Player_npcs (Player, NpcID, UUID) VALUES (?, ?, ?)");
            st.setString(1, player);
            st.setInt(2, npcID);
            st.setString(3, UUID);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
