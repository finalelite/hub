package com.hubfinal.NPC;

import java.util.HashMap;

public class RegionNpc {

    private String UUID;
    private double p1x;
    private double p1y;
    private double p1z;
    private double p2x;
    private double p2y;
    private double p2z;
    private double py;
    private String world;
    private static HashMap<String, RegionNpc> CACHE = new HashMap<String, RegionNpc>();

    public RegionNpc(String UUID, double p1x, double p1y, double p1z, double p2x, double p2y, double p2z, double py, String world) {
        this.UUID = UUID;
        this.p1x = p1x;
        this.p1y = p1y;
        this.p1z = p1z;
        this.p2x = p2x;
        this.p2y = p2y;
        this.p2z = p2z;
        this.py = py;
        this.world = world;
    }

    public static RegionNpc get(String UUID) {
        return CACHE.get(UUID);
    }

    public RegionNpc insert() {
        CACHE.put(this.UUID, this);

        return this;
    }

    public String getUUID() {
        return UUID;
    }

    public void setUUID(String UUID) {
        this.UUID = UUID;
    }

    public String getWorld() {
        return world;
    }

    public void setWorld(String world) {
        this.world = world;
    }

    public double getP1x() {
        return p1x;
    }

    public void setP1x(double p1x) {
        this.p1x = p1x;
    }

    public double getPy() {
        return py;
    }

    public void setPy(double py) {
        this.py = py;
    }

    public double getP1y() {
        return p1y;
    }

    public void setP1y(double p1y) {
        this.p1y = p1y;
    }

    public double getP1z() {
        return p1z;
    }

    public void setP1z(double p1z) {
        this.p1z = p1z;
    }

    public double getP2x() {
        return p2x;
    }

    public void setP2x(double p2x) {
        this.p2x = p2x;
    }

    public double getP2y() {
        return p2y;
    }

    public void setP2y(double p2y) {
        this.p2y = p2y;
    }

    public double getP2z() {
        return p2z;
    }

    public void setP2z(double p2z) {
        this.p2z = p2z;
    }
}
