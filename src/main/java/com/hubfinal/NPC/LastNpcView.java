package com.hubfinal.NPC;

import java.util.HashMap;

public class LastNpcView {

    private String player;
    private String Location;
    private static final HashMap<String, LastNpcView> CACHE = new HashMap<String, LastNpcView>();

    public LastNpcView(String player, String Location) {
        this.player = player;
        this.Location = Location;
    }

    public LastNpcView insert() {
        CACHE.put(String.valueOf(this.player), this);

        return this;
    }

    public String getLocation() {
        return this.Location;
    }

    public void setLocation(String Location) {
        this.Location = Location;
    }

    public void setPlayer(String player) {
        this.player = player;
    }

    public String getPlayer() {
        return this.player;
    }

    public static LastNpcView get(String player) {
        return CACHE.get(String.valueOf(player));
    }

    public static LastNpcView getIamLocation(String Location) {
        return CACHE.get(String.valueOf(Location));
    }
}
