package com.hubfinal.NPC;

import java.util.HashMap;

public class NpcCacheHR {

    private String Location;
    private boolean status;
    private static final HashMap<String, NpcCacheHR> CACHE = new HashMap<String, NpcCacheHR>();

    public NpcCacheHR(String Location, boolean status) {
        this.Location = Location;
        this.status = status;
    }

    public NpcCacheHR insert() {
        CACHE.put(String.valueOf(this.Location), this);

        return this;
    }

    public boolean getStatus() {
        return this.status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public void setLocation(String Location) {
        this.Location = Location;
    }

    public String getLocation() {
        return this.Location;
    }

    public static NpcCacheHR get(String Location) {
        return CACHE.get(String.valueOf(Location));
    }
}
