package com.hubfinal;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;
import com.hubfinal.API.ServerAPI;
import com.hubfinal.API.TagAPI;
import com.hubfinal.chat.MessagesAutomatics;
import com.hubfinal.utils.CentralizeMsg;
import com.hubfinal.utils.Chat;
import com.hubfinal.utils.RestartUtils;
import com.hubfinal.utils.ScoreboardManager;
import com.hubfinal.vipAutentication.AuthAPI;
import com.hubfinal.vipAutentication.MySqlWeb;
import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.messaging.PluginMessageListener;

import java.time.Instant;

public class Main extends JavaPlugin implements Listener, PluginMessageListener {

    public static Main instance;
    public static Plugin plugin;
    public static int playerCount = 0;
    public static int maxPlayer = 0;
    public static String ip = "149.56.243.159";
    public static RestartUtils rUtils = new RestartUtils();
    public static Instant startedDate;
    public static ScoreboardManager scoreboardManager = new ScoreboardManager();

    //    private EntityPlayer npc;


    public void onEnable() {
        MySql.connect();
        MySqlWeb.connect();
        instance = this;
        plugin = this;
        saveDefaultConfig();
        setMaxPlayer(getConfig().getInt("servers.mixedup.players"));
        Chat.chatStatus = false;
        MessagesAutomatics.enable();
        this.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
        this.getServer().getMessenger().registerIncomingPluginChannel(this, "BungeeCord", this);
        Bukkit.getPluginManager().registerEvents(this, this);
        RegisterCommands.register();
        RegisterEvents.register();
        RegisterUtils.register();
        // rUtils.startTask();
        startedDate = Instant.now();
    }

    public static Main getInstance() {
        return instance;
    }

    public void onDisable() {
        ServerAPI.updateOnlineStatus(ServerAPI.getServerName(Bukkit.getServer().getPort()), false);
        ServerAPI.updateOnline(ServerAPI.getServerName(Bukkit.getServer().getPort()), 0);
        MySql.disconnect();
        MySqlWeb.disconnect();
    }

    @Override
    public void onPluginMessageReceived(String channel, Player player, byte[] message) {
        if (!channel.equals("BungeeCord")) {
            return;
        }
        ByteArrayDataInput in = ByteStreams.newDataInput(message);
        String subchannel = in.readUTF();

        if (subchannel.equals("PlayerCount")) {
            in.readUTF();
            playerCount = in.readInt();
        }
    }

    public static void setMaxPlayer(int max) {

        maxPlayer = max;

    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void fullServerJoin(PlayerLoginEvent event) {
        val player = event.getPlayer();
        val tag = TagAPI.getTag(player.getUniqueId().toString());

        if (tag == null)
            return;

        if (AuthAPI.getVip(player.getName()) == true) {
            if (event.getResult() == PlayerLoginEvent.Result.KICK_FULL)
                event.allow();
        }

        if (!tag.equalsIgnoreCase("membro")) {
            if (event.getResult() == PlayerLoginEvent.Result.KICK_FULL)
                event.allow();
        }

    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        event.getPlayer().setPlayerListHeader(ChatColor.translateAlternateColorCodes('&', " \n&6&lFINALELITE\n "));
        event.getPlayer().setPlayerListFooter(CentralizeMsg.sendCenteredMessage("\n                         " +
                "&6Twitter: &fwww.twitter.com/FinalElite                          " +
                "\n&6Discord: &fbit.ly/FinalEliteDiscord" +
                "\n &6Loja: &fwww.finalelite.com.br\n "));

    }
}
