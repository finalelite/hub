package com.hubfinal.chat;

import com.hubfinal.API.PlayerUUID;
import com.hubfinal.API.TagAPI;
import com.hubfinal.utils.Chat;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandsChat implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        Player player = (Player) sender;
        String UUID = PlayerUUID.getUUID(player.getName());

        // /chat limpar - /chat off - /chat on
        if (cmd.getName().equalsIgnoreCase("chat")) {
            if (TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor") || TagAPI.getTag(UUID).equals("Admin")) {
                if (args.length > 1 || args.length < 1) {
                    player.sendMessage(ChatColor.RED + "Comando referentes a Chat: " + "\n" + " " + "\n" + ChatColor.GRAY + "/chat limpar" + "\n" + ChatColor.GRAY + "/chat (on/off)");
                    return false;
                }
                if (args.length == 1) {
                    if (args[0].equals("limpar")) {
                        for (int i = 0; i < 100; i++) {
                            Bukkit.broadcastMessage(" ");
                        }
                        Bukkit.broadcastMessage(ChatColor.YELLOW + " * Chat limpo!");
                    } else if (args[0].equals("on")) {
                        Chat.setStatus(false);
                        player.sendMessage(ChatColor.GREEN + "Chat habilitado com sucesso!");
                    } else if (args[0].equals("off")) {
                        Chat.setStatus(true);
                        player.sendMessage(ChatColor.GREEN + "Chat desabilitado com sucesso!");
                    }
                }
            }
            return false;
        }
        return false;
    }
}
