package com.hubfinal.chat;

import com.destroystokyo.paper.event.player.PlayerAdvancementCriterionGrantEvent;
import com.hubfinal.API.LoginAPI;
import com.hubfinal.API.PlayerUUID;
import com.hubfinal.API.TagAPI;
import com.hubfinal.utils.AlternateColor;
import com.hubfinal.utils.Chat;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.Objects;

public class ChatGlobalEvent implements Listener {

    @EventHandler
    public void messageLeft(PlayerQuitEvent event) {
        event.setQuitMessage(null);
    }

    @EventHandler
    public void onAchiement(PlayerAdvancementCriterionGrantEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onSay(AsyncPlayerChatEvent event) {
        if (event.isCancelled()) {
            event.setCancelled(true);
            return;
        }

        Player player = event.getPlayer();
        String UUID = PlayerUUID.getUUID(player.getName());
        String sended = event.getMessage().replace("%", "%%");
        String msg = AlternateColor.alternate(sended);

        if (Chat.getStatus()) {
            if (!Objects.requireNonNull(TagAPI.getTag(UUID)).equals("Master") || !Objects.requireNonNull(TagAPI.getTag(UUID)).equals("Supervisor") || !Objects.requireNonNull(TagAPI.getTag(UUID)).equals("Admin") || !Objects.requireNonNull(TagAPI.getTag(UUID)).equals("Moderador") || !Objects.requireNonNull(TagAPI.getTag(UUID)).equals("Suporte") || !Objects.requireNonNull(TagAPI.getTag(UUID)).equals("Youtuber")) {
                event.setCancelled(true);
                player.sendMessage(ChatColor.RED + "O chat está temporariamente desabilitado!");
                return;
            }
        }

        if (LoginAPI.getLoggedStatus(UUID)) {
            if (Objects.requireNonNull(TagAPI.getTag(UUID)).equals("Master")) {
                event.setFormat(ChatColor.GOLD + "[GM] " + player.getName() + ChatColor.DARK_GRAY + " : " + ChatColor.GRAY + msg);
            }

            if (Objects.requireNonNull(TagAPI.getTag(UUID)).equals("Supervisor")) {
                event.setFormat(ChatColor.DARK_RED + "[Supervisor] " + player.getName() + ChatColor.DARK_GRAY + " : " + ChatColor.GRAY + msg);
            }

            if (Objects.requireNonNull(TagAPI.getTag(UUID)).equals("Admin")) {
                event.setFormat(ChatColor.RED + "[Admin] " + player.getName() + ChatColor.DARK_GRAY + " : " + ChatColor.GRAY + msg);
            }

            if (Objects.requireNonNull(TagAPI.getTag(UUID)).equals("Moderador")) {
                event.setFormat(ChatColor.DARK_GREEN + "[Moderador] " + player.getName() + ChatColor.DARK_GRAY + " : " + ChatColor.GRAY + msg);
            }

            if (Objects.requireNonNull(TagAPI.getTag(UUID)).equals("Suporte")) {
                event.setFormat(ChatColor.GREEN + "[Suporte] " + player.getName() + ChatColor.DARK_GRAY + " : " + ChatColor.GRAY + msg);
            }

            if (Objects.requireNonNull(TagAPI.getTag(UUID)).equals("Youtuber")) {
                event.setFormat(ChatColor.RED + "[Youtuber] " + player.getName() + ChatColor.DARK_GRAY + " : " + ChatColor.GRAY + msg);
            }

            if (Objects.requireNonNull(TagAPI.getTag(UUID)).equals("Vip")) {
                event.setFormat(ChatColor.YELLOW + "[Conde] " + player.getName() + ChatColor.DARK_GRAY + " : " + ChatColor.GRAY + msg);
            }

            if (Objects.requireNonNull(TagAPI.getTag(UUID)).equals("Vip+")) {
                event.setFormat(ChatColor.AQUA + "[Lord] " + player.getName() + ChatColor.DARK_GRAY + " : " + ChatColor.GRAY + msg);
            }

            if (Objects.requireNonNull(TagAPI.getTag(UUID)).equals("Vip++")) {
                event.setFormat(ChatColor.DARK_RED + "[Duque] " + player.getName() + ChatColor.DARK_GRAY + " : " + ChatColor.GRAY + msg);
            }

            if (Objects.requireNonNull(TagAPI.getTag(UUID)).equals("Vip+++")) {
                event.setFormat(ChatColor.DARK_PURPLE + "[Titan] " + player.getName() + ChatColor.DARK_GRAY + " : " + ChatColor.GRAY + msg);
            }

            if (Objects.requireNonNull(TagAPI.getTag(UUID)).equals("Membro")) {
                event.setFormat(ChatColor.GRAY + player.getName() + " : " + ChatColor.GRAY + sended);
            }
        }
    }
}
