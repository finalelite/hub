package com.hubfinal.chat;

import com.hubfinal.API.ConfigAPI;
import com.hubfinal.API.LoginAPI;
import com.hubfinal.API.PlayerUUID;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandTell implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        Player player = (Player) sender;

        if (cmd.getName().equalsIgnoreCase("tell") || cmd.getName().equalsIgnoreCase("msg")) {
            if (args.length < 2) {
                player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/tell (nick) (msg)");
                return false;
            }
            if (LoginAPI.getUUID(args[0]) != null) {
                String target = args[0];
                if(Bukkit.getPlayer(target) == null) {
                    player.sendMessage(ChatColor.RED + "Ops, este player aparentemente está offline no momento!");
                    return false;
                }

                String UUID = PlayerUUID.getUUID(target);

                StringBuilder build = new StringBuilder();
                for (int i = 1; i < args.length; i++) {
                    build.append(" ");
                    build.append(args[i]);
                }
                String msg = build.toString();

                if (player.getName().equals(target)) {
                    player.sendMessage(ChatColor.RED + "Ops, você está tentando falar consigo mesmo? Procure um médico.");
                    return false;
                }

                if (!ConfigAPI.getMsgPrivadas(UUID)) {
                    try {
                        Player targetPlayer = Bukkit.getPlayerExact(target);
                        targetPlayer.sendMessage(ChatColor.YELLOW + "[Tell] " + player.getName() + ChatColor.DARK_GRAY + " disse a você: " + ChatColor.GRAY + msg);
                        player.sendMessage(ChatColor.YELLOW + "[Tell] " + ChatColor.DARK_GRAY + "Você disse à " + ChatColor.YELLOW + target + ChatColor.DARK_GRAY + ": " + ChatColor.GRAY + msg);

                    } catch (Exception e) {
                        player.sendMessage(ChatColor.RED + "Ops, este player aparentemente está offline no momento!");
                    }
                } else {
                    player.sendMessage(ChatColor.RED + "Este player não deseja receber mensagens privadas no momento.");
                    return false;
                }
            } else {
                player.sendMessage(ChatColor.RED + "Ops, este player não contém um registro em nosso servidor!");
                return false;
            }
        }
        return false;
    }
}
