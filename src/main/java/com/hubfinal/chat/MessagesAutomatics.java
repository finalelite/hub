package com.hubfinal.chat;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class MessagesAutomatics {

    public static void enable() {
        messages();
        //actionBar();
    }

    private static int time = 0;

    private static void messages() {
        Thread thread = new Thread(() -> {
            while(true) {
                try {
                    Thread.sleep(1000 * 90);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (time == 0) {
                    for (Player target : Bukkit.getOnlinePlayers()) {
                        target.sendMessage(
                                "\n\n \n\n §7* §cPrecisa de ajuda?\n \n §7* Utilize: §c/ajuda §7ou contate um de nossos §a[SUPORTES]\n\n \n\n ");
                    }
                } else if (time == 1) {
                    for (Player target : Bukkit.getOnlinePlayers()) {
                        target.sendMessage(
                                "\n\n \n\n §7* Encontrou algum §aBug §7ou §cHacker§7?\n \n §7* Reporte os utilizando: \n §7* §c/reportar ou reporte via discord aba (#suporte)\n\n \n\n ");
                    }
                } else if (time == 2) {
                    for (Player target : Bukkit.getOnlinePlayers()) {
                        target.sendMessage(
                                "\n\n \n\n §7* Adquira §aVIPs§7 e muito mais em nosso site:\n §7* §cwww.finalelite.com.br§7.\n\n \n\n ");
                    }
                } else if (time == 3) {
                    for (Player target : Bukkit.getOnlinePlayers()) {
                        target.sendMessage(
                                "\n\n \n\n §c* MEGA PROMOÇÃO DE ESTREIA!\n \n §7* Venha aproveitar a mega promoção de estreia \n §7* §a30% OFF §7em toda a loja, acesse no site e aproveite!\n §7* §cwww.finalelite.com.br§7.\n\n \n\n ");
                    }
                } else if (time == 4) {
                    for (Player target : Bukkit.getOnlinePlayers()) {
                        target.sendMessage(
                                "\n\n \n\n §c* Esperando uma vaga para logar-se no servidor?\n \n §7* Utilize /1v1 e batalhe enquanto espera uma vaga!\n §7* Ou acesse nosso site e adquira VIP, e obtenha \n §7* privilégios como este!\n §7* §cwww.finalelite.com.br§7.\n\n \n\n ");
                    }
                }
                time++;

                if (time > 4) {
                    time = 0;
                }
            }
        });
        thread.start();
    }

    /*
    private static void actionBar() {
        Thread thread = new Thread(() -> {
            while(true) {
                try {
                    Thread.sleep(1000 * 45);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                for (Player target : Bukkit.getOnlinePlayers()) {
                    ActionBar.send(target, AlternateColor.alternate(ChatColor.RED + "Servidor em versão BETA!"));
                    target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 100.0f);
                }
            }
        });
        thread.start();
    }
    */
}
