package com.hubfinal.listeners;

import br.com.finalelite.pauloo27.api.PauloAPI;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import java.util.Arrays;
import java.util.List;

public class PreProcessCommand implements Listener {

    private List<String> blocks = Arrays.asList("/help", "/plugins", "/me", "/pl", "/seed", "/icanhasbukkit");

    @EventHandler
    public void onCommandProcess(PlayerCommandPreprocessEvent e) {

        if(e.getMessage().startsWith("/")) {

            String cmd = e.getMessage().split(" ")[0];

            Player p = e.getPlayer();

            if(cmd.contains(":") || blocks.contains(cmd)) {

                if(PauloAPI.getInstance().getAccountInfo(p).getRole().isMajorStaff()) return;

                p.sendMessage(ChatColor.RED + " * Você não pode utilizar este comando.");
                e.setCancelled(true);

            }

        }

    }

}
