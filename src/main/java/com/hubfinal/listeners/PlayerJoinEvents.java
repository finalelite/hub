package com.hubfinal.listeners;

import br.com.finalelite.pauloo27.api.PauloAPI;
import com.hubfinal.API.*;
import com.hubfinal.Main;
import com.hubfinal.NPC.NPCApi;
import com.hubfinal.NPC.NPCEvents;
import com.hubfinal.NPC.NpcVerify;
import com.hubfinal.commands.CommandSetspawn;
import com.hubfinal.provisional.IDNpc;
import com.hubfinal.utils.*;
import lombok.val;
import org.bukkit.*;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeModifier;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class PlayerJoinEvents implements Listener {

    @EventHandler
    public void onLeft(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        String UUID = PlayerUUID.getUUID(player.getName());

        if (UUID == null) return;

        if (NPCApi.getIamLastViewerNPC(player.getName()) != null && NPCApi.getIamLastViewerNPC(player.getName()) != null) {
            NPCApi.removeLastViewerNPC(player.getName());
        }

        if (NpcVerify.get(UUID) != null && NpcVerify.get(UUID).getStatus() == true) {
            NpcVerify.get(UUID).setStatus(false);
        }

        if (OnRowUtil.get(UUID) != null) {
            OnRowUtil.get(UUID).setOnRow(false);
        }

        if (LoginTasks.tasks.containsKey(player)) {
            LoginTasks.tasks.get(player).cancel();
            LoginTasks.tasks.remove(player);
        }

        if(ConfigAPI.hasFlyCache(player.getUniqueId().toString()))
            ConfigAPI.removeFromFlyCache(player.getUniqueId().toString());

        TagAPI.getCache().remove(UUID);

        val score = Main.scoreboardManager;
        Bukkit.getOnlinePlayers().forEach(ps -> score.getScoreboard().updateEntry(ps, "online"));

    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        if (UUIDcache.get(player.getName()) == null) {
            new UUIDcache(player.getName(), PlayerUUID.getUUID(player.getName())).insert();
        }
        String UUID = PlayerUUID.getUUID(player.getName());

        player.getAttribute(Attribute.GENERIC_ATTACK_SPEED).addModifier(new AttributeModifier("", 9.9999999E7D, AttributeModifier.Operation.ADD_NUMBER));

        //---------------------------------------
        if (LoginAPI.getUUID(player.getName()) == null) {
            LoginAPI.setInfos(UUID, player.getName());
        }
//        else if (!LoginAPI.getNickName(UUID).equalsIgnoreCase(player.getName())) {
//            LoginAPI.updateNickName(UUID, player.getName());
//        }

        //---------------------------------------

        //---------------------------------------
        if (ConfigAPI.getFly(UUID)) {
            player.setAllowFlight(true);
        }
        //---------------------------------------

        //---------------------------------------
        if (TagAPI.getTag(UUID) == null) {
            TagAPI.setTag(UUID, "Membro");
        }
        //---------------------------------------

        //---------------------------------------
        new IsReportingBug(UUID, false).insert();
        new IsReportingHack(UUID, false).insert();
        //---------------------------------------

        //---------------------------------------
        if (CommandSetspawn.getLoc("Hub") != null) {
            String infos = CommandSetspawn.getLoc("Hub");
            String[] infosList = infos.split("/");
            Location loc = new Location(Bukkit.getWorld(infosList[0]), Double.parseDouble(infosList[1]), Double.parseDouble(infosList[2]), Double.parseDouble(infosList[3]), Float.valueOf(infosList[4]), Float.valueOf(infosList[5]));
            player.teleport(loc);
        } else {
            Bukkit.broadcastMessage(ChatColor.RED + "[Administrador] Defina uma area para spawn!");
        }
        //---------------------------------------

        //---------------------------------------
        if (TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor") || TagAPI.getTag(UUID).equals("Admin") || TagAPI.getTag(UUID).equals("Moderador") || TagAPI.getTag(UUID).equals("Suporte")) {
            if (ServerAPI.getStaffOnlineExist(UUID) == null) {
                ServerAPI.setStaffOnline(UUID, player.getName(), TagAPI.getTag(UUID));
            }
        }
        //---------------------------------------

        //---------------------------------------
        for (int i = 0; i < 100; i++) {
            player.sendMessage(" ");
        }
        //---------------------------------------

        //---------------------------------------
        for (Player target : Bukkit.getOnlinePlayers()) {
            //if (AccountAPI.getPremiumNick(target.getName()) == false) {
            if (ConfigAPI.getVanish(target.getUniqueId().toString())) {
                player.hidePlayer(target);
                target.hidePlayer(player);
            }
            // } else {
            //                if (ConfigAPI.getVanish(UUID) == true) {
            //                    player.hidePlayer(target);
            //                    target.hidePlayer(player);
            //                }
            //            }
        }
        //---------------------------------------

        //---------------------------------------
        event.setJoinMessage(null);
        //---------------------------------------

        //---------------------------------------
        player.setHealth(20);
        player.setFoodLevel(20);
        //---------------------------------------

        //---------------------------------------
        if (ConfigAPI.getExist(UUID) == null) {
            ConfigAPI.setInfos(UUID, false, false, false, false, 1, false);
        }
        //---------------------------------------

        //---------------------------------------
        player.setGameMode(GameMode.ADVENTURE);
        player.getInventory().clear();
        //---------------------------------------

        //---------------------------------------
        if (!LoginAPI.getLoggedStatus(UUID)) {
            boolean isReg = LoginAPI.getPassword(UUID) != null;

            player.sendMessage(ChatColor.GREEN + "Ao registrar e/ou jogar em nosso servidor você aceita os nossos termos de uso e nossa constituição. Você pode ler a mesma em " + ChatColor.GRAY + "https://finalelite.com.br/docs/constituicao" + ChatColor.GREEN +".");

            if (isReg) {
                player.sendMessage(ChatColor.GREEN + "Utilize o comando: " + ChatColor.GRAY + "/logar (senha)");
            } else {
                player.sendMessage(ChatColor.GREEN + "Utilize o comando: " + ChatColor.GRAY + "/registrar (senha)");
            }

            AtomicInteger warns = new AtomicInteger();
            BukkitTask loginTask = new BukkitRunnable() {
                @Override
                public void run() {
                    int times = warns.getAndIncrement();
                    if (times >= 4) {
                        String msg = CentralizeMsg.sendCenteredMessage(ChatColor.RED + "§lFinal Elite\n \n§cVocê foi kickado por demorar\n§ca se registrar!");
                        Bukkit.getPlayer(player.getName()).kickPlayer(msg);
                        cancel();
                    }

                    if (isReg) {
                        player.sendMessage(ChatColor.GREEN + "Utilize o comando: " + ChatColor.GRAY + "/logar (senha)");
                    } else {
                        player.sendMessage(ChatColor.GREEN + "Utilize o comando: " + ChatColor.GRAY + "/registrar (senha)");
                    }
                }
            }.runTaskTimer(Main.plugin, 300l, 300l);

            LoginTasks.tasks.put(player, loginTask);
        } else {
            setRocket(player, UUID);

            Bukkit.getScheduler().runTaskLater(Main.plugin, () -> {
                if (LoginAPI.getEmail(UUID) == null) {
                    player.sendMessage(AlternateColor.alternate("&e * Foi verificado que você ainda não possui um cadastro de email em sua atual conta. Para evitarmos problemas futuros utilize o comando &e&n/cadastrar email (email)." + "\n" + "&e "));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                }
            }, 60L);

            setItems(player);
        }
        //---------------------------------------

        //---------------------------------------
//        if (!LoginAPI.getNickName(UUID).equals(player.getName())) {
//            LoginAPI.updateNickName(UUID, player.getName());
//        }
        //---------------------------------------

        //---------------------------------------
        if (!LoginAPI.getLoggedStatus(UUID)) {
            for (Player target : Bukkit.getOnlinePlayers()) {
                player.hidePlayer(target);
                target.hidePlayer(player);
            }
        }
        //---------------------------------------

        //---------------------------------------
        new IDNpc(player.getName(), 0).insert();
        //---------------------------------------

        //---------------------------------------
        boolean bo = false;
        if (!bo) {
            NPCEvents npc = new NPCEvents(player);
            npc.inject();
            bo = true;
        }
        //---------------------------------------

        //---------------------------------------

        val score = Main.scoreboardManager;
        score.createScoreBoard(player);
        score.setScoreboard(player);
        val tablist = score.getTablistManager();
        tablist.setPlayerGroup(player, tablist.getGroupIdByName(PauloAPI.getInstance().getAccountInfo(player).getRole().getDisplayName()));
        score.setTablist(player);

        score.updateTablist(player);
        Bukkit.getOnlinePlayers().forEach(ps -> score.getScoreboard().updateEntry(ps, "online"));

        //---------------------------------------
    }

    public static void setItems(Player player) {
        ItemStack compass = new ItemStack(Material.COMPASS);
        ItemMeta compassMeta = compass.getItemMeta();
        compassMeta.setDisplayName(ChatColor.YELLOW + "Servidores");
        compass.setItemMeta(compassMeta);

        ItemStack slime = new ItemStack(Material.SLIME_BALL);
        ItemMeta slimeMeta = slime.getItemMeta();
        slimeMeta.setDisplayName(ChatColor.YELLOW + "Lobbies");
        slime.setItemMeta(slimeMeta);

        ItemStack anvil = new ItemStack(Material.ANVIL);
        ItemMeta anvilMeta = anvil.getItemMeta();
        anvilMeta.setDisplayName(ChatColor.RED + "Configurações");
        anvil.setItemMeta(anvilMeta);

        player.getInventory().clear();
        player.getInventory().setItem(1, anvil);
        player.getInventory().setItem(4, compass);
        player.getInventory().setItem(7, EnchantGlow.glow(slime));
    }

    public static void setRocket(Player player, String UUID) {
        if (ConfigAPI.getElytra(UUID) == true) {
            Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                @Override
                public void run() {
                    player.getInventory().setChestplate(new ItemStack(Material.ELYTRA));
                    ItemStack firework = new ItemStack(Material.FIREWORK_ROCKET);
                    ItemMeta meta = firework.getItemMeta();
                    meta.setDisplayName(ChatColor.YELLOW + "Foguete");
                    firework.setItemMeta(meta);
                    player.getInventory().setItem(3, firework);
                    player.getInventory().setItem(5, firework);
                }
            }, 10L);
        }
    }

    public static class LoginTasks {
        public static HashMap<Player, BukkitTask> tasks = new HashMap<>();
    }
}
