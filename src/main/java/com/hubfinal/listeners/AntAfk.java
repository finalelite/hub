package com.hubfinal.listeners;

import com.hubfinal.API.PlayerUUID;
import com.hubfinal.utils.AfkHash;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class AntAfk implements Listener {

    @EventHandler
    public void onMove(PlayerMoveEvent event) {
        Player player = event.getPlayer();
        String UUID = PlayerUUID.getUUID(player.getName());

        if (AfkHash.get(UUID) == null) {
            new AfkHash(UUID, player.getLocation().getBlockX(), player.getLocation().getBlockY(), player.getLocation().getBlockZ(), 0).insert();
        } else {
            AfkHash.get(UUID).setX(player.getLocation().getBlockX());
            AfkHash.get(UUID).setY(player.getLocation().getBlockY());
            AfkHash.get(UUID).setZ(player.getLocation().getBlockZ());
            AfkHash.get(UUID).setTime(0);
        }
    }
}
