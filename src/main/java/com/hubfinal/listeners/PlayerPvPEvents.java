package com.hubfinal.listeners;

import com.hubfinal.events.hashs.CombatHash;
import com.hubfinal.events.hashs.InPvPHash;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;

public class PlayerPvPEvents implements Listener {

    @EventHandler
    public void onPvP(EntityDamageByEntityEvent event) {
        if (event.getEntity() instanceof Player && event.getDamager() instanceof Player) {
            Player player = (Player) event.getEntity();
            Player target = (Player) event.getDamager();

            if (CombatHash.get(player.getUniqueId().toString()) != null && CombatHash.get(player.getUniqueId().toString()).getTarget().equalsIgnoreCase(target.getUniqueId().toString())) {
                if (CombatHash.get(target.getUniqueId().toString()) != null && CombatHash.get(target.getUniqueId().toString()).getTarget().equalsIgnoreCase(player.getUniqueId().toString())) {
                    event.setCancelled(false);
                    return;
                }
            }
        }
        event.setCancelled(true);
    }

    @EventHandler
    public void onDamage(EntityDamageEvent event) {
        if (event.getEntity() instanceof Player) {
            Player player = (Player) event.getEntity();
            if (InPvPHash.get(player.getUniqueId().toString()) != null) return;
        }
        event.setCancelled(true);
    }
}
