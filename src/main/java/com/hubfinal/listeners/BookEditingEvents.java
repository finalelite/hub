package com.hubfinal.listeners;

import com.hubfinal.API.PlayerUUID;
import com.hubfinal.API.ReportsAPI;
import com.hubfinal.API.ServerAPI;
import com.hubfinal.Main;
import com.hubfinal.MySql;
import com.hubfinal.utils.*;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerEditBookEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class BookEditingEvents implements Listener {

    @EventHandler
    public void onEditing(PlayerEditBookEvent event) {

        Player player = event.getPlayer();
        String UUID = PlayerUUID.getUUID(player.getName());

        ItemStack bookBug = new ItemStack(Material.WRITABLE_BOOK);
        ItemMeta bookBugMeta = bookBug.getItemMeta();
        bookBugMeta.setDisplayName(ChatColor.GREEN + "Reporte de bug");
        bookBug.setItemMeta(bookBugMeta);

        ItemStack bookHack = new ItemStack(Material.WRITABLE_BOOK);
        ItemMeta bookHackMeta = bookHack.getItemMeta();
        bookHackMeta.setDisplayName(ChatColor.RED + "Reporte de hack");
        bookHack.setItemMeta(bookHackMeta);

        if (event.getPlayer().getItemInHand().getItemMeta().getDisplayName().equals(ChatColor.RED + "Reporte de hack")) {
            if (event.getNewBookMeta().hasPages() && event.getNewBookMeta().hasTitle()) {
                if (PlayerUUID.getUUID(player.getName()) == null) {
                    player.sendMessage(ChatColor.RED + "Ops, este player informado aparentemente não existe.");
                } else {
                    //RESOLVER ISTO DEPOIS!
                    if (ReportsAPI.getExistReportHack(UUID) != null) {
                        if (ReportsAPI.getExistReportHack(UUID).equals(PlayerUUID.getUUID(event.getNewBookMeta().getTitle()))) {
                            player.sendMessage(ChatColor.RED + "Ops, você aparentemente já reportou este usuário.");
                            IsReportingHack.get(UUID).setIsReporting(false);
                            player.getInventory().removeItem(player.getItemInHand().clone());
                            return;
                            //--------------------
                        } else {
                            if (PlayerUUID.getUUID(event.getNewBookMeta().getTitle()).equals(UUID)) {
                                player.sendMessage(ChatColor.RED + "Ops, você não pode se auto reportar!");
                                IsReportingHack.get(UUID).setIsReporting(false);
                                player.getInventory().removeItem(player.getItemInHand().clone());
                            } else {
                                player.sendMessage(ChatColor.GREEN + "Reporte efetuado com sucesso.");
                                IsReportingHack.get(UUID).setIsReporting(false);
                                event.getNewBookMeta().setTitle(ChatColor.RED + "Suspeito: " + event.getNewBookMeta().getTitle());
                                event.getNewBookMeta().setAuthor(player.getName());
                                player.getInventory().removeItem(player.getItemInHand().clone());

                                if (event.getNewBookMeta().getPages().size() != 1) {
                                    String text = "";
                                    for (int i = 1; i <= event.getNewBookMeta().getPages().size(); i++) {
                                        text = text + "/" + event.getNewBookMeta().getPage(i);
                                    }
                                    ReportsAPI.createReport("Hack", text, UUID, PlayerUUID.getUUID(event.getNewBookMeta().getTitle()));
                                    player.getInventory().removeItem(player.getItemInHand().clone());

                                    for (int i = 0; i <= getLastID(); i++) {

                                        String nick = ServerAPI.getStaffOnlineID(i);

                                        try {
                                            ByteArrayOutputStream b = new ByteArrayOutputStream();
                                            DataOutputStream out = new DataOutputStream(b);

                                            String l0 = ChatColor.RED + " ---== Um reporte acaba de ser efetuado. ==---";
                                            String l1 = ChatColor.RED + "*  Tipo: " + ChatColor.GRAY + "Reporte hack";
                                            String l2 = ChatColor.RED + "*  Reportador: " + ChatColor.GRAY + player.getName();
                                            String l3 = ChatColor.RED + "*  Suspeito: " + ChatColor.GRAY + event.getNewBookMeta().getTitle();
                                            String l4 = AlternateColor.alternate("&c&nVerificar reporte &7");
                                            String line0 = CentralizerText.centralize(l0, 67, " ");
                                            String line1 = CentralizerText.centralize(l1, 72, " ");
                                            String line2 = CentralizerText.centralize(l2, 72, " ");
                                            String line3 = CentralizerText.centralize(l3, 72, " ");
                                            String line4 = CentralizerText.centralize(l4, 75, " ");


                                            out.writeUTF("Message");
                                            out.writeUTF(nick);
                                            out.writeUTF(" " + "\n" + " " + "\n" + line0 + "\n" + " " + line1 + "\n" + line2 + "\n" + line3 + "\n" + " " + "\n" + line4 + "\n" + " " + "\n" + " ");

                                            player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                } else {
                                    ReportsAPI.createReport("Hack", event.getNewBookMeta().getPage(1), UUID, PlayerUUID.getUUID(event.getNewBookMeta().getTitle()));
                                    player.getInventory().removeItem(player.getItemInHand().clone());

                                }
                            }
                        }
                    } else {
                        if (PlayerUUID.getUUID(event.getNewBookMeta().getTitle()).equals(UUID)) {
                            player.sendMessage(ChatColor.RED + "Ops, você não pode se auto reportar!");
                            IsReportingHack.get(UUID).setIsReporting(false);
                            player.getInventory().removeItem(player.getItemInHand().clone());
                        } else {
                            player.sendMessage(ChatColor.GREEN + "Reporte efetuado com sucesso.");
                            IsReportingHack.get(UUID).setIsReporting(false);
                            event.getNewBookMeta().setTitle(ChatColor.RED + "Suspeito: " + event.getNewBookMeta().getTitle());
                            event.getNewBookMeta().setAuthor(player.getName());
                            player.getInventory().remove(player.getInventory().getItem(5));

                            if (event.getNewBookMeta().getPages().size() != 1) {
                                String text = "";
                                for (int i = 1; i <= event.getNewBookMeta().getPages().size(); i++) {
                                    text = text + "/" + event.getNewBookMeta().getPage(i);
                                }
                                ReportsAPI.createReport("Hack", text, UUID, PlayerUUID.getUUID(event.getNewBookMeta().getTitle()));
                                player.getInventory().removeItem(player.getItemInHand().clone());
                            } else {
                                ReportsAPI.createReport("Hack", event.getNewBookMeta().getPage(1), UUID, PlayerUUID.getUUID(event.getNewBookMeta().getTitle()));
                                player.getInventory().removeItem(player.getItemInHand().clone());
                            }

                            for (int i = 0; i <= getLastID(); i++) {

                                String nick = ServerAPI.getStaffOnlineID(i);

                                try {
                                    ByteArrayOutputStream b = new ByteArrayOutputStream();
                                    DataOutputStream out = new DataOutputStream(b);

                                    String l0 = ChatColor.RED + " ---== Um reporte acaba de ser efetuado. ==---";
                                    String l1 = ChatColor.RED + "*  Tipo: " + ChatColor.GRAY + "Reporte hack";
                                    String l2 = ChatColor.RED + "*  Reportador: " + ChatColor.GRAY + player.getName();
                                    String l3 = ChatColor.RED + "*  Suspeito: " + ChatColor.GRAY + event.getNewBookMeta().getTitle();
                                    String l4 = AlternateColor.alternate("&c&nVerificar reporte &7");
                                    String line0 = CentralizerText.centralize(l0, 67, " ");
                                    String line1 = CentralizerText.centralize(l1, 72, " ");
                                    String line2 = CentralizerText.centralize(l2, 72, " ");
                                    String line3 = CentralizerText.centralize(l3, 72, " ");
                                    String line4 = CentralizerText.centralize(l4, 75, " ");


                                    out.writeUTF("Message");
                                    out.writeUTF(nick);
                                    out.writeUTF(" " + "\n" + " " + "\n" + line0 + "\n" + " " + line1 + "\n" + line2 + "\n" + line3 + "\n" + " " + "\n" + line4 + "\n" + " " + "\n" + " ");

                                    player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                }
            } else {
                String text = null;

                for (int x = 1; x <= event.getNewBookMeta().getPages().size(); x++) {
                    if (text == null) {
                        text = event.getNewBookMeta().getPage(x);
                    } else {
                        text = text + ":" + event.getNewBookMeta().getPage(x);
                    }
                }

                new ReportingUtil(UUID, event.getNewBookMeta().getPages().size(), text).insert();

                ItemStack bookH = new ItemStack(Material.WRITABLE_BOOK);
                BookMeta bookHMeta = (BookMeta) bookH.getItemMeta();
                bookHMeta.setDisplayName(ChatColor.RED + "Reporte de hack");
                for (int y = 1; y <= ReportingUtil.get(UUID).getPaginas(); y++) {
                    if (ReportingUtil.get(UUID).getText().contains(":")) {
                        String[] txt = ReportingUtil.get(UUID).getText().split(":");
                        bookHMeta.setPages(txt);
                    } else {
                        bookHMeta.setPages(ReportingUtil.get(UUID).getText());
                    }
                }
                bookH.setItemMeta(bookHMeta);

                event.setCancelled(true);
                player.getInventory().setItem(5, bookH);
                player.sendMessage(ChatColor.RED + " * Ops, por favor nos informe no campo 'Title' o nome do suspeito. Siga este tutorial em caso de dúvida:" + "\n" + ChatColor.AQUA + "    https://www.youtube.com/watch?XXXX");
                return;
            }
        }

        if (event.getPlayer().getItemInHand().getItemMeta().getDisplayName().equals(ChatColor.GREEN + "Reporte de bug")) {
            if (event.getNewBookMeta().hasPages()) {
                player.sendMessage(ChatColor.GREEN + "Reporte efetuado com sucesso.");
                IsReportingBug.get(UUID).setIsReporting(false);

                if (event.getNewBookMeta().getPages().size() != 1) {
                    String text = "";
                    for (int i = 1; i <= event.getNewBookMeta().getPages().size(); i++) {
                        text = text + "/" + event.getNewBookMeta().getPage(i);
                    }
                    ReportsAPI.createReport("Bug", text, UUID, null);
                    player.getInventory().removeItem(player.getItemInHand().clone());
                } else {
                    ReportsAPI.createReport("Bug", event.getNewBookMeta().getPage(1), UUID, "NULL");
                    player.getInventory().removeItem(player.getItemInHand().clone());
                }

                for (int i = 0; i <= getLastID(); i++) {

                    String nick = ServerAPI.getStaffOnlineID(i);

                    try {
                        ByteArrayOutputStream b = new ByteArrayOutputStream();
                        DataOutputStream out = new DataOutputStream(b);

                        String l0 = ChatColor.GREEN + " ---== Um reporte acaba de ser efetuado. ==---";
                        String l1 = ChatColor.GREEN + "*  Tipo: " + ChatColor.GRAY + "Reporte bug";
                        String l2 = ChatColor.GREEN + "*  Reportador: " + ChatColor.GRAY + player.getName();
                        String l3 = AlternateColor.alternate("&a&nVerificar reporte &7");
                        String line0 = CentralizerText.centralize(l0, 67, " ");
                        String line1 = CentralizerText.centralize(l1, 72, " ");
                        String line2 = CentralizerText.centralize(l2, 72, " ");
                        String line3 = CentralizerText.centralize(l3, 75, " ");


                        out.writeUTF("Message");
                        out.writeUTF(nick);
                        out.writeUTF(" " + "\n" + " " + "\n" + line0 + "\n" + " " + line1 + "\n" + line2 + "\n" + " " + "\n" + line3 + "\n" + " " + "\n" + " ");

                        player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private static int getLastID() {
        int id = 0;

        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT max(ID) FROM Staff_online");
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                id = rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id;
    }
}
