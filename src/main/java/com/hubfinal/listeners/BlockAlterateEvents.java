package com.hubfinal.listeners;

import com.hubfinal.API.TagAPI;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

public class BlockAlterateEvents implements Listener {

    @EventHandler
    public void onPlace(BlockPlaceEvent event) {
        if (TagAPI.getTag(event.getPlayer().getUniqueId().toString()).equalsIgnoreCase("Master")) {
            event.setCancelled(false);
        } else {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onBreak(BlockBreakEvent event) {
        if (TagAPI.getTag(event.getPlayer().getUniqueId().toString()).equalsIgnoreCase("Master")) {
            event.setCancelled(false);
        } else {
            event.setCancelled(true);
        }
    }
}
