package com.hubfinal.listeners;

import com.hubfinal.API.PlayerUUID;
import com.hubfinal.managers.InventoryConfigs;
import com.hubfinal.managers.InventoryServers;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.util.Vector;

public class PlayerInteractEvents implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {

        Player player = event.getPlayer();
        String UUID = PlayerUUID.getUUID(player.getName());

        if (event.getAction().name().contains("RIGHT_CLICK")) {
            if (event.getItem() == null) return;
            if (event.getItem().getItemMeta().getDisplayName().equals(ChatColor.YELLOW + "Foguete")) {
                if (event.getPlayer().isGliding()) {
                    event.setCancelled(true);
                    Location loc = player.getLocation();
                    Vector dir = loc.getDirection().add(new Vector(0.0D, 0.1D, 0.0D));
                    player.setVelocity(player.getVelocity().add(dir));
                    player.playSound(player.getLocation(), Sound.ENTITY_FIREWORK_ROCKET_LAUNCH, 1.0f, 1.0f);
                }
            }
            if (event.getItem().getItemMeta().getDisplayName().equals(ChatColor.RED + "Configurações")) {
                player.openInventory(InventoryConfigs.inv(UUID));
            }

            if (event.getItem().getItemMeta().getDisplayName().equals(ChatColor.YELLOW + "Servidores")) {
                player.openInventory(InventoryServers.inv("Factions"));
            }

            if (event.getItem().getItemMeta().getDisplayName().equals(ChatColor.YELLOW + "Lobbies")) {
                player.sendMessage(ChatColor.RED + "Mudança de lobby desativada.");
//                SocketUtil.online();
//                player.openInventory(InventoryLobbys.inv(UUID));
            }
        }
    }
}
