package com.hubfinal.listeners;

import com.hubfinal.API.LoginAPI;
import com.hubfinal.API.PlayerUUID;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

public class PlayerCommandReprocess implements Listener {

    @EventHandler
    public void onCommand(PlayerCommandPreprocessEvent event) {
        Player player = event.getPlayer();
        String UUID = PlayerUUID.getUUID(player.getName());

        if (!LoginAPI.getLoggedStatus(UUID)) {
            if (LoginAPI.getPassword(UUID) == null) {
                if (!event.getMessage().contains("/registrar") && !event.getMessage().contains("/register")) {
                    player.sendMessage(ChatColor.RED + "Ops, vamos nos cadastrar antes de tentarmos utilizar outros comandos.");
                    event.setCancelled(true);
                }
            } else {
                if (LoginAPI.getPassword(UUID) == null) {
                    if (!event.getMessage().contains("/registrar") && !event.getMessage().contains("/register")) {
                        player.sendMessage(ChatColor.RED + "Ops, vamos nos cadastrar antes de tentarmos utilizar outros comandos.");
                        event.setCancelled(true);
                    }
                } else {
                    if (!event.getMessage().contains("/logar") && !event.getMessage().contains("/login")) {
                        player.sendMessage(ChatColor.RED + "Ops, vamos nos logar antes de tentarmos utilizar outros comandos.");
                        event.setCancelled(true);
                    }
                }
            }
        }
    }

    @EventHandler
    public void onSay(AsyncPlayerChatEvent event) {

        Player player = event.getPlayer();
        String UUID = PlayerUUID.getUUID(player.getName());

        if (!LoginAPI.getLoggedStatus(UUID)) {
            event.setCancelled(true);

            if (LoginAPI.getPassword(UUID) == null) {
                player.sendMessage(ChatColor.RED + "Ops, vamos nos cadastrar antes de você tentar se comunicar com outras pessoas.");
            } else {
                if (LoginAPI.getPassword(UUID) == null) {
                    player.sendMessage(ChatColor.RED + "Ops, vamos nos cadastrar antes de você tentar se comunicar com outras pessoas.");
                } else {
                    player.sendMessage(ChatColor.RED + "Ops, vamos nos logar antes de você tentar se comunicar com outras pessoas.");
                }
            }
        }
    }
}
