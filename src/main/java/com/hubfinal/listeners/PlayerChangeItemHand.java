package com.hubfinal.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerItemDamageEvent;
import org.bukkit.event.player.PlayerSwapHandItemsEvent;

public class PlayerChangeItemHand implements Listener {

    @EventHandler
    public void onChange(PlayerItemDamageEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onMoveItem(PlayerSwapHandItemsEvent event) {
        event.setCancelled(true);
    }
}
