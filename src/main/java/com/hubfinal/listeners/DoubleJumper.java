package com.hubfinal.listeners;

import com.hubfinal.API.ConfigAPI;
import com.hubfinal.API.LoginAPI;
import com.hubfinal.events.hashs.InPvPHash;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class DoubleJumper implements Listener {

    private boolean onFloor;

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event) {
        Player player = event.getPlayer();
        String UUID = player.getUniqueId().toString();

        if (InPvPHash.get(UUID) != null) return;

        Location loc = new Location(player.getWorld(), player.getLocation().getX(), player.getLocation().getY() - 1, player.getLocation().getZ());

        if (loc.getBlock().getType().equals(Material.SLIME_BLOCK)) {
            player.setVelocity(player.getLocation().getDirection().multiply(2.5).setY(1.5));

            playEffects(player);
        }

        if (!ConfigAPI.getFlySmart(UUID)) {
            if (player.getGameMode() != GameMode.CREATIVE && player.getLocation().subtract(0, 1, 0).getBlock().getType() != Material.AIR && !player.isFlying()) {
                player.setAllowFlight(true);
            }
        }
    }

    @EventHandler
    public void onPlayerToggleFlight(PlayerToggleFlightEvent event) {
        Player player = event.getPlayer();
        String UUID = LoginAPI.getUUID(player.getName());

        if (InPvPHash.get(UUID) != null) return;

        if (!ConfigAPI.getFlySmart(UUID)) {
            if (player.getGameMode() == GameMode.CREATIVE) {
                return;
            }

            event.setCancelled(true);
            player.setAllowFlight(false);
            player.setFlying(false);
            player.setVelocity(player.getLocation().getDirection().multiply(1.5).setY(1));

            playEffects(player);
        }
    }

    private void playEffects(Player player) {
        Random random = new Random();
        boolean randomBoolean = random.nextBoolean();

        if (randomBoolean) {
            player.playSound(player.getLocation(), Sound.ENTITY_BLAZE_SHOOT, 1.0F, 1.0F);
        } else {
            player.playSound(player.getLocation(), Sound.ENTITY_ENDER_DRAGON_SHOOT, 1.0F, 1.0F);
        }
    }

    private String UUID = "";

    public static void spawnParticles() {
        Thread thread = new Thread(() -> {
            while (true) {
                try {
                    Thread.sleep(750);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                for (int i = 0; i <= 45; i++) {
                    double randomX = 0;
                    double randomZ = 0;
                    if (i <= 5) {
                        randomX = ThreadLocalRandom.current().nextDouble(-16.500 - 0.800, -16.500 + 0.800);
                        randomZ = ThreadLocalRandom.current().nextDouble(-1.500 - 0.800, -1.500 + 0.800);
                    } else if (i <= 10) {
                        randomX = ThreadLocalRandom.current().nextDouble(-18.500 - 0.800, -18.500 + 0.800);
                        randomZ = ThreadLocalRandom.current().nextDouble(-1.500 - 0.800, -1.500 + 0.800);
                    } else if (i <= 15) {
                        randomX = ThreadLocalRandom.current().nextDouble(-18.500 - 0.800, -18.500 + 0.800);
                        randomZ = ThreadLocalRandom.current().nextDouble(-0.500 - 0.800, -0.500 + 0.800);
                    } else if (i <= 20) {
                        randomX = ThreadLocalRandom.current().nextDouble(-17.500 - 0.800, -17.500 + 0.800);
                        randomZ = ThreadLocalRandom.current().nextDouble(-0.500 - 0.800, -0.500 + 0.800);
                    } else if (i <= 25) {
                        randomX = ThreadLocalRandom.current().nextDouble(-16.500 - 0.800, -16.500 + 0.800);
                        randomZ = ThreadLocalRandom.current().nextDouble(-0.500 - 0.800, -0.500 + 0.800);
                    } else if (i <= 30) {
                        randomX = ThreadLocalRandom.current().nextDouble(-16.500 - 0.800, -16.500 + 0.800);
                        randomZ = ThreadLocalRandom.current().nextDouble(0.500 - 0.800, 0.500 + 0.800);
                    } else if (i <= 35) {
                        randomX = ThreadLocalRandom.current().nextDouble(-17.500 - 0.800, -17.500 + 0.800);
                        randomZ = ThreadLocalRandom.current().nextDouble(0.500 - 0.800, 0.500 + 0.800);
                    } else if (i <= 40) {
                        randomX = ThreadLocalRandom.current().nextDouble(-18.500 - 0.800, -18.500 + 0.800);
                        randomZ = ThreadLocalRandom.current().nextDouble(0.500 - 0.800, 0.500 + 0.800);
                    } else {
                        randomX = ThreadLocalRandom.current().nextDouble(-17.500 - 0.800, -17.500 + 0.800);
                        randomZ = ThreadLocalRandom.current().nextDouble(1.500 - 0.800, 1.500 + 0.800);
                    }
                    for (Player target : Bukkit.getOnlinePlayers()) {
                        Location loc = new Location(target.getWorld(), randomX, 64, randomZ);
                        target.spawnParticle(Particle.VILLAGER_HAPPY, loc, 1);
                    }
                }
            }
        });
        thread.start();
    }
}
