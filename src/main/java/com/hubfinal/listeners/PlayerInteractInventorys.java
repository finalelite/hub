package com.hubfinal.listeners;

import com.hubfinal.API.ConfigAPI;
import com.hubfinal.API.PlayerUUID;
import com.hubfinal.API.ServerAPI;
import com.hubfinal.API.TagAPI;
import com.hubfinal.Main;
import com.hubfinal.utils.IsReportingBug;
import com.hubfinal.utils.IsReportingHack;
import com.hubfinal.utils.JoinQueue;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class PlayerInteractInventorys implements Listener {

    private static String UUID = "";

    @EventHandler
    public void onInteract(InventoryClickEvent event) {
        final Player player = (Player) event.getWhoClicked();
        String UUID = PlayerUUID.getUUID(player.getName());

        if (event.getInventory().getName().equalsIgnoreCase("Reporte: ")) {
            event.setCancelled(true);

            ItemStack bookBug = new ItemStack(Material.WRITABLE_BOOK);
            ItemMeta bookBugMeta = bookBug.getItemMeta();
            bookBugMeta.setDisplayName(ChatColor.GREEN + "Reporte de bug");
            bookBug.setItemMeta(bookBugMeta);

            ItemStack bookHack = new ItemStack(Material.WRITABLE_BOOK);
            ItemMeta bookHackMeta = bookHack.getItemMeta();
            bookHackMeta.setDisplayName(ChatColor.RED + "Reporte de hack");
            bookHack.setItemMeta(bookHackMeta);

            if (event.getSlot() == 12) {
                if (IsReportingBug.get(UUID).getIsReporting() == true) {
                    player.sendMessage(ChatColor.GREEN + " * Utilize o livro em seu inventário para reportar o bug!");
                    player.getOpenInventory().close();
                    return;
                } else {
                    player.getOpenInventory().close();
                    player.sendMessage(" " + "\n" + ChatColor.GRAY + " * Utilize o " + ChatColor.GREEN + "livro" + ChatColor.GRAY + " cedido para criar o reporte. Siga este tutorial em caso de dúvida." + "\n" + ChatColor.AQUA + " * https://www.youtube.com/watch?XXXX" + "\n" + " ");
                    player.getInventory().setItem(3, bookBug);
                    IsReportingBug.get(UUID).setIsReporting(true);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            if (IsReportingBug.get(UUID).getIsReporting() == true) {
                                player.getInventory().removeItem(bookBug);
                                IsReportingBug.get(UUID).setIsReporting(false);
                                player.sendMessage(ChatColor.RED + "Ops, removemos o livro de reporte de seu inventário pela demora de efetuar o reporte!");
                            }
                        }
                    }, 3600L);
                }
            }

            if (event.getSlot() == 14) {
                if (IsReportingHack.get(UUID).getIsReporting() == true) {
                    player.sendMessage(ChatColor.GREEN + " * Utilize o livro em seu inventário para reportar o hack!");
                    player.getOpenInventory().close();
                    return;
                } else {
                    player.getOpenInventory().close();
                    player.sendMessage(" " + "\n" + ChatColor.GRAY + " * Utilize o " + ChatColor.GREEN + "livro" + ChatColor.GRAY + " cedido para criar o reporte. Siga este tutorial em caso de dúvida." + "\n" + ChatColor.AQUA + " * https://www.youtube.com/watch?XXXX" + "\n" + " ");
                    player.getInventory().setItem(5, bookHack);
                    IsReportingHack.get(UUID).setIsReporting(true);

                    Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            if (IsReportingHack.get(UUID).getIsReporting() == true) {
                                player.getInventory().removeItem(bookHack);
                                IsReportingHack.get(UUID).setIsReporting(false);
                                player.sendMessage(ChatColor.RED + "Ops, removemos o livro de reporte de seu inventário pela demora de efetuar o reporte!");
                            }
                        }
                    }, 3600L);
                }
            }
        }

        if (event.getInventory().getName().equalsIgnoreCase("Servidores: ")) {
            event.setCancelled(true);

            if (event.getSlot() == 13) {
                if (ServerAPI.getManutencao("Factions") == true) {
                    if (TagAPI.getTag(PlayerUUID.getUUID(player.getName())).equalsIgnoreCase("Membro") || TagAPI.getTag(PlayerUUID.getUUID(player.getName())).equalsIgnoreCase("Vip") || TagAPI.getTag(PlayerUUID.getUUID(player.getName())).equalsIgnoreCase("Vip+")
                            || TagAPI.getTag(PlayerUUID.getUUID(player.getName())).equalsIgnoreCase("Vip++") || TagAPI.getTag(PlayerUUID.getUUID(player.getName())).equalsIgnoreCase("Vip+++")) {
                        player.sendMessage(ChatColor.RED + "Ops, este servidor está em manutenção no momento!");
                        player.getOpenInventory().close();
                        return;
                    }
                }

                player.getOpenInventory().close();
                //InventoryUpdating.update("Factions");

                if (ServerAPI.getOnlineStatus("Factions") == true) {
                    if (JoinQueue.addPlayerToQueue(player))
                        JoinQueue.sendCurrentPosition();
                } else {
                    player.sendMessage(ChatColor.RED + "Ops, este servidor aparentemente está offline no momento!");
                    player.getOpenInventory().close();
                    return;
                }
            }
        }

            /*
        if (event.getInventory().getName().equalsIgnoreCase("Lobbys: ")) {
            event.setCancelled(true);

            if (event.getSlot() == 11) {
                if (LocationAPI.getServer(UUID).equals("Hub1")) {
                    player.getOpenInventory().close();
                    player.sendMessage(ChatColor.RED + "Ops, você ja está conectado a este lobby!");
                    return;
                } else {
                    if (ServerAPI.getManutencao("Hub1") == true) {
                        player.getOpenInventory().close();
                        player.sendMessage(ChatColor.RED + "Ops, este lobby está em manutenção no momento!");
                        return;
                    } else {
                        if (ServerAPI.getOnlineStatus("Hub1") == false) {
                            player.getOpenInventory().close();
                            player.sendMessage(ChatColor.RED + "Ops, este lobby está offline no momento!");
                            return;
                        } else {
                            try {
                                ByteArrayOutputStream b = new ByteArrayOutputStream();
                                DataOutputStream out = new DataOutputStream(b);

                                out.writeUTF("Connect");
                                out.writeUTF("Hub1");
                                ServerAPI.updateOnline(LocationAPI.getServer(UUID), ServerAPI.getOnlineServer(LocationAPI.getServer(UUID)) - 1);

                                player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                            } catch (Exception e) {
                                player.sendMessage(ChatColor.RED + "Ops, este servidor aparentemente está offline no momento!");
                            }
                        }
                    }
                }
            }

            if (event.getSlot() == 13) {
                if (LocationAPI.getServer(UUID).equals("Hub2")) {
                    player.getOpenInventory().close();
                    player.sendMessage(ChatColor.RED + "Ops, você ja está conectado a este lobby!");
                    return;
                } else {
                    if (ServerAPI.getManutencao("Hub2") == true) {
                        player.getOpenInventory().close();
                        player.sendMessage(ChatColor.RED + "Ops, este lobby está em manutenção no momento!");
                        return;
                    } else {
                        if (ServerAPI.getOnlineStatus("Hub2") == false) {
                            player.getOpenInventory().close();
                            player.sendMessage(ChatColor.RED + "Ops, este lobby está offline no momento!");
                            return;
                        } else {
                            try {
                                ByteArrayOutputStream b = new ByteArrayOutputStream();
                                DataOutputStream out = new DataOutputStream(b);

                                out.writeUTF("Connect");
                                out.writeUTF("Hub2");
                                ServerAPI.updateOnline(LocationAPI.getServer(UUID), ServerAPI.getOnlineServer(LocationAPI.getServer(UUID)) - 1);

                                player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                            } catch (Exception e) {
                                player.sendMessage(ChatColor.RED + "Ops, este servidor aparentemente está offline no momento!");
                            }
                        }
                    }
                }
            }

            if (event.getSlot() == 15) {
                if (LocationAPI.getServer(UUID).equals("Hub3")) {
                    player.getOpenInventory().close();
                    player.sendMessage(ChatColor.RED + "Ops, você ja está conectado a este lobby!");
                    return;
                } else {
                    if (ServerAPI.getManutencao("Hub3") == true) {
                        player.getOpenInventory().close();
                        player.sendMessage(ChatColor.RED + "Ops, este lobby está em manutenção no momento!");
                        return;
                    } else {
                        if (ServerAPI.getOnlineStatus("Hub3") == false) {
                            player.getOpenInventory().close();
                            player.sendMessage(ChatColor.RED + "Ops, este lobby está offline no momento!");
                            return;
                        } else {
                            try {
                                ByteArrayOutputStream b = new ByteArrayOutputStream();
                                DataOutputStream out = new DataOutputStream(b);

                                out.writeUTF("Connect");
                                out.writeUTF("Hub3");
                                ServerAPI.updateOnline(LocationAPI.getServer(UUID), ServerAPI.getOnlineServer(LocationAPI.getServer(UUID)) - 1);

                                player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                            } catch (Exception e) {
                                player.sendMessage(ChatColor.RED + "Ops, este servidor aparentemente está offline no momento!");
                            }
                        }
                    }
                }
            }
        }
        */

        if (event.getInventory().getName().equalsIgnoreCase("Configurações: ")) {
            event.setCancelled(true);

            if (event.getSlot() == 22) {
                if (ConfigAPI.getElytra(UUID) == false) {
                    ConfigAPI.updateElytra(UUID, true);
                    player.getOpenInventory().close();
                    player.sendMessage(ChatColor.GREEN + "Elytra, habilitada com sucesso!");
                    player.getInventory().setChestplate(new ItemStack(Material.ELYTRA));
                    ItemStack firework = new ItemStack(Material.FIREWORK_ROCKET);
                    ItemMeta meta = firework.getItemMeta();
                    meta.setDisplayName(ChatColor.YELLOW + "Foguete");
                    firework.setItemMeta(meta);
                    player.getInventory().setItem(3, firework);
                    player.getInventory().setItem(5, firework);
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, -5.0F);
                } else {
                    ConfigAPI.updateElytra(UUID, false);
                    player.getOpenInventory().close();
                    player.getInventory().setChestplate(new ItemStack(Material.AIR));
                    player.getInventory().setItem(3, new ItemStack(Material.AIR));
                    player.getInventory().setItem(5, new ItemStack(Material.AIR));
                    player.sendMessage(ChatColor.RED + "Elytra, desabilitada com sucesso!");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, -5.0F);
                }
            }

            if (event.getSlot() == 10) {
                if (!TagAPI.getTag(UUID).equals("Membro")) {
                    if (ConfigAPI.getFly(UUID) == false) {
                        ConfigAPI.updateFly(UUID, true);
                        player.getOpenInventory().close();
                        player.sendMessage(ChatColor.GREEN + "Fly, habilitado com sucesso!");
                        player.setAllowFlight(true);
                        player.setFlying(true);
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, -5.0F);
                    } else {
                        ConfigAPI.updateFly(UUID, false);
                        player.getOpenInventory().close();
                        player.sendMessage(ChatColor.RED + "Fly, desabilitado com sucesso!");
                        player.setAllowFlight(false);
                        player.setFlying(false);
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, -5.0F);
                    }
                } else {
                    if (ConfigAPI.getFly(UUID) == true) {
                        ConfigAPI.updateFly(UUID, false);
                        player.setAllowFlight(false);
                        player.setFlying(false);
                    }
                    player.getOpenInventory().close();
                    player.sendMessage(ChatColor.GRAY + "Adquira vantagens como essas adquirindo " + ChatColor.AQUA + "VIP" + ChatColor.GRAY + " em nosso site: " + "\n" + ChatColor.YELLOW + "loja.finalelite.com.br");
                }
            }

            if (event.getSlot() == 12) {
                if (ConfigAPI.getVanish(UUID) == false) {
                    ConfigAPI.updateVanish(UUID, true);
                    player.getOpenInventory().close();
                    player.sendMessage(ChatColor.RED + "Agora você não vê ninguem, e ninguem lhe vê :)");

                    for (Player target : Bukkit.getServer().getOnlinePlayers()) {
                        player.hidePlayer(target);
                        target.hidePlayer(player);
                    }
                } else {
                    ConfigAPI.updateVanish(UUID, false);
                    player.getOpenInventory().close();
                    player.sendMessage(ChatColor.GREEN + "Agora você vê a todos, e todos lhe vêm :)");

                    for (Player target : Bukkit.getServer().getOnlinePlayers()) {
                        player.showPlayer(target);
                        target.showPlayer(player);
                    }
                }
            }

            if (event.getSlot() == 13) {
                if (ConfigAPI.getChat(UUID) == false) {
                    ConfigAPI.updateChat(UUID, true);
                    player.getOpenInventory().close();
                    player.sendMessage(ChatColor.RED + "Chat desabilitado com sucesso!");
                } else {
                    ConfigAPI.updateChat(UUID, false);
                    player.getOpenInventory().close();
                    player.sendMessage(ChatColor.GREEN + "Chat habilitado com sucesso!");
                }
            }

            if (event.getSlot() == 14) {
                if (ConfigAPI.getMsgPrivadas(UUID) == false) {
                    ConfigAPI.updateMsgPrivadas(UUID, true);
                    player.getOpenInventory().close();
                    player.sendMessage(ChatColor.RED + "Mensagens privadas desabilitadas com sucesso!");
                } else {
                    ConfigAPI.updateMsgPrivadas(UUID, false);
                    player.getOpenInventory().close();
                    player.sendMessage(ChatColor.GREEN + "Mensagens privadas habilitadas com sucesso!");
                }
            }

            if (event.getSlot() == 16) {
                if (!TagAPI.getTag(UUID).equals("Membro")) {
                    if (ConfigAPI.getVelocidade(UUID) == 1) {
                        ConfigAPI.updateVelocidade(UUID, 2);
                        player.getOpenInventory().close();
                        player.sendMessage(ChatColor.GREEN + "Velocidade setado ao nível 2");
                        player.setWalkSpeed(0.3F);
                        player.setFlySpeed(0.3F);
                    } else if (ConfigAPI.getVelocidade(UUID) == 2) {
                        ConfigAPI.updateVelocidade(UUID, 3);
                        player.getOpenInventory().close();
                        player.sendMessage(ChatColor.GREEN + "Velocidade setado ao nível 3");
                        player.setWalkSpeed(0.4F);
                        player.setFlySpeed(0.4F);
                    } else if (ConfigAPI.getVelocidade(UUID) == 3) {
                        ConfigAPI.updateVelocidade(UUID, 1);
                        player.getOpenInventory().close();
                        player.sendMessage(ChatColor.GREEN + "Velocidade setado ao nível 1");
                        player.setWalkSpeed(0.2F);
                        player.setFlySpeed(0.2F);
                    }
                } else {
                    player.getOpenInventory().close();
                    player.sendMessage(ChatColor.GRAY + "Adquira vantagens como essas adquirindo " + ChatColor.AQUA + "VIP" + ChatColor.GRAY + " em nosso site: " + "\n" + ChatColor.YELLOW + "loja.finalelite.com.br");
                }
            }
        }
    }

}
