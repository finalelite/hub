package com.hubfinal.listeners;

import com.hubfinal.API.ServerAPI;
import com.hubfinal.API.TagAPI;
import com.hubfinal.utils.JoinQueue;
import lombok.val;
import net.citizensnpcs.api.event.NPCClickEvent;
import net.citizensnpcs.api.event.NPCLeftClickEvent;
import net.citizensnpcs.api.event.NPCRightClickEvent;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class CitizensListener implements Listener {

    @EventHandler
    public void onRightClick(NPCRightClickEvent event) {
        joinQueue(event);
    }

    @EventHandler
    public void onLeftClick(NPCLeftClickEvent event) {
        joinQueue(event);
    }

    public void joinQueue(NPCClickEvent event) {
        if (event.getNPC().getFullName().contains("Mixedup")) {
            val player = event.getClicker();
            val tag = TagAPI.getTag(player.getUniqueId().toString());

            if (ServerAPI.getManutencao("Factions") && (tag.equalsIgnoreCase("Membro") || tag.toLowerCase().contains("vip"))) {
                player.sendMessage(ChatColor.RED + "Servidor em manutenção.");
                return;
            }

            if (ServerAPI.getOnlineStatus("Factions")) {
                if (JoinQueue.addPlayerToQueue(player))
                    JoinQueue.sendCurrentPosition();
            } else {
                player.sendMessage(ChatColor.RED + "Ops, este servidor aparentemente está offline no momento!");
            }
        }
    }

}
