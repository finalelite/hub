package com.hubfinal;

import com.hubfinal.NPC.PlayerMoveNPCUpdate;
import com.hubfinal.chat.ChatGlobalEvent;
import com.hubfinal.events.listeners.PlayerDeathEvent;
import com.hubfinal.events.listeners.PlayerInteractAtPlayer;
import com.hubfinal.listeners.*;
import org.bukkit.Bukkit;

public class RegisterEvents {

    public static void register() {
        Bukkit.getPluginManager().registerEvents(new CitizensListener(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new PlayerCommandReprocess(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new PlayerJoinEvents(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new PlayerDropItemEvents(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new PlayerHungreEvents(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new PlayerPvPEvents(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new PlayerInteractEvents(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new ChatGlobalEvent(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new PlayerInteractInventorys(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new WeatherChangeEvents(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new BookEditingEvents(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new PlayerMoveNPCUpdate(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new DoubleJumper(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new BlockAlterateEvents(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new PlayerChangeItemHand(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new AntAfk(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new PreProcessCommand(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new PlayerDeathEvent(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new PlayerInteractAtPlayer(), Main.plugin);
    }
}
