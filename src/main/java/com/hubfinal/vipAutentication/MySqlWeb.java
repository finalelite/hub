package com.hubfinal.vipAutentication;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MySqlWeb {

    public static Connection con;

    public static void connect() {
        if (!isConnected()) {
            try {
                con = DriverManager.getConnection("jdbc:mysql://167.114.128.154/finalelite?autoReconnect=true&useSSL=false", "webintegration",
                        "WT84owgf00b4re48v0bEFV4f4FVne5f4");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void disconnect() {
        if (isConnected()) {
            try {
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static boolean isConnected() {
        return (con != null);
    }
}
