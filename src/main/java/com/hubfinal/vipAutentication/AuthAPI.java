package com.hubfinal.vipAutentication;

import com.hubfinal.MySql;
import lombok.val;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AuthAPI {

    public static void createIndo(String UUID, String vip, String server, boolean eterno, Date date) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Vips_data(UUID, Vip, Server, Eterno, Ends) VALUES (?, ?, ?, ?, ?)");
            st.setString(1, UUID);
            st.setString(2, vip);
            st.setString(3, server);
            st.setBoolean(4, eterno);
            st.setDate(5, date);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void removeVip(String UUID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("DELETE FROM Vips_data WHERE UUID = ?");
            st.setString(1, UUID);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static Date getDateEnds(String UUID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Vips_data WHERE UUID = ?");
            st.setString(1, UUID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getDate("Ends");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getTagVip(String UUID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Vips_data WHERE UUID = ?");
            st.setString(1, UUID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Vip");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getExistInfo(String UUID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Vips_data WHERE UUID = ?");
            st.setString(1, UUID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("UUID");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean getVip(String nick) {
        return getPaid(getUserId(nick));
    }

    private static int getUserId(String nick) {
        try {
            PreparedStatement st = MySqlWeb.con.prepareStatement("SELECT * FROM users WHERE username = ?");
            st.setString(1, nick);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt("id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static void removeVipWeb(String nick) {
        removePaid(getUserId(nick));
    }

    private static void removePaid(int id) {
        try {
            PreparedStatement st = MySqlWeb.con.prepareStatement("DELETE FROM invoices WHERE user_id = ?");
            st.setInt(1, id);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static boolean getPaid(int id) {
        boolean back = false;
        try {
            PreparedStatement st = MySqlWeb.con.prepareStatement("SELECT * FROM invoices WHERE user_id = ?");
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                if (back == false && rs.getBoolean("paid") == true) {
                    back = true;
                    break;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return back;
    }

    private static int getPriceId(int id) {
        try {
            PreparedStatement st = MySqlWeb.con.prepareStatement("SELECT * FROM invoices WHERE user_id = ?");
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                if (rs.getBoolean("paid") == true) {
                    return rs.getInt("price_id");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    private static Float getTotal(int id) {
        try {
            PreparedStatement st = MySqlWeb.con.prepareStatement("SELECT * FROM invoices WHERE user_id = ?");
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                if (rs.getBoolean("paid") == true) {
                    return rs.getFloat("total");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0F;
    }

    private static int getProductId(Float total) {
        try {
            PreparedStatement st = MySqlWeb.con.prepareStatement("SELECT * FROM prices WHERE total = ?");
            st.setFloat(1, total);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt("product_id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    private static String getProductTime(Float total) {
        try {
            PreparedStatement st = MySqlWeb.con.prepareStatement("SELECT * FROM prices WHERE total = ?");
            st.setFloat(1, total);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("name");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String getProductType(int productId) {
        try {
            PreparedStatement st = MySqlWeb.con.prepareStatement("SELECT * FROM products WHERE id = ?");
            st.setInt(1, productId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("name");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    //GETUSERID(24) > TOTAL(20) > PRODUCTID(14) > PRODUCTS(CONDE)
    public static String getProduct(String nick) {
        val priceId = getPriceId(getUserId(nick));

        val invoice = new Invoice(priceId);
        return br.com.finalelite.pauloo27.api.message.MessageUtils.capitalize(invoice.getVip().name().toLowerCase()) + " " + invoice.getType().toPtBR();
    }
}
