package com.hubfinal.vipAutentication;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum VIPRole {
    TITAN,
    DUQUE,
    LORD,
    CONDE;

    public static VIPRole fromVIPName(String name) {
        try {
            return VIPRole.valueOf(name.toUpperCase());
        } catch (IllegalArgumentException e) {
            return null;
        }
    }

    public static VIPRole fromId(byte id) {
        return VIPRole.values()[id];
    }

}
