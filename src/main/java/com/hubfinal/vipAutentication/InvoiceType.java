package com.hubfinal.vipAutentication;


public enum InvoiceType {
    ONE_MONTH,
    TWO_MONTHS,
    ETERNAL;

    public static InvoiceType fromId(byte id) {
        return InvoiceType.values()[id];
    }

    public String toPtBR() {
        if (this == ONE_MONTH)
            return "1";
        if (this == TWO_MONTHS)
            return "2";
        if (this == ETERNAL)
            return "Eterno";
        else
            return null;
    }

}
