package com.hubfinal;

import com.hubfinal.NPC.CommandsNPC;
import com.hubfinal.chat.CommandTell;
import com.hubfinal.chat.CommandsChat;
import com.hubfinal.commands.*;
import com.hubfinal.events.Command1v1;

public class RegisterCommands {

    public static void register() {
        Main.instance.getCommand("registrar").setExecutor(new CommandRegistrar());
        Main.instance.getCommand("register").setExecutor(new CommandRegistrar());
        Main.instance.getCommand("logar").setExecutor(new CommandLogar());
        Main.instance.getCommand("login").setExecutor(new CommandLogar());
        Main.instance.getCommand("cadastrar").setExecutor(new CommandCadastrar());
        Main.instance.getCommand("trocarsenha").setExecutor(new CommandTrocarSenha());
        Main.instance.getCommand("manutencao").setExecutor(new CommandManutencao());
        Main.instance.getCommand("mixedup").setExecutor(new CommandMixedup());        
        Main.instance.getCommand("tag").setExecutor(new CommandSettag());
        Main.instance.getCommand("msg").setExecutor(new CommandTell());
        Main.instance.getCommand("tell").setExecutor(new CommandTell());
        Main.instance.getCommand("chat").setExecutor(new CommandsChat());
        Main.instance.getCommand("setspawn").setExecutor(new CommandSetspawn());
        Main.instance.getCommand("npc").setExecutor(new CommandsNPC());
        Main.instance.getCommand("construir").setExecutor(new CommandConstruir());
        Main.instance.getCommand("fly").setExecutor(new CommandFly());
        Main.instance.getCommand("voar").setExecutor(new CommandFly());
        Main.instance.getCommand("gamemode").setExecutor(new CommandGamemode());
        Main.instance.getCommand("gm").setExecutor(new CommandGamemode());
        Main.instance.getCommand("ajuda").setExecutor(new CommandAjuda());
        Main.instance.getCommand("help").setExecutor(new CommandAjuda());
        Main.instance.getCommand("desligar").setExecutor(new CommandStop());
        Main.instance.getCommand("reiniciar").setExecutor(new CommandRestart());
        Main.instance.getCommand("1v1").setExecutor(new Command1v1());
        Main.instance.getCommand("resetarsenha").setExecutor(new CommandResetPassword());
        Main.instance.getCommand("setmax").setExecutor(new CommandMaxPlayers());
    }
}
