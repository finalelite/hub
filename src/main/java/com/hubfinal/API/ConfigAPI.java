package com.hubfinal.API;

import com.hubfinal.MySql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class ConfigAPI {

    private static Map<String, Boolean> flyCache = new HashMap<>();

    public static boolean getFlySmart(String uuid) {
        if (flyCache.containsKey(uuid))
            return flyCache.get(uuid);

        flyCache.put(uuid, getFly(uuid));
        return getFlySmart(uuid);
    }

    public static void removeFromFlyCache(String uuid) {
        flyCache.remove(uuid);
    }

    public static boolean hasFlyCache(String uuid) {
        return flyCache.containsKey(uuid);
    }

    public static void updateVelocidade(String UUID, int Velocidade) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("UPDATE Config_infos SET Velocidade = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setInt(1, Velocidade);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateMsgPrivadas(String UUID, boolean MsgPrivadas) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("UPDATE Config_infos SET MsgPrivadas = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setBoolean(1, MsgPrivadas);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateChat(String UUID, boolean Chat) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("UPDATE Config_infos SET Chat = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setBoolean(1, Chat);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateVanish(String UUID, boolean vanish) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("UPDATE Config_infos SET Vanish = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setBoolean(1, vanish);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateElytra(String UUID, boolean elytra) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("UPDATE Config_infos SET Elytra = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setBoolean(1, elytra);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateFly(String uuid, boolean fly) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("UPDATE Config_infos SET Fly = ? WHERE UUID = ?");
            st.setString(2, uuid);
            st.setBoolean(1, fly);
            st.executeUpdate();

            flyCache.put(uuid, fly);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void setInfos(String UUID, boolean fly, boolean vanish, boolean chat, boolean msgprivadas, int velocidade, boolean elytra) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Config_infos (UUID, Fly, Vanish, Chat, MsgPrivadas, Velocidade, Elytra) VALUES (?, ?, ?, ?, ?, ?, ?)");
            st.setString(1, UUID);
            st.setBoolean(2, fly);
            st.setBoolean(3, vanish);
            st.setBoolean(4, chat);
            st.setBoolean(5, msgprivadas);
            st.setInt(6, velocidade);
            st.setBoolean(7, elytra);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static String getExist(String UUID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Config_infos WHERE UUID = ?");
            st.setString(1, UUID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("UUID");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean getElytra(String UUID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Config_infos WHERE UUID = ?");
            st.setString(1, UUID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getBoolean("Elytra");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean getFly(String UUID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Config_infos WHERE UUID = ?");
            st.setString(1, UUID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getBoolean("Fly");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean getVanish(String UUID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Config_infos WHERE UUID = ?");
            st.setString(1, UUID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getBoolean("Vanish");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean getChat(String UUID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Config_infos WHERE UUID = ?");
            st.setString(1, UUID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getBoolean("Chat");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean getMsgPrivadas(String UUID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Config_infos WHERE UUID = ?");
            st.setString(1, UUID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getBoolean("MsgPrivadas");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static int getVelocidade(String UUID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Config_infos WHERE UUID = ?");
            st.setString(1, UUID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt("Velocidade");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 1;
    }
}
