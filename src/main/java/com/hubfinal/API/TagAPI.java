package com.hubfinal.API;

import com.hubfinal.MySql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class TagAPI {

    private static HashMap<String, String> cache = new HashMap<>();

    public static HashMap<String, String> getCache() {

        return cache;

    }

    public static String getTag(String UUID) {

        if(cache.containsKey(UUID)) return cache.get(UUID);

        try {

            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Tag_data WHERE UUID = ?");
            st.setString(1, UUID);
            ResultSet rs = st.executeQuery();

            if(rs.next()) {

                cache.put(UUID, rs.getString("Tag"));
                return rs.getString("Tag");

            }

        } catch (SQLException e) {

            e.printStackTrace();

        }

        return null;

    }

    public static void setTag(String UUID, String tag) {

        try {

            PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Tag_data (UUID, Tag) VALUES (?, ?)");
            st.setString(1, UUID);
            st.setString(2, tag);
            st.executeUpdate();

            if(cache.containsKey(UUID)) {

                cache.remove(UUID);
                cache.put(UUID, tag);

            }

        } catch (SQLException e) {

            e.printStackTrace();

        }

    }

    public static void updateTag(String UUID, String tag) {

        try {

            PreparedStatement st = MySql.con.prepareStatement("UPDATE Tag_data SET Tag = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setString(1, tag);
            st.executeUpdate();

            if(cache.containsKey(UUID)) {

                cache.remove(UUID);
                cache.put(UUID, tag);

            }

        } catch (SQLException e) {

            e.printStackTrace();

        }

    }

}
