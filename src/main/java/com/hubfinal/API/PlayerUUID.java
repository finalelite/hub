package com.hubfinal.API;

import org.bukkit.Bukkit;

public class PlayerUUID {

    public static String getUUID(String nick) {
        return Bukkit.getPlayerExact(nick).getUniqueId().toString();
    }

    public static String getNick(String UUID) { return Bukkit.getPlayer(java.util.UUID.fromString(UUID)).getName();
    }
}
