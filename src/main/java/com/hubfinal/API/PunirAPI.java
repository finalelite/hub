package com.hubfinal.API;

public class PunirAPI {
    /*
    public static void punir(String UUID, String autor, String motivo, String prova, int tempo, boolean eterno, boolean mutado) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Punicoes_data(UUID, Por, Motivo, Prova, Tempo, Eterno, Mutado) VALUES (?, ?, ?, ?, ?, ?, ?)");
            st.setString(1, UUID);
            st.setString(2, autor);
            st.setString(3, motivo);
            st.setString(4, prova);
            st.setInt(5, tempo);
            st.setBoolean(6, eterno);
            st.setBoolean(7, mutado);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void punicaoInfo(String UUID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Punicoes_infos(UUID, Quantia) VALUES (?, ?)");
            st.setString(1, UUID);
            st.setInt(2, 1);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateVezes(String UUID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("UPDATE Punicoes_infos SET Quantia = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setInt(1, getVezes(UUID) + 1);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static int getVezes(String UUID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Punicoes_infos WHERE UUID = ?");
            st.setString(1, UUID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt("Quantia");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static String getExistInfo(String UUID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Punicoes_infos WHERE UUID = ?");
            st.setString(1, UUID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("UUID");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean getIsMutado(String UUID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Punicoes_data WHERE UUID = ?");
            st.setString(1, UUID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getBoolean("Mutado");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }


    public static boolean getIsEterno(String UUID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Punicoes_data WHERE UUID = ?");
            st.setString(1, UUID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getBoolean("Eterno");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static int getTempo(String UUID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Punicoes_data WHERE UUID = ?");
            st.setString(1, UUID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt("Tempo");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static String getProva(String UUID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Punicoes_data WHERE UUID = ?");
            st.setString(1, UUID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Prova");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getMotivo(String UUID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Punicoes_data WHERE UUID = ?");
            st.setString(1, UUID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Motivo");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getAutor(String UUID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Punicoes_data WHERE UUID = ?");
            st.setString(1, UUID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Por");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getExistPunicao(String UUID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Punicoes_data WHERE UUID = ?");
            st.setString(1, UUID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("UUID");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void removerPunicao(String UUID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("DELETE FROM Punicoes_data WHERE UUID = ?");
            st.setString(1, UUID);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
     */
}
