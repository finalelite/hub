package com.hubfinal.API;

import com.hubfinal.MySql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ServerAPI {

    public static int getOnlineServer(String Server) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Online_server WHERE Server = ?");
            st.setString(1, Server);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt("Online");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static String getServerName(int port) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Server_info WHERE Porta = ?");
            st.setInt(1, port);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("ServerName");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean getOnlineStatus(String Server) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Online_server WHERE Server = ?");
            st.setString(1, Server);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getBoolean("Status");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void setInfo(String Server, int online, boolean status) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Online_server(Server, Online, Status) VALUES (?, ?, ?)");
            st.setString(1, Server);
            st.setInt(2, online);
            st.setBoolean(3, status);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateOnline(String Server, int online) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("UPDATE Online_server SET Online = ? WHERE Server = ?");
            st.setString(2, Server);
            st.setInt(1, online);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateOnlineStatus(String Server, boolean status) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("UPDATE Online_server SET Status = ? WHERE Server = ?");
            st.setString(2, Server);
            st.setBoolean(1, status);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static int getPort(String Server) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Server_info WHERE ServerName = ?");
            st.setString(1, Server);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt("Porta");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static String getIp(String Server) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Server_info WHERE ServerName = ?");
            st.setString(1, Server);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Ip");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void setInfoManutencao(String Server, boolean manutencao) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Lobbys_info(Server, Manutencao) VALUES (?, ?)");
            st.setString(1, Server);
            st.setBoolean(2, manutencao);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateManutencao(String Server, boolean manutencao) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("UPDATE Lobbys_info SET Manutencao = ? WHERE Server = ?");
            st.setString(2, Server);
            st.setBoolean(1, manutencao);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static boolean getManutencao(String Server) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Lobbys_info WHERE Server = ?");
            st.setString(1, Server);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getBoolean("Manutencao");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static String getManutencaoExist(String Server) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Lobbys_info WHERE Server = ?");
            st.setString(1, Server);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Server");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean isRestarting(String Server) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Restart_data WHERE Server = ?");
            st.setString(1, Server);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getBoolean("Restarting");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public static String getStaffOnline() {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Staff_online");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Nick");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getStaffOnlineID(int ID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Staff_online WHERE ID = ?");
            st.setInt(1, ID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Nick");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getStaffOnlineExist(String UUID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Staff_online WHERE UUID = ?");
            st.setString(1, UUID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("UUID");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void setStaffOnline(String UUID, String nick, String tag) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Staff_online(UUID, Nick, Tag) VALUES (?, ?, ?)");
            st.setString(1, UUID);
            st.setString(2, nick);
            st.setString(3, tag);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateStaffOnlineTagID(int ID, String players) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("UPDATE Staff_online SET Players = ? WHERE ID = ?");
            st.setInt(2, ID);
            st.setString(1, players);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateStaffOnlineTag(String UUID, String players) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("UPDATE Staff_online SET Players = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setString(1, players);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void deleteStaffOnline(String UUID) {
        try {
            if (getStaffOnlineExist(UUID) != null) {
                PreparedStatement st = MySql.con.prepareStatement("DELETE FROM Staff_online WHERE = UUID");
                st.setString(1, UUID);
                st.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
