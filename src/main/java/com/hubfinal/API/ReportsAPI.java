package com.hubfinal.API;

import com.hubfinal.MySql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ReportsAPI {

    public static void createReport(String reporte, String text, String reportador, String suspeito) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Reports_data(Reporte, Texto, Reportador, Suspeito) VALUES (?, ?, ?, ?)");
            st.setString(1, reporte);
            st.setString(2, text);
            st.setString(3, reportador);
            st.setString(4, suspeito);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void removeReportHack(String suspeito) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("DELETE FROM Reports_data WHERE Suspeito = ?");
            st.setString(1, suspeito);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void removeReportBug(String reportador) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("DELETE FROM Reports_data WHERE Reportador = ?");
            st.setString(1, reportador);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static String getText(int ID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Reports_data WHERE ID = ?");
            st.setInt(1, ID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Texto");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getReportador(int ID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Reports_data WHERE ID = ?");
            st.setInt(1, ID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Reportador");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getExistReportHack(String reportador) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Reports_data WHERE Reportador = ?");
            st.setString(1, reportador);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Suspeito");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getSuspeito(int ID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Reports_data WHERE ID = ?");
            st.setInt(1, ID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Suspeito");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getReporte(int ID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Reports_data WHERE ID = ?");
            st.setInt(1, ID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Reporte");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
