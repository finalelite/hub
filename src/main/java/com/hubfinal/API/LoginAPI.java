package com.hubfinal.API;

import com.google.common.hash.Hashing;
import com.hubfinal.MySql;

import java.nio.charset.StandardCharsets;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class LoginAPI {

    public static String getPassword(String UUID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Login_data WHERE UUID = ?");
            st.setString(1, UUID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Password");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    //    public static String getNickName(String UUID) {
//        try {
//            System.out.println("UUID " + UUID);
//            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Login_data WHERE UUID = ?");
//            st.setString(1, UUID);
//            ResultSet rs = st.executeQuery();
//            while (rs.next()) {
//                return rs.getString("Nick");
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }
//
    public static String getUUID(String nick) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Login_data WHERE Nick = ?");
            st.setString(1, nick);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("UUID");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getEmail(String UUID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Login_data WHERE UUID = ?");
            st.setString(1, UUID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Email");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void setInfos(String UUID, String nick) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Login_data (UUID, Nick, Password) VALUES (?, ?, ?)");
            st.setString(1, UUID);
            st.setString(2, nick);
            st.setString(3, null);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updatePassword(String UUID, String password) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("UPDATE Login_data SET Password = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setString(1, hashPassword(password));
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static String hashPassword(String password) {
        return Hashing.sha256().hashString(password, StandardCharsets.UTF_8).toString();
    }

    public static void updateEmail(String UUID, String password) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("UPDATE Login_data SET Email = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setString(1, password);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static boolean getLoggedStatus(String UUID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Is_logged WHERE UUID = ?");
            st.setString(1, UUID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getBoolean("Logged");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void setLoggedStatus(String UUID, boolean status) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Is_logged (UUID, Logged) VALUES (?, ?)");
            st.setString(1, UUID);
            st.setBoolean(2, status);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static String getLoggedStatusExist(String UUID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Is_logged WHERE UUID = ?");
            st.setString(1, UUID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("UUID");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void updateLoggedStatus(String UUID, boolean status) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("UPDATE Is_logged SET Logged = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setBoolean(1, status);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static int getTentativa(String UUID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Tentativa_pass WHERE UUID = ?");
            st.setString(1, UUID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt("Tentativa");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 10;
    }

    public static void setTentativa(String UUID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Tentativa_pass (UUID, Tentativa) VALUES (?, ?)");
            st.setString(1, UUID);
            st.setInt(2, 3);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateTentativa(String UUID, int tentativas) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("UPDATE Tentativa_pass SET Tentativa = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setInt(1, tentativas);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void deleteTentativas(String UUID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("DELETE FROM Tentativa_pass WHERE UUID = ?");
            st.setString(1, UUID);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
