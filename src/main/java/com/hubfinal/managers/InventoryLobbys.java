package com.hubfinal.managers;

import org.bukkit.inventory.Inventory;

public class InventoryLobbys {

    public static Inventory inv(String UUID) {
        /*

        Inventory INVENTORY_LOBBYS = Bukkit.createInventory(null, 27, "Lobbys: ");

        if (ServerAPI.getOnlineStatus("Hub1") == true) {
            if (LocationAPI.getServer(UUID).equals("Hub1")) {
                ItemStack green = new ItemStack(Material.LIME_DYE);
                ItemMeta greenMeta = green.getItemMeta();
                greenMeta.setDisplayName(ChatColor.GREEN + "Lobby1: Online");
                ArrayList<String> lore = new ArrayList<String>();
                lore.add(" ");
                lore.add(ChatColor.YELLOW + " * Você está logado neste lobby!");
                lore.add(" ");
                greenMeta.setLore(lore);
                green.setItemMeta(greenMeta);

                INVENTORY_LOBBYS.setItem(11, EnchantGlow.glow(green));
            } else {
                if (ServerAPI.getManutencao("Hub1") == true) {
                    ItemStack red = new ItemStack(Material.ROSE_RED);
                    ItemMeta redMeta = red.getItemMeta();
                    redMeta.setDisplayName(ChatColor.RED + "Lobby1: Em manutenção");
                    red.setItemMeta(redMeta);

                    INVENTORY_LOBBYS.setItem(11, red);
                } else {
                    ItemStack green = new ItemStack(Material.LIME_DYE);
                    ItemMeta greenMeta = green.getItemMeta();
                    greenMeta.setDisplayName(ChatColor.GREEN + "Lobby1: Online");
                    green.setItemMeta(greenMeta);

                    INVENTORY_LOBBYS.setItem(11, green);
                }
            }
        } else {
            ItemStack red = new ItemStack(Material.GRAY_DYE);
            ItemMeta redMeta = red.getItemMeta();
            redMeta.setDisplayName(ChatColor.DARK_GRAY + "Lobby1: Offline");
            red.setItemMeta(redMeta);

            INVENTORY_LOBBYS.setItem(11, red);
        }

        if (ServerAPI.getOnlineStatus("Hub2") == true) {
            if (LocationAPI.getServer(UUID).equals("Hub2")) {
                ItemStack green = new ItemStack(Material.LIME_DYE);
                ItemMeta greenMeta = green.getItemMeta();
                greenMeta.setDisplayName(ChatColor.GREEN + "Lobby2: Online");
                ArrayList<String> lore = new ArrayList<String>();
                lore.add(" ");
                lore.add(ChatColor.YELLOW + " * Você está logado neste lobby!");
                lore.add(" ");
                greenMeta.setLore(lore);
                green.setItemMeta(greenMeta);

                INVENTORY_LOBBYS.setItem(13, EnchantGlow.glow(green));
            } else {
                if (ServerAPI.getManutencao("Hub2") == true) {
                    ItemStack red = new ItemStack(Material.ROSE_RED);
                    ItemMeta redMeta = red.getItemMeta();
                    redMeta.setDisplayName(ChatColor.RED + "Lobby2: Em manutenção");
                    red.setItemMeta(redMeta);

                    INVENTORY_LOBBYS.setItem(13, red);
                } else {

                    ItemStack green = new ItemStack(Material.LIME_DYE);
                    ItemMeta greenMeta = green.getItemMeta();
                    greenMeta.setDisplayName(ChatColor.GREEN + "Lobby2: Online");
                    green.setItemMeta(greenMeta);

                    INVENTORY_LOBBYS.setItem(13, green);
                }
            }
        } else {
            ItemStack red = new ItemStack(Material.GRAY_DYE);
            ItemMeta redMeta = red.getItemMeta();
            redMeta.setDisplayName(ChatColor.DARK_GRAY + "Lobby2: Offline");
            red.setItemMeta(redMeta);

            INVENTORY_LOBBYS.setItem(13, red);
        }

        if (ServerAPI.getOnlineStatus("Hub3") == true) {
            if (LocationAPI.getServer(UUID).equals("Hub3")) {
                ItemStack green = new ItemStack(Material.LIME_DYE);
                ItemMeta greenMeta = green.getItemMeta();
                greenMeta.setDisplayName(ChatColor.GREEN + "Lobby3: Online");
                ArrayList<String> lore = new ArrayList<String>();
                lore.add(" ");
                lore.add(ChatColor.YELLOW + " * Você está logado neste lobby!");
                lore.add(" ");
                greenMeta.setLore(lore);
                green.setItemMeta(greenMeta);

                INVENTORY_LOBBYS.setItem(15, EnchantGlow.glow(green));
            } else {
                if (ServerAPI.getManutencao("Hub3") == true) {
                    ItemStack red = new ItemStack(Material.ROSE_RED);
                    ItemMeta redMeta = red.getItemMeta();
                    redMeta.setDisplayName(ChatColor.RED + "Lobby3: Em manutenção");
                    red.setItemMeta(redMeta);

                    INVENTORY_LOBBYS.setItem(15, red);
                } else {

                    ItemStack green = new ItemStack(Material.LIME_DYE);
                    ItemMeta greenMeta = green.getItemMeta();
                    greenMeta.setDisplayName(ChatColor.GREEN + "Lobby3: Online");
                    green.setItemMeta(greenMeta);

                    INVENTORY_LOBBYS.setItem(15, green);
                }
            }
        } else {
            ItemStack red = new ItemStack(Material.GRAY_DYE);
            ItemMeta redMeta = red.getItemMeta();
            redMeta.setDisplayName(ChatColor.DARK_GRAY + "Lobby3: Offline");
            red.setItemMeta(redMeta);

            INVENTORY_LOBBYS.setItem(15, red);
        }
        return INVENTORY_LOBBYS;
        */

        return null;
    }
}
