package com.hubfinal.managers;

import com.hubfinal.API.ServerAPI;
import com.hubfinal.Main;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class InventoryServers {

    public static Inventory inv(String server) {
        Inventory INVENTORY_SERVERS = Bukkit.createInventory(null, 27, "Servidores: ");

        if (ServerAPI.getOnlineStatus(server) == true) {
            if (ServerAPI.getManutencao(server) == true) {
                ItemStack factions = new ItemStack(Material.DIAMOND_SWORD);
                ItemMeta factionsMeta = factions.getItemMeta();
                factionsMeta.setDisplayName(ChatColor.GREEN + "MixedUP: A nova geração");
                ArrayList<String> lore = new ArrayList<String>();
                lore.add(" ");
                lore.add("§7Embarque em uma jornada cheia de aventuras");
                lore.add("§7e surpresas, onde apenas os mais fortes e sábios");
                lore.add("§7resistem. Crie sua história e deixe o seu legado.");
                lore.add(" ");
                lore.add(" ");
                lore.add(ChatColor.GREEN + "Modos de jogo: ");
                lore.add(ChatColor.RED + "➔ §7Factions");
                lore.add(ChatColor.RED + "➔ §7RankUP");
                lore.add(ChatColor.RED + "➔ §7Survival");
                lore.add(ChatColor.RED + "➔ §7SkyBlock");
                lore.add(ChatColor.RED + "➔ §7RPG");
                lore.add(" ");
                lore.add(ChatColor.RED + "EM MANUTENÇÃO!");
                lore.add(" ");
                factionsMeta.setLore(lore);
                factions.setItemMeta(factionsMeta);

                INVENTORY_SERVERS.setItem(13, factions);
            } else {
                ItemStack factions = new ItemStack(Material.DIAMOND_SWORD);
                ItemMeta factionsMeta = factions.getItemMeta();
                factionsMeta.setDisplayName(ChatColor.GREEN + "MixedUP: A nova geração");
                ArrayList<String> lore = new ArrayList<String>();
                lore.add(" ");
                lore.add("§7Embarque em uma jornada cheia de aventuras");
                lore.add("§7e surpresas, onde apenas os mais fortes e sábios");
                lore.add("§7resistem. Crie sua história e deixe o seu legado.");
                lore.add(" ");
                lore.add(" ");
                lore.add(ChatColor.GREEN + "Modos de jogo: ");
                lore.add(ChatColor.RED + "➔ §7Factions");
                lore.add(ChatColor.RED + "➔ §7RankUP");
                lore.add(ChatColor.RED + "➔ §7Survival");
                lore.add(ChatColor.RED + "➔ §7SkyBlock");
                lore.add(ChatColor.RED + "➔ §7RPG");
                lore.add(" ");
                lore.add(ChatColor.DARK_PURPLE + "" + Main.playerCount + ChatColor.GRAY + " Jogadores online.");
                lore.add(" ");
                factionsMeta.setLore(lore);
                factions.setItemMeta(factionsMeta);

                INVENTORY_SERVERS.setItem(13, factions);
            }
        } else {
            ItemStack factions = new ItemStack(Material.DIAMOND_SWORD);
            ItemMeta factionsMeta = factions.getItemMeta();
            factionsMeta.setDisplayName(ChatColor.GREEN + "MixedUP: A nova geração");
            ArrayList<String> lore = new ArrayList<String>();
            lore.add(" ");
            lore.add("§7Embarque em uma jornada cheia de aventuras");
            lore.add("§7e surpresas, onde apenas os mais fortes e sábios");
            lore.add("§7resistem. Crie sua história e deixe o seu legado.");
            lore.add(" ");
            lore.add(" ");
            lore.add(ChatColor.GREEN + "Modos de jogo: ");
            lore.add(ChatColor.RED + "➔ §7Factions");
            lore.add(ChatColor.RED + "➔ §7RankUP");
            lore.add(ChatColor.RED + "➔ §7Survival");
            lore.add(ChatColor.RED + "➔ §7SkyBlock");
            lore.add(ChatColor.RED + "➔ §7RPG");
            lore.add(" ");
            lore.add(ChatColor.RED + "OFFLINE");
            lore.add(" ");
            factionsMeta.setLore(lore);
            factions.setItemMeta(factionsMeta);

            INVENTORY_SERVERS.setItem(13, factions);
        }

        return INVENTORY_SERVERS;
    }
}
