package com.hubfinal.managers;

import com.hubfinal.API.ConfigAPI;
import com.hubfinal.utils.EnchantGlow;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class InventoryConfigs {

    public static Inventory inv(String UUID) {
        Inventory INVENTORY_CONFIGS = Bukkit.createInventory(null, 4 * 9, "Configurações: ");

        if (ConfigAPI.getFly(UUID) == false) {
            ItemStack paper = new ItemStack(Material.FEATHER);
            ItemMeta paperMeta = paper.getItemMeta();
            paperMeta.setDisplayName(ChatColor.GREEN + "Voar: " + ChatColor.GOLD + "[VIP]");
            ArrayList<String> lore1 = new ArrayList<String>();
            lore1.add(" ");
            lore1.add("§c * Desativado");
            lore1.add(" ");
            paperMeta.setLore(lore1);
            paper.setItemMeta(paperMeta);

            INVENTORY_CONFIGS.setItem(10, EnchantGlow.glow(paper));
        } else {
            ItemStack map = new ItemStack(Material.LEATHER_BOOTS);
            ItemMeta mapMeta = map.getItemMeta();
            mapMeta.setDisplayName(ChatColor.GREEN + "Voar: " + ChatColor.GOLD + "[VIP]");
            ArrayList<String> lore1 = new ArrayList<String>();
            lore1.add(" ");
            lore1.add("§e * Ativado");
            lore1.add(" ");
            mapMeta.setLore(lore1);
            map.setItemMeta(mapMeta);

            INVENTORY_CONFIGS.setItem(10, EnchantGlow.glow(map));
        }

        if (ConfigAPI.getVanish(UUID) == false) {
            ItemStack green = new ItemStack(Material.INK_SAC, 1, (short) 10);
            ItemMeta greenMeta = green.getItemMeta();
            greenMeta.setDisplayName(ChatColor.GREEN + "Players: ");
            ArrayList<String> lore1 = new ArrayList<String>();
            lore1.add(" ");
            lore1.add("§e * Visíveis");
            lore1.add(" ");
            greenMeta.setLore(lore1);
            green.setItemMeta(greenMeta);

            INVENTORY_CONFIGS.setItem(12, green);
        } else {
            ItemStack red = new ItemStack(Material.INK_SAC, 1, (short) 1);
            ItemMeta redMeta = red.getItemMeta();
            redMeta.setDisplayName(ChatColor.GREEN + "Players: ");
            ArrayList<String> lore1 = new ArrayList<String>();
            lore1.add(" ");
            lore1.add("§c * Invisíveis");
            lore1.add(" ");
            redMeta.setLore(lore1);
            red.setItemMeta(redMeta);

            INVENTORY_CONFIGS.setItem(12, red);
        }

        if (ConfigAPI.getChat(UUID) == false) {
            ItemStack paper = new ItemStack(Material.PAPER);
            ItemMeta paperMeta = paper.getItemMeta();
            paperMeta.setDisplayName(ChatColor.GREEN + "Chat: ");
            ArrayList<String> lore1 = new ArrayList<String>();
            lore1.add(" ");
            lore1.add("§e * Ativado");
            lore1.add(" ");
            paperMeta.setLore(lore1);
            paper.setItemMeta(paperMeta);

            INVENTORY_CONFIGS.setItem(13, paper);
        } else {
            ItemStack map = new ItemStack(Material.MAP);
            ItemMeta mapMeta = map.getItemMeta();
            mapMeta.setDisplayName(ChatColor.GREEN + "Chat: ");
            ArrayList<String> lore1 = new ArrayList<String>();
            lore1.add(" ");
            lore1.add("§c * Desativado");
            lore1.add(" ");
            mapMeta.setLore(lore1);
            map.setItemMeta(mapMeta);

            INVENTORY_CONFIGS.setItem(13, map);
        }

        if (ConfigAPI.getMsgPrivadas(UUID) == false) {
            ItemStack book = new ItemStack(Material.BOOK);
            ItemMeta bookMeta = book.getItemMeta();
            bookMeta.setDisplayName(ChatColor.GREEN + "Mensagens privadas: ");
            ArrayList<String> lore1 = new ArrayList<String>();
            lore1.add(" ");
            lore1.add("§e * Ativado");
            lore1.add(" ");
            bookMeta.setLore(lore1);
            book.setItemMeta(bookMeta);

            INVENTORY_CONFIGS.setItem(14, book);
        } else {
            ItemStack bookp = new ItemStack(Material.WRITABLE_BOOK);
            ItemMeta bookpMeta = bookp.getItemMeta();
            bookpMeta.setDisplayName(ChatColor.GREEN + "Mensagens privadas: ");
            ArrayList<String> lore1 = new ArrayList<String>();
            lore1.add(" ");
            lore1.add("§c * Desativado");
            lore1.add(" ");
            bookpMeta.setLore(lore1);
            bookp.setItemMeta(bookpMeta);

            INVENTORY_CONFIGS.setItem(14, bookp);
        }

        if (ConfigAPI.getVelocidade(UUID) == 1) {
            ItemStack boots = new ItemStack(Material.LEATHER_BOOTS);
            ItemMeta bootsMeta = boots.getItemMeta();
            bootsMeta.setDisplayName(ChatColor.GREEN + "Velocidade: " + ChatColor.GOLD + "[VIP]");
            ArrayList<String> lore1 = new ArrayList<String>();
            lore1.add(" ");
            lore1.add("§e * Nível - 1");
            lore1.add(" ");
            bootsMeta.setLore(lore1);
            boots.setItemMeta(bootsMeta);

            INVENTORY_CONFIGS.setItem(16, EnchantGlow.glow(boots));
        } else if (ConfigAPI.getVelocidade(UUID) == 2) {
            ItemStack boots = new ItemStack(Material.IRON_BOOTS);
            ItemMeta bootsMeta = boots.getItemMeta();
            bootsMeta.setDisplayName(ChatColor.GREEN + "Velocidade: " + ChatColor.GOLD + "[VIP]");
            ArrayList<String> lore1 = new ArrayList<String>();
            lore1.add(" ");
            lore1.add("§e * Nível - 2");
            lore1.add(" ");
            bootsMeta.setLore(lore1);
            boots.setItemMeta(bootsMeta);

            INVENTORY_CONFIGS.setItem(16, EnchantGlow.glow(boots));
        } else if (ConfigAPI.getVelocidade(UUID) == 3) {
            ItemStack boots = new ItemStack(Material.DIAMOND_BOOTS);
            ItemMeta bootsMeta = boots.getItemMeta();
            bootsMeta.setDisplayName(ChatColor.GREEN + "Velocidade: " + ChatColor.GOLD + "[VIP]");
            ArrayList<String> lore1 = new ArrayList<String>();
            lore1.add(" ");
            lore1.add("§e * Nível - 3");
            lore1.add(" ");
            bootsMeta.setLore(lore1);
            boots.setItemMeta(bootsMeta);

            INVENTORY_CONFIGS.setItem(16, EnchantGlow.glow(boots));
        }

        if (ConfigAPI.getElytra(UUID) == false) {
            ItemStack elytra = new ItemStack(Material.ELYTRA);
            ItemMeta elytraMeta = elytra.getItemMeta();
            elytraMeta.setDisplayName(ChatColor.GREEN + "Elytra: ");
            ArrayList<String> lore1 = new ArrayList<String>();
            lore1.add(" ");
            lore1.add(ChatColor.RED + " * Desativada");
            lore1.add(" ");
            elytraMeta.setLore(lore1);
            elytra.setItemMeta(elytraMeta);

            INVENTORY_CONFIGS.setItem(22, elytra);
        } else {
            ItemStack elytra = new ItemStack(Material.ELYTRA);
            ItemMeta elytraMeta = elytra.getItemMeta();
            elytraMeta.setDisplayName(ChatColor.GREEN + "Elytra: ");
            ArrayList<String> lore1 = new ArrayList<String>();
            lore1.add(" ");
            lore1.add(ChatColor.GREEN + " * Ativada");
            lore1.add(" ");
            elytraMeta.setLore(lore1);
            elytra.setItemMeta(elytraMeta);

            INVENTORY_CONFIGS.setItem(22, EnchantGlow.glow(elytra));
        }
        return INVENTORY_CONFIGS;
    }
}
