package com.hubfinal.commands;

import com.hubfinal.API.LoginAPI;
import com.hubfinal.API.PlayerUUID;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandTrocarSenha implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String lb, String[] args) {

        Player p = (Player) sender;

        String uuid = PlayerUUID.getUUID(p.getName());

        if (args.length == 2) {
            if (LoginAPI.getPassword(uuid).equals(LoginAPI.hashPassword(args[0]))) {
                if (args[1].length() >= 4) {
                    if (args[1].equals("abcd") || args[1].equals("abcde") || args[1].equals("abcdef") || args[1].equals("abcdefg") || args[1].equals("abcdefgh") || args[1].equals("abcdefghi") || args[1].equals("abcdefghio") || args[1].equals("1234") || args[1].equals("12345") || args[1].equals("123456") || args[1].equals("1234567") || args[1].equals("12345678") || args[1].equals("123456789")) {
                        p.sendMessage(ChatColor.RED + "Ops, A sua senha parece muito fácil, tente outra novamente.");
                    } else {
                        LoginAPI.updatePassword(uuid, args[1]);
                        p.sendMessage(ChatColor.GREEN + "A sua senha foi trocada com sucesso.");
                    }
                } else {
                    p.sendMessage(ChatColor.RED + "Ops, A sua nova senha precisa ter no mínimo 4 caractéres.");
                }

            } else {
                p.sendMessage(ChatColor.RED + "A sua senha antiga está incorreta.");
            }
        } else {
            p.sendMessage(ChatColor.RED + "Use /trocarsenha <senha antiga> <nova senha>.");
        }

        return false;

    }
}
