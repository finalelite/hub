package com.hubfinal.commands;

import br.com.finalelite.pauloo27.api.PauloAPI;
import com.hubfinal.Main;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandMaxPlayers implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String lb, String[] args) {

        if(sender instanceof Player) {

            Player p = (Player) sender;

            if(PauloAPI.getInstance().getAccountInfo(p).getRole().isMajorStaff()) {

                int number;

                try {

                    number = Integer.parseInt(args[0]);

                } catch (Exception e) {

                    p.sendMessage(ChatColor.RED + " * Whoops, Número inválido.");
                    return true;

                }

                Main.getInstance().getConfig().set("servers.mixedup.players", number);
                Main.setMaxPlayer(number);

                p.sendMessage(ChatColor.GREEN + " * Quantidade de jogadores setada para " + ChatColor.WHITE + number + ChatColor.GREEN + ".");
                return true;

            } else {

                p.sendMessage(ChatColor.RED + " * Você não tem permissão para isso.");
                return true;

            }

        } else {

            sender.sendMessage(ChatColor.RED + " * Whoops, apenas jogadores");
            return true;

        }

    }
}
