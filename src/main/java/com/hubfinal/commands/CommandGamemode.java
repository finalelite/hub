package com.hubfinal.commands;

import com.hubfinal.API.PlayerUUID;
import com.hubfinal.API.TagAPI;
import com.hubfinal.utils.AlternateColor;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandGamemode implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        Player player = (Player) sender;
        String UUID = PlayerUUID.getUUID(player.getName());

        if (cmd.getName().equalsIgnoreCase("gamemode") || cmd.getName().equalsIgnoreCase("gm")) {
            if (TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor")) {
                if (args.length > 1 || args.length < 1) {
                    player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/gamemode (0, 1, 2, 3) ou /gm (0, 1, 2 ,3)");
                    return false;
                }
                if (args.length == 1) {
                    if (args[0].equals("0") || args[0].equals("1") || args[0].equals("2") || args[0].equals("3") || args[0].equals("survival") || args[0].equals("criativo")  || args[0].equals("adventure") || args[0].equals("espectador")) {
                        if (args[0].equals("0")) {
                            player.setGameMode(GameMode.SURVIVAL);
                            player.sendMessage(ChatColor.GREEN + " * Gamemode setado para " + AlternateColor.alternate("&a&nSURVIVAL") + ChatColor.GREEN + ".");
                        }
                        if (args[0].equals("1")) {
                            player.setGameMode(GameMode.CREATIVE);
                            player.sendMessage(ChatColor.GREEN + " * Gamemode setado para " + AlternateColor.alternate("&a&nCRIATIVO") + ChatColor.GREEN + ".");
                        }
                        if (args[0].equals("2")) {
                            player.setGameMode(GameMode.ADVENTURE);
                            player.sendMessage(ChatColor.GREEN + " * Gamemode setado para " + AlternateColor.alternate("&a&nADVENTURE") + ChatColor.GREEN + ".");
                        }
                        if (args[0].equals("3")) {
                            player.setGameMode(GameMode.SPECTATOR);
                            player.sendMessage(ChatColor.GREEN + " * Gamemode setado para " + AlternateColor.alternate("&a&nESPECTADOR") + ChatColor.GREEN + ".");
                        }
                        if (args[0].equals("survival")) {
                            player.setGameMode(GameMode.SURVIVAL);
                            player.sendMessage(ChatColor.GREEN + " * Gamemode setado para " + AlternateColor.alternate("&a&nSURVIVAL") + ChatColor.GREEN + ".");
                        }
                        if (args[0].equals("criativo")) {
                            player.setGameMode(GameMode.CREATIVE);
                            player.sendMessage(ChatColor.GREEN + " * Gamemode setado para " + AlternateColor.alternate("&a&nCRIATIVO") + ChatColor.GREEN + ".");
                        }
                        if (args[0].equals("adventure")) {
                            player.setGameMode(GameMode.ADVENTURE);
                            player.sendMessage(ChatColor.GREEN + " * Gamemode setado para " + AlternateColor.alternate("&a&nADVENTURE") + ChatColor.GREEN + ".");
                        }
                        if (args[0].equals("espectador")) {
                            player.setGameMode(GameMode.SPECTATOR);
                            player.sendMessage(ChatColor.GREEN + " * Gamemode setado para " + AlternateColor.alternate("&a&nESPECTADOR") + ChatColor.GREEN + ".");
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + "Ops, veja qual argumento foi utilizado para definir o gamemode e tente novamente.");
                    }
                }
            }
        }
        return false;
    }
}
