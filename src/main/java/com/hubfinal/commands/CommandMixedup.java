package com.hubfinal.commands;

import com.hubfinal.API.ServerAPI;
import com.hubfinal.API.TagAPI;
import com.hubfinal.utils.JoinQueue;
import lombok.val;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandMixedup implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        val player = (Player) sender;
        
        val tag = TagAPI.getTag(player.getUniqueId().toString());

        if(ServerAPI.getManutencao("Factions") && (tag.equalsIgnoreCase("Membro") || tag.toLowerCase().contains("vip"))) {
            player.sendMessage(ChatColor.RED + "Servidor em manutenção.");
            return true;
        }
            
        if (ServerAPI.getOnlineStatus("Factions")) {
            if (JoinQueue.addPlayerToQueue(player))
                JoinQueue.sendCurrentPosition();
        } else {
            player.sendMessage(ChatColor.RED + "Ops, este servidor aparentemente está offline no momento!");
            return true;
        }
        return true;
    }
}
