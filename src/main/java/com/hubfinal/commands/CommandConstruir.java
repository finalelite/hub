package com.hubfinal.commands;

import com.hubfinal.API.PlayerUUID;
import com.hubfinal.API.ServerAPI;
import com.hubfinal.API.TagAPI;
import com.hubfinal.Main;
import com.hubfinal.utils.AlternateColor;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;

public class CommandConstruir implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        Player player = (Player) sender;
        String UUID = PlayerUUID.getUUID(player.getName());

        if (cmd.getName().equalsIgnoreCase("construir")) {
            if (TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor") || TagAPI.getTag(UUID).equals("Admin")) {
                if (ServerAPI.getOnlineStatus("Construir") == false) {
                    player.sendMessage(ChatColor.RED + "Ops, este servidor está offline no momento.");
                } else {
                    try {
                        ByteArrayOutputStream b = new ByteArrayOutputStream();
                        DataOutputStream out = new DataOutputStream(b);

                        out.writeUTF("Connect");
                        out.writeUTF("Construir");

                        player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                        player.sendMessage(ChatColor.GREEN + " * Conectando se a " + AlternateColor.alternate("&a&nConstruir") + ChatColor.GREEN + ".");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return false;
    }
}
