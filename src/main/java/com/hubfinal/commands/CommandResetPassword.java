package com.hubfinal.commands;

import br.com.finalelite.pauloo27.api.PauloAPI;
import br.com.finalelite.pauloo27.api.message.MessageUtils;
import com.hubfinal.API.LoginAPI;
import lombok.val;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandResetPassword implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String lb, String[] args) {

        if (sender instanceof Player) {

            Player p = (Player) sender;

            if (PauloAPI.getInstance().getAccountInfo(p).getRole().isMajorStaff()) {

                if (args.length == 1) {

                    String uuid = LoginAPI.getUUID(args[0]);

                    if (uuid != null) {

                        val targetUser = PauloAPI.getInstance().getAccountsCache().getAccountInfo(uuid);

                        if (targetUser.getRole().isStaff()) {
                            p.sendMessage(ChatColor.RED + " * Você não pode alterar a senha de um membro da equipe.");
                            return true;
                        }

                        String password = MessageUtils.getRandomAlphanumericString(6);

                        LoginAPI.updatePassword(uuid, password);
                        p.sendMessage(ChatColor.GREEN + " * A senha do jogador " + ChatColor.WHITE + args[0] + ChatColor.GREEN + " foi alterada para " + ChatColor.WHITE + password + ChatColor.GREEN + ".");
                        return true;

                    } else {

                        p.sendMessage(ChatColor.RED + " * Não há ninguém no banco de dados registrado com este nick.");
                        return true;

                    }

                } else {

                    p.sendMessage(ChatColor.RED + " Use /" + lb + " <jogador>.");
                    return true;

                }

            } else {

                p.sendMessage(ChatColor.RED + " * Você não tem permissão para isso.");
                return true;

            }

        } else {

            sender.sendMessage(ChatColor.RED + " * Apenas jogadores podem executar este comando.");
            return true;

        }

    }
}
