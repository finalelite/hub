package com.hubfinal.commands;

import com.hubfinal.utils.AlternateColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandAjuda implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        Player player = (Player) sender;

        if (cmd.getName().equalsIgnoreCase("ajuda") || cmd.getName().equalsIgnoreCase("help")) {
            player.sendMessage(AlternateColor.alternate("\n \n&e&l * AJUDA: \n \n&3 * Utilize: &7/fly ou /voar para habilitar modo voar. &6[VIP]\n&3 * Utilize: &7/tell ou /msg para conversar privado.\n&3 * Utilize: &7/reportar para reportar &aBugs &7ou &cHacks&7.\n&3 * Utilize: &7/cadastrar email para cadastrar um email a vossa conta.\n \n&e * Site: &awww.finalelite.com.br\n&e * Twitter: &a@FinalElite\n&e * Discord: &adiscord.gg/vmBJm2\n&e * Dúvidas frequentes: &awww.finalelite.com.br/duvidas/\n \n "));
        }
        return false;
    }
}
