package com.hubfinal.commands;

import com.hubfinal.API.ConfigAPI;
import com.hubfinal.API.PlayerUUID;
import com.hubfinal.API.TagAPI;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandFly implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        Player player = (Player) sender;
        String UUID = PlayerUUID.getUUID(player.getName());

        if (cmd.getName().equalsIgnoreCase("fly") || cmd.getName().equalsIgnoreCase("voar")) {
            if (args.length > 0) {
                player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/fly ou /voar");
                return false;
            }
            if (args.length == 0) {
                if (!TagAPI.getTag(UUID).equals("Membro")) {
                    if (ConfigAPI.getFly(UUID) == true) {
                        player.setAllowFlight(false);
                        player.sendMessage(ChatColor.GREEN + " * Fly desabilitado com sucesso.");
                        ConfigAPI.updateFly(UUID, false);
                    } else {
                        player.setAllowFlight(true);
                        player.sendMessage(ChatColor.GREEN + " * Fly habilitado com sucesso.");
                        ConfigAPI.updateFly(UUID, true);
                    }
                } else {
                    player.sendMessage(ChatColor.GRAY + " * Adquira vantagens como estas adquirindo " + ChatColor.GREEN + "VIP" + ChatColor.GRAY + " em nosso site." + "\n" + ChatColor.YELLOW + " * www.finalelite.com.br");
                }
            }
        }
        return false;
    }
}
