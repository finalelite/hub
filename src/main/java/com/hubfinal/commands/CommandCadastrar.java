package com.hubfinal.commands;

import com.hubfinal.API.LoginAPI;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandCadastrar implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        Player player = (Player) sender;

        if (cmd.getName().equalsIgnoreCase("cadastrar")) {
            if (LoginAPI.getEmail(player.getUniqueId().toString()) == null) {
                if (args.length < 2 || args.length > 2) {
                    player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/cadastrar email (email)");
                    return false;
                }
                if (args.length == 2) {
                    if (args[0].equals("email")) {
                        if (args[1].contains(".") && args[1].contains("@")) {
                            if (args[1].contains("gmail") || args[1].contains("hotmail") || args[1].contains("outlook") || args[1].contains("yahoo") || args[1].contains("bol") || args[1].contains("ig") || args[1].contains("globomail") || args[1].contains("oi") || args[1].contains("pop") || args[1].contains("r7") || args[1].contains("folha") || args[1].contains("zipmail") || args[1].contains("inteligweb")) {
                                if (args[1].contains("com") || args[1].contains("br") || args[1].contains("net")) {
                                    LoginAPI.updateEmail(player.getUniqueId().toString(), args[1]);
                                    player.sendMessage("\n" + " " + ChatColor.YELLOW + " * Email atualizado com sucesso, para recuperar sua senha ou alterar o seu email acesse o nosso site: " + "\n" + " " + ChatColor.AQUA + "www.finalelite.com.br/logar/" + "\n" + " ");
                                } else {
                                    player.sendMessage(ChatColor.RED + "Ops, este email parece estar incorreto.");
                                    return false;
                                }
                            } else {
                                player.sendMessage(ChatColor.RED + "Ops, este email parece estar incorreto.");
                                return false;
                            }
                        } else {
                            player.sendMessage(ChatColor.RED + "Ops, este email parece estar incorreto.");
                            return false;
                        }
                    }
                }
            }
            return false;
        }
        return false;
    }
}
