package com.hubfinal.commands;

import com.hubfinal.API.PlayerUUID;
import com.hubfinal.API.TagAPI;
import com.hubfinal.MySql;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CommandSetspawn implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        Player player = (Player) sender;
        String UUID = PlayerUUID.getUUID(player.getName());

        // /setspawn (nome do spawn)
        if (cmd.getName().equalsIgnoreCase("setspawn")) {
            if (TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor")) {
                if (args.length < 1 || args.length > 1) {
                    player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/setspawn (nome)");
                    return false;
                }
                if (args.length == 1) {
                    if (getLoc(args[0]) != null) {
                        removeInfo(args[0]);
                        setInfo(args[0], player.getWorld().getName(), player.getLocation().getX(), player.getLocation().getY(), player.getLocation().getZ(), player.getLocation().getYaw(), player.getLocation().getPitch());
                        player.sendMessage(ChatColor.GREEN + "Spawn de " + args[0] + " definido para: " + ChatColor.GRAY + player.getWorld().getName() + "/" + player.getLocation().getX() + "/" + player.getLocation().getY() + "/" + player.getLocation().getZ() + "/" + player.getLocation().getYaw() + "/" + player.getLocation().getPitch());
                    } else {
                        setInfo(args[0], player.getWorld().getName(), player.getLocation().getX(), player.getLocation().getY(), player.getLocation().getZ(), player.getLocation().getYaw(), player.getLocation().getPitch());
                        player.sendMessage(ChatColor.GREEN + "Spawn de " + args[0] + " definido para: " + ChatColor.GRAY + player.getWorld().getName() + "/" + player.getLocation().getX() + "/" + player.getLocation().getY() + "/" + player.getLocation().getZ() + "/" + player.getLocation().getYaw() + "/" + player.getLocation().getPitch());
                    }
                }
            }
            return false;
        }
        return false;
    }

    //World,X,Y,Z,YAW,PITCH
    public static String getLoc(String server) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Spawn_location WHERE Server = ?");
            st.setString(1, server);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Location");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static void setInfo(String server, String world, double x, double y, double z, Float yaw, Float pitch) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Spawn_location(Server, Location) VALUES (?, ?)");
            st.setString(1, server);
            st.setString(2, world + "/" + x + "/" + y + "/" + z + "/" + yaw + "/" + pitch);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void removeInfo(String server) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("DELETE FROM Spawn_location WHERE Server = ?");
            st.setString(1, server);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
