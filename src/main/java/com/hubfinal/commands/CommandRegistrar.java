package com.hubfinal.commands;

import com.hubfinal.API.LoginAPI;
import com.hubfinal.API.PlayerUUID;
import com.hubfinal.Main;
import com.hubfinal.listeners.PlayerJoinEvents;
import com.hubfinal.utils.AlternateColor;
import com.hubfinal.utils.CentralizerText;
import com.hubfinal.utils.EnchantGlow;
import org.bukkit.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class CommandRegistrar implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        Player player = (Player) sender;
        String UUID = PlayerUUID.getUUID(player.getName());

        if (cmd.getName().equalsIgnoreCase("registrar") || cmd.getName().equalsIgnoreCase("register")) {
            if (LoginAPI.getLoggedStatus(UUID) == false) {
                if (LoginAPI.getPassword(UUID) == null) {
                    if (args.length < 1 || args.length > 1) {
                        player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/registrar (senha)");
                        return false;
                    }
                    if (args.length == 1) {
                        if (args[0].length() >= 4) {
                            if (args[0].equals("abcd") || args[0].equals("abcde") || args[0].equals("abcdef") || args[0].equals("abcdefg") || args[0].equals("abcdefgh") || args[0].equals("abcdefghi") || args[0].equals("abcdefghio") || args[0].equals("1234") || args[0].equals("12345") || args[0].equals("123456") || args[0].equals("1234567") || args[0].equals("12345678") || args[0].equals("123456789")) {
                                player.sendMessage(ChatColor.RED + "Ops, está senha aparenta ser facíl demais para se descobrir tente outra senha.");
                                return false;
                            }
                            LoginAPI.updatePassword(UUID, args[0]);
                            LoginAPI.setLoggedStatus(UUID, true);
                            String str1 = ChatColor.YELLOW + "Bem-vindo " + ChatColor.GRAY + "ao universo" + ChatColor.DARK_PURPLE + " FinalElite";
                            String str2 = ChatColor.GRAY + "Utilize a bússola ou os NPC para acessar nossos servidores!";
                            String str3 = ChatColor.GRAY + "Caso necessite de mais informações ou ajuda utilize" + ChatColor.YELLOW + " /ajuda";
                            String str4 = ChatColor.GRAY + "Para adquirir " + ChatColor.GREEN + "VIP" + ChatColor.GRAY + " acesse: ";
                            String str5 = ChatColor.YELLOW + "finalelite.com.br";
                            String line1 = CentralizerText.centralize(str1, 72, " ");
                            String line2 = CentralizerText.centralize(str2, 63, " ");
                            String line3 = CentralizerText.centralize(str3, 66, " ");
                            String line4 = CentralizerText.centralize(str4, 72, " ");
                            String line5 = CentralizerText.centralize(str5, 72, " ");

                            player.resetTitle();
                            player.playEffect(player.getLocation(), Effect.DRAGON_BREATH, 0);
                            //MODIFICADO AQUI 1.12
                            player.playSound(player.getLocation(), Sound.UI_TOAST_CHALLENGE_COMPLETE, 1.0F, 1.0F);

                            PlayerJoinEvents.LoginTasks.tasks.get(player).cancel();
                            PlayerJoinEvents.LoginTasks.tasks.remove(player);

                            for (Player target : Bukkit.getOnlinePlayers()) {
                                target.showPlayer(player);
                                player.showPlayer(target);
                            }

                            Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                @Override
                                public void run() {
                                    if (LoginAPI.getEmail(UUID) == null) {
                                        player.sendMessage(AlternateColor.alternate("&e * Foi verificado que você ainda não possui um cadastro de email em sua atual conta. Para evitarmos problemas futuros utilize o comando &e&n/cadastrar email (email)." + "\n" + "&e "));
                                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                    }
                                }
                            }, 60L);

                            player.sendMessage(" " + "\n" + " " + "\n" + line1 + "\n" + line2 + "\n" + line3 + "\n" + line4 + "\n" + line5 + "\n" + " ");

                            ItemStack compass = new ItemStack(Material.COMPASS);
                            ItemMeta compassMeta = compass.getItemMeta();
                            compassMeta.setDisplayName(ChatColor.YELLOW + "Servidores");
                            compass.setItemMeta(compassMeta);

                            ItemStack slime = new ItemStack(Material.SLIME_BALL);
                            ItemMeta slimeMeta = slime.getItemMeta();
                            slimeMeta.setDisplayName(ChatColor.YELLOW + "Lobbys");
                            slime.setItemMeta(slimeMeta);

                            ItemStack anvil = new ItemStack(Material.ANVIL);
                            ItemMeta anvilMeta = anvil.getItemMeta();
                            anvilMeta.setDisplayName(ChatColor.RED + "Configurações");
                            anvil.setItemMeta(anvilMeta);

                            player.getInventory().clear();
                            player.getInventory().setItem(1, anvil);
                            player.getInventory().setItem(4, compass);
                            player.getInventory().setItem(7, EnchantGlow.glow(slime));
                        } else {
                            player.sendMessage(ChatColor.RED + "Ops, está senha é muito curta tente utilizar mais de 4 argumentos na sua password!");
                            return false;
                        }
                    }
                } else {
                    if (LoginAPI.getPassword(UUID) == null) {
                        if (args.length < 1 || args.length > 1) {
                            player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/registrar (senha)");
                            return false;
                        }
                        if (args.length == 1) {
                            if (args[0].length() >= 4) {
                                if (args[0].equals("abcd") || args[0].equals("abcde") || args[0].equals("abcdef") || args[0].equals("abcdefg") || args[0].equals("abcdefgh") || args[0].equals("abcdefghi") || args[0].equals("abcdefghio") || args[0].equals("1234") || args[0].equals("12345") || args[0].equals("123456") || args[0].equals("1234567") || args[0].equals("12345678") || args[0].equals("123456789")) {
                                    player.sendMessage(ChatColor.RED + "Ops, está senha aparenta ser facíl demais para se descobrir tente outra senha.");
                                    return false;
                                }
                                LoginAPI.updatePassword(UUID, args[0]);
                                LoginAPI.setLoggedStatus(UUID, true);
                                String str1 = ChatColor.YELLOW + "Bem-vindo " + ChatColor.GRAY + "ao universo" + ChatColor.DARK_PURPLE + " FinalElite";
                                String str2 = ChatColor.GRAY + "Utilize a bússola ou os NPC para acessar nossos servidores!";
                                String str3 = ChatColor.GRAY + "Caso necessite de mais informações ou ajuda utilize" + ChatColor.YELLOW + " /ajuda";
                                String str4 = ChatColor.GRAY + "Para adquirir " + ChatColor.GREEN + "VIP" + ChatColor.GRAY + " acesse: ";
                                String str5 = ChatColor.YELLOW + "finalelite.com.br";
                                String line1 = CentralizerText.centralize(str1, 72, " ");
                                String line2 = CentralizerText.centralize(str2, 63, " ");
                                String line3 = CentralizerText.centralize(str3, 66, " ");
                                String line4 = CentralizerText.centralize(str4, 72, " ");
                                String line5 = CentralizerText.centralize(str5, 72, " ");

                                player.resetTitle();
                                player.playEffect(player.getLocation(), Effect.DRAGON_BREATH, 0);
                                //MODIFICADO AQUI 1.12
                                player.playSound(player.getLocation(), Sound.UI_TOAST_CHALLENGE_COMPLETE, 1.0F, 1.0F);

                                PlayerJoinEvents.LoginTasks.tasks.get(player).cancel();
                                PlayerJoinEvents.LoginTasks.tasks.remove(player);

                                for (Player target : Bukkit.getOnlinePlayers()) {
                                    target.showPlayer(player);
                                    player.showPlayer(target);
                                }

                                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                    @Override
                                    public void run() {
                                        if (LoginAPI.getEmail(UUID) == null) {
                                            player.sendMessage(AlternateColor.alternate("&e * Foi verificado que você ainda não possui um cadastro de email em sua atual conta. Para evitarmos problemas futuros utilize o comando &e&n/cadastrar email (email)." + "\n" + "&e "));
                                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                        }
                                    }
                                }, 60L);

                                player.sendMessage(" " + "\n" + " " + "\n" + line1 + "\n" + line2 + "\n" + line3 + "\n" + line4 + "\n" + line5 + "\n" + " ");

                                PlayerJoinEvents.setItems(player);
                            } else {
                                player.sendMessage(ChatColor.RED + "Ops, está senha é muito curta tente utilizar mais de 4 argumentos na sua password!");
                                return false;
                            }
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + "Ops, você ja é cadastrado. Utilize comando " + AlternateColor.alternate("&e&n/logar (senha)") + ChatColor.RED + " para se logar ao servidor.");
                        return false;
                    }
                }
            }
            return false;
        }
        return false;
    }
}
