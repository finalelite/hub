package com.hubfinal.commands;

import com.hubfinal.API.PlayerUUID;
import com.hubfinal.API.ServerAPI;
import com.hubfinal.API.TagAPI;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandManutencao implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        Player player = (Player) sender;
        String UUID = PlayerUUID.getUUID(player.getName());

        if (cmd.getName().equalsIgnoreCase("manutencao")) {
            if (TagAPI.getTag(UUID).equals("Master") || TagAPI.getTag(UUID).equals("Supervisor")) {
                if (args.length < 1 || args.length > 1) {
                    player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/manutencao (lobby)");
                    return false;
                }
                if (args.length == 1) {
                    if (args[0].equals("Hub1") || args[0].equals("Hub2") || args[0].equals("Hub3") || args[0].equals("Factions")) {
                        if (ServerAPI.getManutencaoExist(args[0]) == null) {
                            ServerAPI.setInfoManutencao(args[0], true);
                            player.sendMessage(ChatColor.RED + "[" + args[0] + "]" + " manutenção ativada.");
                        } else {
                            if (ServerAPI.getManutencao(args[0]) == false) {
                                ServerAPI.updateManutencao(args[0], true);
                                player.sendMessage(ChatColor.RED + "[" + args[0] + "]" + " manutenção ativada.");
                            } else {
                                ServerAPI.updateManutencao(args[0], false);
                                player.sendMessage(ChatColor.GREEN + "[" + args[0] + "]" + " manutenção desativada.");
                            }
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + "Ops, este lobby aparentemente não existe.");
                    }
                }
            }
            return false;
        }
        return false;
    }
}
