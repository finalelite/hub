package com.hubfinal.commands;

import com.hubfinal.API.ServerAPI;
import com.hubfinal.Main;
import com.hubfinal.MySql;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CommandStop implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (cmd.getName().equalsIgnoreCase("desligar")) {
            for (Player target : Bukkit.getOnlinePlayers()) {
                try {
                    ByteArrayOutputStream b = new ByteArrayOutputStream();
                    DataOutputStream out = new DataOutputStream(b);

                    out.writeUTF("Connect");
                    out.writeUTF(getNumberInOrder().get(0));
                    ServerAPI.updateOnline(ServerAPI.getServerName(Bukkit.getPort()), ServerAPI.getOnlineServer("Factions") - 1);

                    target.sendMessage(ChatColor.RED + "(!) teletransportando para outro Hub.");
                    target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    target.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                } catch (Exception e) {
                    System.out.println(ChatColor.RED + "Ops, algo ocorreu de errado!");
                }
            }

            while (Bukkit.getOnlinePlayers().size() != 0) {
                try {
                    Thread.sleep(25);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            Bukkit.shutdown();
        }
        return false;
    }

    private static List<String> getNumberInOrder() {
        List<String> list = new ArrayList<String>();
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Online_server ORDER BY Online");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                if (rs.getString("Server").contains("Hub") && ServerAPI.getManutencao(rs.getString("Server")) == false) {
                    list.add(rs.getString("Server"));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }
}
