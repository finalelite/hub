package com.hubfinal.commands;

import com.hubfinal.utils.AlternateColor;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class CommandReportar implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        Player player = (Player) sender;

        Inventory inv = Bukkit.createInventory(null, 27, "Reporte: ");

        ItemStack bugs = new ItemStack(Material.SLIME_BALL);
        ItemMeta bugsMeta = bugs.getItemMeta();
        bugsMeta.setDisplayName(ChatColor.GREEN + "Reporte: Bug");
        ArrayList<String> lore = new ArrayList<String>();
        lore.add(" ");
        lore.add(ChatColor.YELLOW + " * Se confirmado o bug, o reportador");
        lore.add(ChatColor.YELLOW + "   recebera uma premiação de $7.500,00");
        lore.add(ChatColor.YELLOW + "   Moedas, pelo reporte.");
        lore.add(" ");
        lore.add(ChatColor.RED + " * Caso as informações do reporte não");
        lore.add(ChatColor.RED + "   tenham nada haver com o assunto do");
        lore.add(ChatColor.RED + AlternateColor.alternate("   &nReporte-Bug") + ChatColor.RED + ", o mesmo leverá ban por");
        lore.add(ChatColor.RED + "   falso reporte.");
        lore.add(" ");
        bugsMeta.setLore(lore);
        bugs.setItemMeta(bugsMeta);

        ItemStack hacks = new ItemStack(Material.MAGMA_CREAM);
        ItemMeta hacksMeta = hacks.getItemMeta();
        hacksMeta.setDisplayName(ChatColor.RED + "Reporte: Hack");
        ArrayList<String> lore1 = new ArrayList<String>();
        lore1.add(" ");
        lore1.add(ChatColor.YELLOW + " * Se confirmado o hack, o reportador");
        lore1.add(ChatColor.YELLOW + "   recebera uma premiação de $4.000,00");
        lore1.add(ChatColor.YELLOW + "   Moedas, pelo reporte.");
        lore1.add(" ");
        lore1.add(ChatColor.RED + " * Caso as informações do reporte não");
        lore1.add(ChatColor.RED + "   tenham nada haver com o assunto do");
        lore1.add(ChatColor.RED + AlternateColor.alternate("   &nReporte-Hack") + ChatColor.RED + ", o mesmo leverá ban por");
        lore1.add(ChatColor.RED + "   falso reporte.");
        lore1.add(" ");
        hacksMeta.setLore(lore1);
        hacks.setItemMeta(hacksMeta);

        inv.setItem(12, bugs);
        inv.setItem(14, hacks);

        if (cmd.getName().equalsIgnoreCase("reportar")) {
            player.openInventory(inv);
        }
        return false;
    }
}
