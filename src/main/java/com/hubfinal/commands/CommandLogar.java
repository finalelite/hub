package com.hubfinal.commands;

import com.hubfinal.API.LoginAPI;
import com.hubfinal.API.PlayerUUID;
import com.hubfinal.API.TagAPI;
import com.hubfinal.Main;
import com.hubfinal.listeners.PlayerJoinEvents;
import com.hubfinal.utils.AlternateColor;
import com.hubfinal.utils.CentralizeMsg;
import com.hubfinal.utils.CentralizerText;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandLogar implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        Player player = (Player) sender;
        String UUID = PlayerUUID.getUUID(player.getName());

        if (cmd.getName().equalsIgnoreCase("Logar") || cmd.getName().equalsIgnoreCase("login")) {
            if (LoginAPI.getLoggedStatus(UUID) == false) {
                if (LoginAPI.getPassword(UUID) != null) {
                    if (args.length > 1 || args.length < 1) {
                        player.sendMessage(ChatColor.RED + "Comando correto: " + ChatColor.GRAY + "/logar (senha)");
                        return false;
                    }
                    if (args.length == 1) {
                        if (LoginAPI.getPassword(UUID).equals(LoginAPI.hashPassword(args[0]))) {
                            LoginAPI.updateLoggedStatus(UUID, true);
                            LoginAPI.updateTentativa(UUID, 3);
                            String str1 = ChatColor.YELLOW + "Bem-vindo " + ChatColor.GRAY + "ao universo" + ChatColor.DARK_PURPLE + " FinalElite";
                            String str2 = ChatColor.GRAY + "Utilize a bússola ou os NPC para acessar nossos servidores!";
                            String str3 = ChatColor.GRAY + "Caso necessite de mais informações ou ajuda utilize" + ChatColor.YELLOW + " /ajuda";
                            String str4 = ChatColor.GRAY + "Para adquirir " + ChatColor.GREEN + "VIP" + ChatColor.GRAY + " acesse: ";
                            String str5 = ChatColor.YELLOW + "finalelite.com.br";
                            String line1 = CentralizerText.centralize(str1, 72, " ");
                            String line2 = CentralizerText.centralize(str2, 63, " ");
                            String line3 = CentralizerText.centralize(str3, 66, " ");
                            String line4 = CentralizerText.centralize(str4, 72, " ");
                            String line5 = CentralizerText.centralize(str5, 72, " ");

                            PlayerJoinEvents.setRocket(player, UUID);

                            String tag = TagAPI.getTag(UUID);
                            if (!tag.equals("Membro")) {
                                if (tag.equals("Master")) {
                                    for (Player target : Bukkit.getServer().getOnlinePlayers()) {
                                        target.sendMessage(ChatColor.GOLD + "[GameMaster] " + player.getName() + ChatColor.GRAY + " - " + ChatColor.YELLOW + "acaba de se logar ao servidor.");
                                    }
                                }
                                if (tag.equals("Supervisor")) {
                                    for (Player target : Bukkit.getServer().getOnlinePlayers()) {
                                        target.sendMessage(ChatColor.DARK_RED + "[Supervisor] " + player.getName() + ChatColor.GRAY + " - " + ChatColor.YELLOW + "acaba de se logar ao servidor.");
                                    }
                                }
                                if (tag.equals("Admin")) {
                                    for (Player target : Bukkit.getServer().getOnlinePlayers()) {
                                        target.sendMessage(ChatColor.RED + "[Admin] " + player.getName() + ChatColor.GRAY + " - " + ChatColor.YELLOW + "acaba de se logar ao servidor.");
                                    }
                                }
                                if (tag.equals("Moderador")) {
                                    for (Player target : Bukkit.getServer().getOnlinePlayers()) {
                                        target.sendMessage(ChatColor.DARK_GREEN + "[Moderador] " + player.getName() + ChatColor.GRAY + " - " + ChatColor.YELLOW + "acaba de se logar ao servidor.");
                                    }
                                }
                                if (tag.equals("Suporte")) {
                                    for (Player target : Bukkit.getServer().getOnlinePlayers()) {
                                        target.sendMessage(ChatColor.GREEN + "[Suporte] " + player.getName() + ChatColor.GRAY + " - " + ChatColor.YELLOW + "acaba de se logar ao servidor.");
                                    }
                                }
                                if (tag.equals("Youtuber")) {
                                    for (Player target : Bukkit.getServer().getOnlinePlayers()) {
                                        target.sendMessage(ChatColor.RED + "[Youtuber] " + player.getName() + ChatColor.GRAY + " - " + ChatColor.YELLOW + "acaba de se logar ao servidor.");
                                    }
                                }
                                if (tag.equals("Vip")) {
                                    for (Player target : Bukkit.getServer().getOnlinePlayers()) {
                                        target.sendMessage(ChatColor.YELLOW + "[Conde] " + player.getName() + ChatColor.GRAY + " - " + ChatColor.YELLOW + "acaba de se logar ao servidor.");
                                    }
                                }
                                if (tag.equals("Vip+")) {
                                    for (Player target : Bukkit.getServer().getOnlinePlayers()) {
                                        target.sendMessage(ChatColor.AQUA + "[Lord] " + player.getName() + ChatColor.GRAY + " - " + ChatColor.YELLOW + "acaba de se logar ao servidor.");
                                    }
                                }
                                if (tag.equals("Vip++")) {
                                    for (Player target : Bukkit.getServer().getOnlinePlayers()) {
                                        target.sendMessage(ChatColor.DARK_PURPLE + "[Titan] " + player.getName() + ChatColor.GRAY + " - " + ChatColor.YELLOW + "acaba de se logar ao servidor.");
                                    }
                                }
                            }

                            player.resetTitle();
                            player.playEffect(player.getLocation(), Effect.DRAGON_BREATH, 0);
                            //EDITADO AQUI 1.12
                            player.playSound(player.getLocation(), Sound.UI_TOAST_CHALLENGE_COMPLETE, 1.0F, 1.0F);

                            PlayerJoinEvents.LoginTasks.tasks.get(player).cancel();
                            PlayerJoinEvents.LoginTasks.tasks.remove(player);

                            for (Player target : Bukkit.getOnlinePlayers()) {
                                target.showPlayer(player);
                                player.showPlayer(target);
                            }

                            Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                @Override
                                public void run() {
                                    if (LoginAPI.getEmail(UUID) == null) {
                                        player.sendMessage(AlternateColor.alternate("&e * Foi verificado que você ainda não possui um cadastro de email em sua atual conta. Para evitarmos problemas futuros utilize o comando &e&n/cadastrar email (email)." + "\n" + "&e "));
                                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                    }
                                }
                            }, 60L);

                            player.sendMessage(" " + "\n" + " " + "\n" + line1 + "\n" + line2 + "\n" + line3 + "\n" + line4 + "\n" + line5 + "\n" + " ");

                            PlayerJoinEvents.setItems(player);
                        } else {
                            if (LoginAPI.getTentativa(UUID) == 3) {
                                player.sendMessage(ChatColor.RED + "Senha, incorreta tente novamente. +3 tentativas!");
                                LoginAPI.updateTentativa(UUID, LoginAPI.getTentativa(UUID) - 1);
                            } else if (LoginAPI.getTentativa(UUID) == 2) {
                                player.sendMessage(ChatColor.RED + "Senha, incorreta tente novamente. +2 tentativas!");
                                LoginAPI.updateTentativa(UUID, LoginAPI.getTentativa(UUID) - 1);
                            } else if (LoginAPI.getTentativa(UUID) == 1) {
                                player.sendMessage(ChatColor.RED + "(!) Você tem apenas +1 tentativa.");
                                LoginAPI.updateTentativa(UUID, LoginAPI.getTentativa(UUID) - 1);
                            } else if (LoginAPI.getTentativa(UUID) == 0) {
                                String msg = CentralizeMsg.sendCenteredMessage(ChatColor.RED + "§lFinal Elite\n \n§cKickado.\nMotivo: Tentativas demasiadas de acesso.\n");
                                player.kickPlayer(msg);
                                LoginAPI.updateTentativa(UUID, 3);
                            } else {
                                player.sendMessage("T " + LoginAPI.getTentativa(UUID));
                                player.sendMessage(ChatColor.RED + "Senha, incorreta tente novamente.");
                                LoginAPI.setTentativa(UUID);
                            }
                        }
                    }
                }
            }
            return false;
        } else {
            player.sendMessage(ChatColor.RED + "Você já esta logado!");
            return false;
        }
    }
}
