package com.hubfinal.commands;

import com.hubfinal.API.TagAPI;
import com.hubfinal.Main;
import lombok.var;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandRestart implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String lb, String[] args) {
        var canUse = true;

        if(sender instanceof Player) {
            String uuid = ((Player) sender).getUniqueId().toString();

            canUse = TagAPI.getTag(uuid) != null && TagAPI.getTag(uuid).equalsIgnoreCase("Master") ||
                    TagAPI.getTag(uuid).equalsIgnoreCase("Supervisor");
        }
        if(canUse) {
            sender.sendMessage(ChatColor.GREEN + " * Iniciando temporizador de reinicio...");
            Main.rUtils.restart();
        } else {
            sender.sendMessage(ChatColor.RED + " * Você não tem permissão para executar este comando.");
        }

        return false;
    }

}
