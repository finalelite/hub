package com.hubfinal.provisional;

import java.util.HashMap;

public class IDprov {

    private String UUID;
    private int IDNpcPlayer;
    private static final HashMap<String, IDprov> CACHE = new HashMap<String, IDprov>();

    public IDprov(String UUIDwithNPCid, int IDNpcPlayer) {
        this.UUID = UUIDwithNPCid;
        this.IDNpcPlayer = IDNpcPlayer;
    }

    public IDprov insert() {
        CACHE.put(String.valueOf(this.UUID), this);

        return this;
    }

    public int getIDNpcPlayer() {
        return this.IDNpcPlayer;
    }

    public String getUUID() {
        return this.UUID;
    }

    public void setUUID(String UUID) {
        this.UUID = UUID;
    }

    public static IDprov get(String UUID) {
        return CACHE.get(String.valueOf(UUID));
    }
}
