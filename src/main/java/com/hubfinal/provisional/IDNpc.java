package com.hubfinal.provisional;

import java.util.HashMap;

public class IDNpc {

    private String UUID;
    private int ID;
    private static final HashMap<String, IDNpc> CACHE = new HashMap<String, IDNpc>();

    public IDNpc(String UUID, int ID) {
        this.UUID = UUID;
        this.ID = ID;
    }

    public IDNpc insert() {
        CACHE.put(String.valueOf(this.UUID), this);

        return this;
    }

    public int getId() {
        return this.ID;
    }

    public void setId(int ID) {
        this.ID = ID;
    }

    public String getUUID() {
        return this.UUID;
    }

    public static IDNpc get(String UUID) {
        return CACHE.get(String.valueOf(UUID));
    }
}
