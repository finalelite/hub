package com.hubfinal;

import com.hubfinal.API.PlayerUUID;
import com.hubfinal.listeners.DoubleJumper;
import com.hubfinal.utils.AfkHash;
import com.hubfinal.utils.CentralizeMsg;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class RegisterUtils {

    public static void register() {
        DoubleJumper.spawnParticles();
        updateAFKtime();
    }

    private static void updateAFKtime() {
        Thread thread = new Thread(() -> {
            while (true) {
                try {
                    Thread.sleep(1000 * 10);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                for (Player target : Bukkit.getOnlinePlayers()) {
                    String UUID = PlayerUUID.getUUID(target.getName());
                    if (AfkHash.get(UUID) != null) {
                        if (target.getLocation().getBlockX() == AfkHash.get(UUID).getX() && target.getLocation().getBlockY() == AfkHash.get(UUID).getY() && target.getLocation().getBlockZ() == AfkHash.get(UUID).getZ()) {
                            AfkHash.get(UUID).setTime(AfkHash.get(UUID).getTime() + 5);

                            if (AfkHash.get(UUID).getTime() >= 600) {
                                AfkHash.get(UUID).setTime(0);
                                String msg = CentralizeMsg.sendCenteredMessage(ChatColor.RED + "§lFinal Elite\n \n§cVocê foi kickado por ter ficado AFK\n§cpor muito tempo!");
                                target.kickPlayer(msg);
                            }
                        }
                    }
                }
            }
        });
        thread.start();
    }
}
