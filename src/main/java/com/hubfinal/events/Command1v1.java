package com.hubfinal.events;

import com.hubfinal.events.hashs.Hash1v1;
import com.hubfinal.events.hashs.InPvPHash;
import com.hubfinal.listeners.PlayerJoinEvents;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class Command1v1 implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        Player player = (Player) sender;
        String uuid = player.getUniqueId().toString();

        if (cmd.getName().equalsIgnoreCase("1v1")) {
            if (Hash1v1.get(uuid) == null) {
                new Hash1v1(uuid, true).insert();
                player.getInventory().clear();
                player.getInventory().setHelmet(new ItemStack(Material.RED_BANNER));
                player.sendMessage(ChatColor.GREEN + " \n * Modo 1v1 habilitado, para desabilitar utilizar /1v1\n  " + ChatColor.GRAY + "Players com banner vermelho em sua cabeça, se\n   encontram com o modo 1v1 ativo!\n ");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                ItemStack selec = new ItemStack(Material.BLAZE_ROD);
                ItemMeta meta = selec.getItemMeta();
                meta.setDisplayName(ChatColor.YELLOW + "Selecionador");
                ArrayList<String> lore = new ArrayList<>();
                lore.add(" ");
                lore.add(ChatColor.GRAY + "Utilize este item para convidar players para 1v1.");
                lore.add(" ");
                meta.setLore(lore);
                selec.setItemMeta(meta);

                player.getInventory().setItem(4, selec);
            } else {
                if (Hash1v1.get(uuid).getStatus() == true) {
                   if (InPvPHash.get(uuid) != null) {
                       player.sendMessage(ChatColor.RED + " * Ops, você não pode sair do 1v1 em combate!");
                       player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                       player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                       return false;
                   } else {
                       Hash1v1.get(uuid).setStatus(false);
                       player.sendMessage(ChatColor.GREEN + " * Modo 1v1 desativado!");
                       player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                       PlayerJoinEvents.setRocket(player, player.getUniqueId().toString());
                       PlayerJoinEvents.setItems(player);
                   }
                } else {
                    Hash1v1.get(uuid).setStatus(true);
                    player.getInventory().clear();
                    player.getInventory().setHelmet(new ItemStack(Material.RED_BANNER));
                    player.sendMessage(ChatColor.GREEN + " \n * Modo 1v1 habilitado, para desabilitar utilizar /1v1\n  " + ChatColor.GRAY + "Players com banner vermelho em sua cabeça, se\n   encontram com o modo 1v1 ativo!\n ");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    ItemStack selec = new ItemStack(Material.BLAZE_ROD);
                    ItemMeta meta = selec.getItemMeta();
                    meta.setDisplayName(ChatColor.YELLOW + "Selecionador");
                    ArrayList<String> lore = new ArrayList<>();
                    lore.add(" ");
                    lore.add(ChatColor.GRAY + "Utilize este item para convidar players para 1v1.");
                    lore.add(" ");
                    meta.setLore(lore);
                    selec.setItemMeta(meta);

                    player.getInventory().setItem(4, selec);
                }
            }
        }
        return false;
    }
}
