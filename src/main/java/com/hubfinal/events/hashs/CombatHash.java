package com.hubfinal.events.hashs;

import java.util.HashMap;

public class CombatHash {

    private String player;
    private String target;

    public static HashMap<String, CombatHash> cache = new HashMap<>();

    public CombatHash(String player, String target) {
        this.player = player;
        this.target = target;
    }

    public static CombatHash get(String player) {
        return cache.get(player);
    }

    public CombatHash insert() {
        cache.put(this.player, this);
        return this;
    }

    public String getTarget() {
        return this.target;
    }
}
