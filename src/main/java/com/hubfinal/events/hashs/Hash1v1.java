package com.hubfinal.events.hashs;

import java.util.HashMap;

public class Hash1v1 {

    private String uuid;
    private boolean status;

    public static HashMap<String, Hash1v1> cache = new HashMap<>();

    public Hash1v1(String uuid, boolean status) {
        this.uuid = uuid;
        this.status = status;
    }

    public static Hash1v1 get(String uuid) {
        return cache.get(uuid);
    }

    public Hash1v1 insert() {
        cache.put(this.uuid, this);
        return this;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public boolean getStatus() {
        return this.status;
    }
}
