package com.hubfinal.events.hashs;

import java.util.HashMap;

public class InPvPHash {

    private String uuid;

    public static HashMap<String, InPvPHash> cache = new HashMap<>();

    public InPvPHash (String uuid) {
        this.uuid = uuid;
    }

    public static InPvPHash get(String uuid) {
        return cache.get(uuid);
    }

    public InPvPHash insert() {
        cache.put(this.uuid, this);
        return this;
    }
}
