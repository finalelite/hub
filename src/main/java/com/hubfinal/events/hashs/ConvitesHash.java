package com.hubfinal.events.hashs;

import java.util.ArrayList;
import java.util.HashMap;

public class ConvitesHash {

    private String uuid;
    private ArrayList<String> list;

    public static HashMap<String, ConvitesHash> cache = new HashMap<>();

    public ConvitesHash (String uuid, ArrayList<String> list) {
        this.uuid = uuid;
        this.list = list;
    }

    public static ConvitesHash get(String uuid) {
        return cache.get(uuid);
    }

    public ConvitesHash insert() {
        cache.put(this.uuid, this);
        return this;
    }

    public ArrayList<String> getList() {
        return this.list;
    }

    public boolean containsPlayer(String player) {
        return this.list.contains(player);
    }

    public void addPlayer(String player) {
        this.list.add(player);
    }

    public void removePlayer(String player) {
        this.list.remove(player);
    }
}
