package com.hubfinal.events.listeners;

import com.hubfinal.commands.CommandSetspawn;
import com.hubfinal.events.hashs.CombatHash;
import com.hubfinal.events.hashs.InPvPHash;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class PlayerDeathEvent implements Listener {

    @EventHandler
    public void onDeath(org.bukkit.event.entity.PlayerDeathEvent event) {
        Player player = event.getEntity();
        Player killer = event.getEntity().getKiller();

        event.setDeathMessage(null);
        event.getDrops().clear();

        if (InPvPHash.get(player.getUniqueId().toString()) != null && InPvPHash.get(killer.getUniqueId().toString()) != null) {
            InPvPHash.cache.remove(player.getUniqueId().toString());
            InPvPHash.cache.remove(killer.getUniqueId().toString());
            CombatHash.cache.remove(player.getUniqueId().toString());
            CombatHash.cache.remove(killer.getUniqueId().toString());

            ItemStack selec = new ItemStack(Material.BLAZE_ROD);
            ItemMeta meta = selec.getItemMeta();
            meta.setDisplayName(ChatColor.YELLOW + "Selecionador");
            ArrayList<String> lore = new ArrayList<>();
            lore.add(" ");
            lore.add(ChatColor.GRAY + "Utilize este item para convidar players para 1v1.");
            lore.add(" ");
            meta.setLore(lore);
            selec.setItemMeta(meta);

            String infos = CommandSetspawn.getLoc("Hub");
            String[] infosList = infos.split("/");
            Location loc = new Location(Bukkit.getWorld(infosList[0]), Double.parseDouble(infosList[1]), Double.parseDouble(infosList[2]), Double.parseDouble(infosList[3]), Float.valueOf(infosList[4]), Float.valueOf(infosList[5]));


            player.getInventory().clear();
            player.getInventory().setHelmet(new ItemStack(Material.RED_BANNER));
            player.getInventory().setItem(4, selec);
            player.sendMessage(ChatColor.RED + " * Nem sempre o mais forte é campeão!");
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
            player.teleport(loc);

            killer.getInventory().clear();
            killer.getInventory().setHelmet(new ItemStack(Material.RED_BANNER));
            killer.getInventory().setItem(4, selec);
            killer.sendMessage(ChatColor.GREEN + " * Mais uma vitória, já está se tornando comum!");
            killer.playSound(killer.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            killer.playSound(killer.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
            killer.teleport(loc);
        }
    }

    @EventHandler
    public void respawn(PlayerRespawnEvent event) {
        Player player = event.getPlayer();

        String infos = CommandSetspawn.getLoc("Hub");
        String[] infosList = infos.split("/");
        Location loc = new Location(Bukkit.getWorld(infosList[0]), Double.parseDouble(infosList[1]), Double.parseDouble(infosList[2]), Double.parseDouble(infosList[3]), Float.valueOf(infosList[4]), Float.valueOf(infosList[5]));
        event.setRespawnLocation(loc);

        ItemStack selec = new ItemStack(Material.BLAZE_ROD);
        ItemMeta meta = selec.getItemMeta();
        meta.setDisplayName(ChatColor.YELLOW + "Selecionador");
        ArrayList<String> lore = new ArrayList<>();
        lore.add(" ");
        lore.add(ChatColor.GRAY + "Utilize este item para convidar players para 1v1.");
        lore.add(" ");
        meta.setLore(lore);
        selec.setItemMeta(meta);

        player.getInventory().clear();
        player.getInventory().setHelmet(new ItemStack(Material.RED_BANNER));
        player.getInventory().setItem(4, selec);
    }
}
