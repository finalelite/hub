package com.hubfinal.events.listeners;

import com.hubfinal.API.ConfigAPI;
import com.hubfinal.events.hashs.CombatHash;
import com.hubfinal.events.hashs.ConvitesHash;
import com.hubfinal.events.hashs.Hash1v1;
import com.hubfinal.events.hashs.InPvPHash;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

public class PlayerInteractAtPlayer implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractAtEntityEvent event) {
        if (event.getRightClicked() instanceof Player) {
            Player player = event.getPlayer();
            Player target = (Player) event.getRightClicked();

            if (Hash1v1.get(player.getUniqueId().toString()) != null && Hash1v1.get(player.getUniqueId().toString()).getStatus() == true) {
                if (Hash1v1.get(target.getUniqueId().toString()) != null && Hash1v1.get(target.getUniqueId().toString()).getStatus() == true) {
                    if (ConvitesHash.get(player.getUniqueId().toString()) != null && ConvitesHash.get(player.getUniqueId().toString()).getList().contains(target.getUniqueId().toString())) {
                        ConvitesHash.get(player.getUniqueId().toString()).removePlayer(target.getUniqueId().toString());

                        new InPvPHash(player.getUniqueId().toString()).insert();
                        new CombatHash(player.getUniqueId().toString(), target.getUniqueId().toString()).insert();
                        player.getInventory().clear();
                        ConfigAPI.updateFly(player.getUniqueId().toString(), false);
                        player.setFlying(false);
                        player.setAllowFlight(false);
                        player.sendMessage(ChatColor.RED + "  * 1V1 INICIADO!\n \n  " + ChatColor.GRAY + "Openente: " + target.getName() + "\n  Estatura: 2 blocos\n  Peso: ?\n \n  " + ChatColor.RED + "GOOD LUCK!\n ");
                        player.getInventory().addItem(new ItemStack(Material.DIAMOND_SWORD));
                        player.getInventory().addItem(new ItemStack(Material.GOLDEN_APPLE, 5));
                        player.getInventory().setHelmet(new ItemStack(Material.IRON_HELMET));
                        player.getInventory().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
                        player.getInventory().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
                        player.getInventory().setBoots(new ItemStack(Material.IRON_BOOTS));
                        Location loc1 = new Location(Bukkit.getWorld("world"), -123.5, 63, -56.5, -0.4f, 0f);

                        new InPvPHash(target.getUniqueId().toString()).insert();
                        new CombatHash(target.getUniqueId().toString(), player.getUniqueId().toString()).insert();
                        target.getInventory().clear();
                        ConfigAPI.updateFly(target.getUniqueId().toString(), false);
                        target.setFlying(false);
                        target.setAllowFlight(false);
                        target.sendMessage(ChatColor.RED + "  * 1V1 INICIADO!\n \n  " + ChatColor.GRAY + "Openente: " + player.getName() + "\n  Estatura: 2 blocos\n  Peso: ?\n \n  " + ChatColor.RED + "GOOD LUCK!\n ");
                        target.getInventory().addItem(new ItemStack(Material.DIAMOND_SWORD));
                        target.getInventory().addItem(new ItemStack(Material.GOLDEN_APPLE, 5));
                        target.getInventory().setHelmet(new ItemStack(Material.IRON_HELMET));
                        target.getInventory().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
                        target.getInventory().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
                        target.getInventory().setBoots(new ItemStack(Material.IRON_BOOTS));
                        //123.5 63 -32.5 179.3 -0.9

                        return;
                    }
                    if (ConvitesHash.get(target.getUniqueId().toString()) == null) {
                        ArrayList<String> list = new ArrayList<>();
                        list.add(player.getUniqueId().toString());
                        new ConvitesHash(target.getUniqueId().toString(), list).insert();

                        target.sendMessage(ChatColor.GREEN + " \n  * " + player.getName() + " lhe convidou para 1v1!\n  " + ChatColor.GRAY + "Para aceitar utilize seu selecionador sobre o mesmo.\n ");
                        target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    } else {
                        if (ConvitesHash.get(target.getUniqueId().toString()).containsPlayer(player.getUniqueId().toString()) == false) {
                            ConvitesHash.get(target.getUniqueId().toString()).addPlayer(player.getUniqueId().toString());
                            target.sendMessage(ChatColor.GREEN + " \n  * " + player.getName() + " lhe convidou para 1v1!\n  " + ChatColor.GRAY + "Para aceitar utilize seu selecionador sobre o mesmo.\n ");
                            target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        } else {
                            player.sendMessage(ChatColor.RED + " * Ops, você já enviou um convite a este player!");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    }
                } else {
                    player.sendMessage(ChatColor.RED + " * Ops, este player não está com o modo 1v1 ativo.");
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }
        }
    }
}
