package com.hubfinal.utils;

import br.com.finalelite.pauloo27.api.PauloAPI;
import br.com.finalelite.pauloo27.api.database.AccountInfo;
import br.com.finalelite.pauloo27.api.database.PlayerRole;
import com.hubfinal.API.LoginAPI;
import com.hubfinal.API.PlayerUUID;
import com.hubfinal.API.ServerAPI;
import com.hubfinal.API.TagAPI;
import com.hubfinal.Main;
import com.hubfinal.vipAutentication.AuthAPI;
import lombok.val;
import lombok.var;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class JoinQueue {
    private static List<AccountInfo> queue = new ArrayList<>();

    static {
        startQueueTask();
    }

    public static void startQueueTask() {
        new BukkitRunnable() {
            @Override
            public void run() {
                AccountInfo info;
                Player player = null;
                var found = false;

                while (!found) {
                    try {
                        Thread.sleep(500);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    sendCurrentPosition();

                    try {
                        Thread.sleep(2000);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (queue.size() == 0)
                        return;

                    info = queue.get(0);
                    player = Bukkit.getPlayer(UUID.fromString(info.getUuid()));
                    if (player == null) {
                        queue.remove(info);
                        System.out.println("> Offline, removing from queue");
                        return;
                    }

                    try {
                        ByteArrayOutputStream b = new ByteArrayOutputStream();
                        DataOutputStream out = new DataOutputStream(b);

                        out.writeUTF("PlayerCount");
                        out.writeUTF("Factions");
                        player.sendPluginMessage(Main.getInstance(), "BungeeCord", b.toByteArray());
                    } catch (Exception e) {
                        e.printStackTrace();
                        return;
                    }

                    System.out.println("> Online: " + Main.playerCount);
                    if (Main.playerCount >= Main.maxPlayer) {
                        System.out.println("> Full");
                        if (Objects.requireNonNull(TagAPI.getTag(player.getUniqueId().toString())).equalsIgnoreCase("membro")) {
                            if (AuthAPI.getVip(player.getName()) == false) {
                                    player.sendActionBar(ChatColor.RED + "Servidor lotado! Atualmente temos" +
                                            " 120 vagas liberadas sendo que VIPs e Membros da Equipe tem suas vagas reservadas.");
                                    System.out.println("> Not VIP and Staff, keep in the queue");
                                    return;
                            }
                        }
                        System.out.println("> VIP or Staff. Teleport to the server");
                    }

                    if (ServerAPI.isRestarting("Factions")) {
                        System.out.println("> Restarting, keep in the queue");
                        player.sendActionBar(ChatColor.RED + "Servidor reiniciando! Você será "
                                + "conectado assim que possível. Aguarde...");
                        return;
                    }
                    queue.remove(info);
                    found = true;
                }

                System.out.println("Sending the player...");
                try {
                    ByteArrayOutputStream b = new ByteArrayOutputStream();
                    DataOutputStream out = new DataOutputStream(b);

                    out.writeUTF("Connect");
                    out.writeUTF("Factions");

                    player.sendMessage(ChatColor.GREEN + "(!) Conectando...");
                    player.sendPluginMessage(Main.plugin, "BungeeCord", b.toByteArray());
                } catch (Exception e) {
                    e.printStackTrace();
                    player.sendMessage(ChatColor.RED + "Ops, algo ocorreu de errado!");
                }
            }
        }.runTaskTimerAsynchronously(Main.getInstance(), 20, 40);

    }

    public static void sendCurrentPosition() {
        IntStream.range(0, queue.size()).forEach(i -> {
            val info = queue.get(i);
            val player = Bukkit.getPlayer(UUID.fromString(info.getUuid()));
            if (player == null)
                return;

            player.sendActionBar(String.format("%s(!) Posição atual na fila: %d", ChatColor.GREEN, i + 1));
        });
    }

    public static boolean addPlayerToQueue(Player player) {
        String uuid = PlayerUUID.getUUID(player.getName());

        if (!LoginAPI.getLoggedStatus(uuid)) {
            if (LoginAPI.getPassword(uuid) == null) {
                player.sendMessage(ChatColor.RED + "Ops, vamos nos cadastrar antes.");
            } else {
                if (LoginAPI.getPassword(uuid) == null) {
                    player.sendMessage(ChatColor.RED + "Ops, vamos nos cadastrar antes.");
                } else {
                    player.sendMessage(ChatColor.RED + "Ops, vamos nos logar antes.");
                }
            }

            return false;
        }

        val info = PauloAPI.getInstance().getAccountsCache().getAccountInfo(player);
        if (queue.contains(info))
            return false;

        System.out.println("Player added to queue " + player.getName());
        queue.add(info);
        queue = queue.stream()
                .filter(i -> Bukkit.getPlayer(UUID.fromString(i.getUuid())) != null)
                .sorted(Comparator.comparingInt(accountInfo -> {
                    val pp = Bukkit.getPlayer(UUID.fromString(info.getUuid())); 
                    if (pp == null)
                        return 100;

                    return (AuthAPI.getVip(pp.getName()) && accountInfo.getRole() == PlayerRole.MEMBRO) ? 0 :
                        accountInfo.getRole().ordinal();
                }))
                .collect(Collectors.toList());
        return true;
    }
}
