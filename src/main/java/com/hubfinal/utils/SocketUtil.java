package com.hubfinal.utils;

import com.hubfinal.API.ServerAPI;
import com.hubfinal.Main;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class SocketUtil {

    public static void online() {
        try {
            Socket s = new Socket();
            s.connect(new InetSocketAddress(Main.ip, ServerAPI.getPort("Hub1")));
            s.close();

            ServerAPI.updateOnlineStatus("Hub1", true);
        } catch (UnknownHostException e) {
            ServerAPI.updateOnlineStatus("Hub1", false);
        } catch (IOException e) {
            ServerAPI.updateOnlineStatus("Hub1", false);
        }

        try {
            Socket s = new Socket();
            s.connect(new InetSocketAddress(Main.ip, ServerAPI.getPort("Hub2")));
            s.close();

            ServerAPI.updateOnlineStatus("Hub2", true);
        } catch (UnknownHostException e) {
            ServerAPI.updateOnlineStatus("Hub2", false);
        } catch (IOException e) {
            ServerAPI.updateOnlineStatus("Hub2", false);
        }

        try {
            Socket s = new Socket();
            s.connect(new InetSocketAddress(Main.ip, ServerAPI.getPort("Hub3")));
            s.close();

            ServerAPI.updateOnlineStatus("Hub3", true);
        } catch (UnknownHostException e) {
            ServerAPI.updateOnlineStatus("Hub3", false);
        } catch (IOException e) {
            ServerAPI.updateOnlineStatus("Hub3", false);
        }

        try {
            Socket s = new Socket();
            s.connect(new InetSocketAddress(Main.ip, ServerAPI.getPort("Factions")));
            s.close();

            ServerAPI.updateOnlineStatus("Factions", true);
        } catch (UnknownHostException e) {
            ServerAPI.updateOnlineStatus("Factions", false);
        } catch (IOException e) {
            ServerAPI.updateOnlineStatus("Factions", false);
        }

        try {
            Socket s = new Socket();
            s.connect(new InetSocketAddress(Main.ip, ServerAPI.getPort("Construir")));
            s.close();

            ServerAPI.updateOnlineStatus("Construir", true);
        } catch (UnknownHostException e) {
            ServerAPI.updateOnlineStatus("Construir", false);
        } catch (IOException e) {
            ServerAPI.updateOnlineStatus("Construir", false);
        }
    }
}
