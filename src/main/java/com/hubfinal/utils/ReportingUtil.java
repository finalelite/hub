package com.hubfinal.utils;

import java.util.HashMap;

public class ReportingUtil {

    private String UUID;
    private int paginas;
    private String text;
    private static final HashMap<String, ReportingUtil> CACHE = new HashMap<String, ReportingUtil>();

    public ReportingUtil(String UUID, int paginas, String text) {
        this.UUID = UUID;
        this.paginas = paginas;
        this.text = text;
    }

    public ReportingUtil insert() {
        CACHE.put(String.valueOf(this.UUID), this);

        return this;
    }

    public String getUUID() {
        return this.UUID;
    }

    public int getPaginas() {
        return this.paginas;
    }

    public void setPaginas(int paginas) {
        this.paginas = paginas;
    }

    public String getText() {
        return this.text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public static ReportingUtil get(String UUID) {
        return CACHE.get(String.valueOf(UUID));
    }
}
