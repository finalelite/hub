package com.hubfinal.utils;

import java.util.HashMap;

public class RowNumber {

    private String UUID;
    private int ID;
    private static final HashMap<String, RowNumber> CACHE = new HashMap<String, RowNumber>();

    public RowNumber(String UUID, int ID) {
        this.UUID = UUID;
        this.ID = ID;
    }

    public RowNumber insert() {
        CACHE.put(String.valueOf(this.UUID), this);

        return this;
    }

    public int getId() {
        return this.ID;
    }

    public void setId(int ID) {
        this.ID = ID;
    }

    public void setUUID(String UUID) {
        this.UUID = UUID;
    }

    public String getUUID() {
        return this.UUID;
    }

    public static RowNumber get(String UUID) {
        return CACHE.get(String.valueOf(UUID));
    }
}
