package com.hubfinal.utils;

import br.com.finalelite.pauloo27.api.message.MessageUtils;
import com.destroystokyo.paper.Title;
import com.hubfinal.Main;
import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.concurrent.atomic.AtomicInteger;

public class RestartUtils {

    public boolean restarting = false;

    public void startTask() {
        new BukkitRunnable() {
            @Override
            public void run() {
                if (restarting) {
                    cancel();
                    return;
                }
                val now = Instant.now();
                val uptime = now.getEpochSecond() - Main.startedDate.getEpochSecond();
             
                val ldt = LocalDateTime.ofInstant(now, ZoneId.systemDefault());
                if (uptime >= 2 * 60 * 60 && ldt.getHour() == 12 &&
                        ldt.getMinute() >= 00)
                    restart();
            }
        }.runTaskTimerAsynchronously(Main.getInstance(), 60 * 20, 20 * 30);
    }

    public void restart() {
        if (restarting)
            return;

        restarting = true;
        val cycle = new AtomicInteger();
        new BukkitRunnable() {
            @Override
            public void run() {
                val currentCycle = cycle.getAndIncrement();
                val reamingTime = (5 * 60) - currentCycle * 10;
                // 4.59 minutes
                if (currentCycle >= 30) {
                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            Bukkit.getOnlinePlayers().forEach(p -> p.sendTitle(Title.builder()
                                    .fadeIn(30)
                                    .fadeOut(30)
                                    .stay(50)
                                    .title("")
                                    .subtitle(MessageUtils
                                            .colourMessage("&4* Servidor reiniciando *"))
                                    .build()));

                            kickPlayers();

                        }

                    }.runTask(Main.getInstance());

                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            Bukkit.getOnlinePlayers().forEach(p -> p.kickPlayer(ChatColor.RED + "Reiniciando..."));
                            Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "restart");
                        }

                    }.runTask(Main.getInstance());
                    cancel();
                }

                if (reamingTime != 60 && reamingTime > 30 && currentCycle % 6 == 0) {
                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            Bukkit.getOnlinePlayers().forEach(p -> {
                                p.sendMessage(MessageUtils
                                        .colourMessage(String.format("\n&4 * O servidor irá"
                                                        + " reiniciar em %s.\n",
                                                MessageUtils.pluralize(reamingTime / 60,
                                                        "minuto", "minutos"))));
                            });
                        }

                    }.runTask(Main.getInstance());
                } else if (reamingTime == 60) {
                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            Bukkit.getOnlinePlayers().forEach(p -> {
                                p.sendMessage(MessageUtils
                                        .colourMessage("&4&l * O servidor irá"
                                                + "reiniciar em 1 minuto."));

                                p.sendTitle(Title.builder()
                                        .fadeIn(30)
                                        .fadeOut(30)
                                        .stay(50)
                                        .title("")
                                        .subtitle(MessageUtils
                                                .colourMessage("&4&l * O servidor irá reiniciar em 1 minuto.")).build());
                            });
                        }
                    }.runTask(Main.getInstance());
                } else if (reamingTime <= 30) {
                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            Bukkit.getOnlinePlayers().forEach(p -> {
                                p.sendMessage(MessageUtils
                                        .colourMessage(String.format("&4&l * O servidor irá reiniciar em %s.",
                                                MessageUtils.pluralize(reamingTime,
                                                        "segundo", "segundos"))));

                                p.sendTitle(Title.builder()
                                        .fadeIn(30)
                                        .fadeOut(30)
                                        .stay(50)
                                        .title("")
                                        .subtitle(MessageUtils
                                                .colourMessage(String.format("&4&l * O servidor irá reiniciar em %s.",
                                                        MessageUtils.pluralize(reamingTime,
                                                                "segundo", "segundos")))).build());
                            });
                        }
                    }.runTask(Main.getInstance());
                }
            }
        }.runTaskTimerAsynchronously(Main.getInstance(), 20, 20 * 10);
    }

    public static void kickPlayers() {
        if (Bukkit.getOnlinePlayers().size() > 0) {
            for (Player target : Bukkit.getOnlinePlayers()) {

                target.kickPlayer("§6§lFINALELITE\n\n§eO Lobby está reiniciando, por favor conectar-se novamente.");

            }
        }
    }
}
