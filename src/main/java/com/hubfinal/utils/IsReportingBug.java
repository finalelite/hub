package com.hubfinal.utils;

import java.util.HashMap;

public class IsReportingBug {

    private String UUID;
    private boolean IsReporting;
    private static final HashMap<String, IsReportingBug> CACHE = new HashMap<String, IsReportingBug>();

    public IsReportingBug(String UUID, boolean IsReporting) {
        this.UUID = UUID;
        this.IsReporting = IsReporting;
    }

    public IsReportingBug insert() {
        CACHE.put(String.valueOf(this.UUID), this);

        return this;
    }

    public String getUUID() {
        return this.UUID;
    }

    public boolean getIsReporting() {
        return this.IsReporting;
    }

    public void setIsReporting(boolean IsReporting) {
        this.IsReporting = IsReporting;
    }

    public static IsReportingBug get(String UUID) {
        return CACHE.get(String.valueOf(UUID));
    }
}
