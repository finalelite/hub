package com.hubfinal.utils;

public class Chat {

    public static boolean chatStatus;

    public static void setStatus(boolean status) {
        chatStatus = status;
    }

    public static boolean getStatus() {
        return chatStatus;
    }
}
