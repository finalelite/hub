package com.hubfinal.utils;

import java.util.HashMap;

public class OnRowUtil {

    private String UUID;
    private boolean onRow;
    private static final HashMap<String, OnRowUtil> CACHE = new HashMap<String, OnRowUtil>();

    public OnRowUtil(String UUID, boolean onRow) {
        this.UUID = UUID;
        this.onRow = onRow;
    }

    public OnRowUtil insert() {
        CACHE.put(String.valueOf(this.UUID), this);

        return this;
    }

    public boolean getOnRow() {
        return this.onRow;
    }

    public void setOnRow(boolean onRow) {
        this.onRow = onRow;
    }

    public String getUUID() {
        return this.UUID;
    }

    public static OnRowUtil get(String UUID) {
        return CACHE.get(String.valueOf(UUID));
    }
}
