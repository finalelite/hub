package com.hubfinal.utils;

import java.util.HashMap;

public class UUIDcache {

    private String nick;
    private String UUID;
    private static final HashMap<String, UUIDcache> CACHE = new HashMap<String, UUIDcache>();

    public UUIDcache(String nick, String UUID) {
        this.nick = nick;
        this.UUID = UUID;
    }

    public UUIDcache insert() {
        CACHE.put(String.valueOf(this.nick), this);

        return this;
    }

    public String getUUID() {
        return this.UUID;
    }

    public void setUUID(String UUID) {
        this.UUID = UUID;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getNick() {
        return this.nick;
    }

    public static UUIDcache get(String nick) {
        return CACHE.get(String.valueOf(nick));
    }
}
