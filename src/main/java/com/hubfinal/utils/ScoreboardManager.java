package com.hubfinal.utils;

import br.com.finalelite.pauloo27.api.PauloAPI;
import br.com.finalelite.pauloo27.api.database.PlayerRole;
import br.com.finalelite.pauloo27.api.scoreboard.ScoreboardBuilder;
import br.com.finalelite.pauloo27.api.scoreboard.ScoreboardBuilder.ScoreboardEntry;
import br.com.finalelite.pauloo27.api.scoreboard.TabListManager;
import br.com.finalelite.pauloo27.api.scoreboard.TabListManager.TabGroup;
import lombok.Getter;
import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.Arrays;

public class ScoreboardManager {

    @Getter
    private final ScoreboardBuilder scoreboard;

    @Getter
    private final TabListManager tablistManager;


    public ScoreboardManager() {

        scoreboard = new ScoreboardBuilder("scoreFE", "&6&lFinalElite");

        scoreboard.addWhiteSpace();

        scoreboard.addEntry(new ScoreboardEntry("group",
                "Grupo: ",
                 player -> {

                     val role = PauloAPI.getInstance().getAccountInfo(player).getRole();
                     return role.getColor() + role.getDisplayName();

                 }

                ));

        scoreboard.addEntry(new ScoreboardEntry("online",

                "Online: ",
                player -> "&a" + Bukkit.getOnlinePlayers().size()

                ));


        scoreboard.addEntry(new ScoreboardEntry("server",

                "Servidor: ",
                player -> "&aHub1"

                ));

        scoreboard.addWhiteSpace();

        scoreboard.addEntry(new ScoreboardEntry("site",

                "&5www.finalelite.com.br",
                player -> ""

                ));

        tablistManager = new TabListManager();

        Arrays.stream(PlayerRole.values()).forEach(role -> {

            val color = ChatColor.getByChar(role.getColor().replace("&", "").charAt(0));

            tablistManager
                    .addGroup(new TabGroup(color,

                            (role == PlayerRole.MEMBRO ? color.toString() : String.format("%s[%s] ", color, role.getDisplayName())),
                            role.getDisplayName(),
                            null));

        });

    }

    public void createScoreBoard(Player p) {

        val scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
        p.setScoreboard(scoreboard);

    }

    public void setScoreboard(Player p) {

        scoreboard.setScoreboard(p);

    }


    public void setTablist(Player p) {

        tablistManager.setTabListToPlayer(p);

    }

    public void updateTablist(Player p) {

        Bukkit.getOnlinePlayers().forEach(target -> this.tablistManager.updatePlayerGroup(p, target));

    }

}
