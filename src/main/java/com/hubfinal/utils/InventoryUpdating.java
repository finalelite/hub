package com.hubfinal.utils;

import com.hubfinal.API.ServerAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class InventoryUpdating {

    public static void update(String server) {
        for (Player target : Bukkit.getServer().getOnlinePlayers()) {
            if (!ServerAPI.getManutencao(server) && ServerAPI.getOnlineStatus(server)) {
                if (target.getOpenInventory().getTitle().equals("Servidores: ")) {
                    ItemStack prov = target.getOpenInventory().getItem(13).clone();
                    String on = prov.getItemMeta().getLore().get(5);
                    String teste = on.substring(1);
                    String requiredString = teste.substring(teste.indexOf("5") + 1, teste.indexOf("§"));

                    if (ServerAPI.getOnlineServer("Factions") != Integer.valueOf(requiredString)) {
                        ItemStack factions = new ItemStack(Material.DIAMOND_SWORD);
                        ItemMeta factionsMeta = factions.getItemMeta();
                        factionsMeta.setDisplayName(ChatColor.GREEN + "Factions: A nova geração");
                        ArrayList<String> lore = new ArrayList<String>();
                        lore.add(" ");
                        lore.add("§7Viva em um cenário pós apocalíptico no qual");
                        lore.add("§7apenas os mais fortes e leigos resistem, tente");
                        lore.add("§7emudar a historia deixe sua marca para sempre!");
                        lore.add(" ");
                        lore.add(ChatColor.DARK_PURPLE + String.valueOf(ServerAPI.getOnlineServer("Factions")) + ChatColor.GRAY + " Jogadores online.");
                        lore.add(" ");
                        factionsMeta.setLore(lore);
                        factions.setItemMeta(factionsMeta);

                        target.getOpenInventory().setItem(13, factions);
                    }
                }
            }
        }
    }
}
