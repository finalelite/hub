package com.hubfinal.utils;

import com.hubfinal.MySql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CooldownBan {

    public static String getBan(String UUID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Cooldown_ban WHERE UUID = ?");
            st.setString(1, UUID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("UUID");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getBanTime(String UUID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Cooldown_ban WHERE UUID = ?");
            st.setString(1, UUID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Tempo");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getBanMotivo(String UUID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Cooldown_ban WHERE UUID = ?");
            st.setString(1, UUID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Motivo");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getBanPor(String UUID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Cooldown_ban WHERE UUID = ?");
            st.setString(1, UUID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Por");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void setBan(String UUID, String Motivo, String Por, String Tempo) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Cooldown_ban (UUID, Motivo, Por, Tempo) VALUES (?, ?, ?, ?)");
            st.setString(1, UUID);
            st.setString(2, Motivo);
            st.setString(3, Por);
            st.setString(4, Tempo);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void removeBan(String UUID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("DELETE FROM Cooldown_ban WHERE UUID = ?");
            st.setString(1, UUID);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
