package com.hubfinal.utils;

import java.util.HashMap;

public class IsReportingHack {

    private String UUID;
    private boolean IsReporting;
    private static final HashMap<String, IsReportingHack> CACHE = new HashMap<String, IsReportingHack>();

    public IsReportingHack(String UUID, boolean IsReporting) {
        this.UUID = UUID;
        this.IsReporting = IsReporting;
    }

    public IsReportingHack insert() {
        CACHE.put(String.valueOf(this.UUID), this);

        return this;
    }

    public String getUUID() {
        return this.UUID;
    }

    public boolean getIsReporting() {
        return this.IsReporting;
    }

    public void setIsReporting(boolean IsReporting) {
        this.IsReporting = IsReporting;
    }

    public static IsReportingHack get(String UUID) {
        return CACHE.get(String.valueOf(UUID));
    }
}
