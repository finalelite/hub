package com.hubfinal.utils;

import java.util.HashMap;

public class AfkHash {

    private String UUID;
    private int x;
    private int y;
    private int z;
    private int time;
    private static final HashMap<String, AfkHash> CACHE = new HashMap<String, AfkHash>();

    public AfkHash(String UUID, int x, int y, int z, int time) {
        this.UUID = UUID;
        this.x = x;
        this.y = y;
        this.z = z;
        this.time = time;
    }

    public AfkHash insert() {
        CACHE.put(String.valueOf(this.UUID), this);

        return this;
    }

    public int getTime() {
        return this.time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public int getX() {
        return this.x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return this.y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getZ() {
        return this.z;
    }

    public void setZ(int z) {
        this.z = z;
    }

    public String getUUID() {
        return this.UUID;
    }

    public void setUUID(String UUID) {
        this.UUID = UUID;
    }

    public static AfkHash get(String UUID) {
        return CACHE.get(String.valueOf(UUID));
    }
}
